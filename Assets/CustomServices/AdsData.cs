﻿using UnityEngine;
using System.Collections;
using CustomService;
using SimpleJSON;

[CreateAssetMenu(fileName = "AdsData", menuName = "Service/AdsData")]
public class AdsData : ScriptableObject
{
    [Header("ADS")]
    public PlatformBool enabledAds;

    public int interstitialDelay = 30;

    [Header("Placements")]
    public AdsPlacement banner;
    public AdsPlacement[] interstitials;
    public AdsPlacement[] rewardeds;

#if UNITY_EDITOR

    public string testJson = "";

    [DefaultUtility.Button()]
    public void LoadDataToConsole()
    {
        Debug.Log(Settings);
    }

    [DefaultUtility.Button()]
    public void TestUpdate()
    {
        Settings = testJson;
    }
#endif
    public string Settings
    {
        get
        {
            return JsonData.ToString();
        }

        set
        {
            try
            {
                JsonData = JSON.Parse(value);
                AdsController.Instance.RefreshModule();
            }
            catch
            {
                Debug.LogWarningFormat("Json data is damaged : {0}", value);
            }
        }
    }

    protected JSONNode JsonData
    {
        get
        {
            JSONObject data = new JSONObject();
            data.Add("enableAds", enabledAds.Json);
            data.Add("interstitialDelay", new JSONNumber(interstitialDelay));
            data.Add("bannerEnabled", banner.enabled.Json);
            data.Add("interstitials", GetPlacementsSettings(interstitials));
            data.Add("rewarded", GetPlacementsSettings(rewardeds));
            return data;
        }

        set
        {
            enabledAds.Json = value["enableAds"];
            interstitialDelay = value["interstitialDelay"].AsInt;
            banner.enabled.Json = value["bannerEnabled"];
            SetPlacementsSettings(interstitials, value["interstitials"]);
            SetPlacementsSettings(rewardeds, value["rewarded"]);
        }
    }

    protected JSONNode GetPlacementsSettings(AdsPlacement[] placements)
    {
        JSONObject data = new JSONObject();

        for (int i = 0; i < placements.Length; i++)
        {
            data.Add(placements[i].placementKey, placements[i].enabled.Json);
        }
        return data;
    }

    protected void SetPlacementsSettings(AdsPlacement[] placements, JSONNode json)
    {
        for (int i = 0; i < placements.Length; i++)
        {
            if (json.HasKey(placements[i].placementKey))
            {
                placements[i].enabled.Json = json[placements[i].placementKey];
            }
        }
    }
}

[System.Serializable]
public struct PlatformBool
{
    public bool desktop;
    public bool ios;
    public bool android;

    public bool Value
    {
        get
        {
#if UNITY_EDITOR
            return desktop;
#elif UNITY_IOS
            return ios;
#elif UNITY_ANDROID
            return android;
#endif
        }

        set
        {
#if UNITY_EDITOR
            desktop = value;
#elif UNITY_IOS
            ios = value;
#elif UNITY_ANDROID
            android = value;
#endif
        }
    }

    public JSONNode Json
    {
        get
        {
            JSONObject data = new JSONObject();
            data.Add("ios", new JSONBool(ios));
            data.Add("android", new JSONBool(android));
            data.Add("desktop", new JSONBool(desktop));
            return data;
        }

        set
        {
            try
            {
                ios = value["ios"].AsBool;
                android = value["android"].AsBool;
                desktop = value["desktop"].AsBool;
            }
            catch
            {
                Debug.LogWarningFormat("Json data is damaged : {0}", value);
            }
        }
    }

    public string JsonData
    {
        get
        {
            return Json.ToString();
        }

        set
        {
            try
            {
                Json = JSON.Parse(value);
            }
            catch
            {
                Debug.LogWarningFormat("Json data is damaged : {0}", value);
            }
        }
    }
}
