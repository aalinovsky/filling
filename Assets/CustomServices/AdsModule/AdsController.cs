﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

namespace CustomService
{
    public class AdsController : Singletone<AdsController>
    {        
        public AdsData data;

        public MoPubAdsProvider moPubAds;
        public MockAdsProvider mockAds;
        [Space()]

        public AudioListener listener;
        protected AdsManager _adsManager;
        public AdsManager ADS
        {
            get
            {
                InitAdsManager();
                return _adsManager;
            }
        }

        protected bool enabledModule = false;

        public bool EnabledMobule
        {
            get
            {
                return enabledModule;
            }

            set
            {
                if (value == EnabledMobule)
                    return;

                if(value && !NoADS && !enabledModule)
                {
                    EnableAdsModule();
                }
                else if (!value)
                {
                    DisableAdsModule();
                }
            }
        }

        protected float lastAdsShowTime = 0;

        public static event Action<bool> setNoAdsState;

        public static bool NoADS
        {
            get
            {
                return PlayerPrefsUtility.GetBool("no_ads_state", false);
            }

            set
            {
                PlayerPrefsUtility.SetBool("no_ads_state", value);
                if (setNoAdsState != null)
                    setNoAdsState(value);
            }
        }

        protected bool CanShowAds
        {
            get
            {
                if (lastAdsShowTime == 0 || Time.time - lastAdsShowTime >= data.interstitialDelay)
                {
                    return true;
                }
                return false;
            }
        }

        private void Awake()
        {

        }

        void Start()
        {
            setNoAdsState += SetNoADS;
            InitAdsManager();
            if (data.enabledAds.Value)
            {
                EnabledMobule = data.enabledAds.Value;                
            }            
        }

        protected void SetNoADS(bool val)
        {
            if (val)
            {
                SetActiveBanner(!val, data.banner);
            }
        }

        protected void InitAdsManager()
        {
            if (_adsManager == null)
            {

#if UNITY_EDITOR
                _adsManager = new AdsManager(mockAds);
#elif MO_PUB
                _adsManager = new AdsManager(moPubAds);
#endif
            }
        }

        protected void DisableAudioListenerOnAds()
        {
            //HOT FIX
            //            return;
            //            if(ADS != null)
            //            {
            //                ADS.AdsStart += () => {
            //                    MusicManager.Instance.AdsStart();
            //                    SoundManager.Instance.AdsStart();
            //#if DEV_AN
            //                    DevLogs.LogFormat("Dev: Ads start");
            //#endif
            //                };
            //                ADS.AdsStop += () => {
            //                    MusicManager.Instance.AdsFinish();
            //                    SoundManager.Instance.AdsFinish();
            //#if DEV_AN
            //                    DevLogs.LogFormat("Dev: Ads finish");
            //#endif
            //                };
            //            }
        }

        protected void EnableAdsModule()
        {            
            if (!NoADS)
            {
                InitPlacementsEvents();
                ADS.InitModule();
                ADS.SetAdsEnable(true);
                enabledModule = true;
            }
            else
            {
                ADS.SetAdsEnable(false);
            }
        }

        protected void DisableAdsModule()
        {
            enabledModule = false;
            ADS.SetAdsEnable(false);
        }

        protected void InitPlacementsEvents()
        {
            data.banner.Init(()=> { return data.banner.IsEnabled; });

            for (int i = 0; i < data.interstitials.Length; i++)
            {
                var index = i;
                data.interstitials[index].Init( () => { return InterstitialIsReady(data.interstitials[index]); });
            }

            for (int i = 0; i < data.rewardeds.Length; i++)
            {
                var index = i;
                data.rewardeds[index].Init(() => { return RewardedIsReady(data.rewardeds[index]); });
            }
        }

        public void RefreshModule()
        {
            EnabledMobule = data.enabledAds.Value;
            SetActiveBanner(data.banner.enabled.Value, data.banner);
        }

        //////////////////////////////////////////////////////////////////

        public void SetActiveBanner(bool enabled, AdsPlacement placement)
        {
            if (ADS == null || !enabledModule)
            {
                return;
            }

            if (!placement.enabled.Value)
            {
                ADS.HideBanner(placement.Key);
                return;
            }

            if (enabled)
            {
                if (!NoADS || (NoADS && placement.activeWhenNoAds))
                {
                    ADS.ShowBanner(placement.Key);
                }
                else
                {
                    ADS.HideBanner(placement.Key);
                }
            }
            else
            {
                ADS.HideBanner(placement.Key);
            }
        }

        public void CacheInterstitial(AdsPlacement placement)
        {
            if (ADS == null || !enabledModule)
            {
                return;
            }
            if (!NoADS || (NoADS && placement.activeWhenNoAds))
            {
                ADS.CacheInterstitial(placement.Key);
            }
        }

        public bool ShowInterstitial(AdsPlacement placement, Action start, Action finish)
        {
            if (ADS == null || !enabledModule)
            {
                return false;
            }

            if (!placement.IsEnabled)
                return false;

            if ((!NoADS || (NoADS && placement.activeWhenNoAds)) && CanShowAds)
            {
                ADS.ClearInterstitialCallbacks();
                ADS.InterstitialShow += start;
                ADS.InterstitialShown += (string s) => { finish(); };
                lastAdsShowTime = Time.time;
                return ADS.ShowInterstitial(placement.Key);
            }
            else
            {
                return false;
            }
        }

        public bool InterstitialIsReady(AdsPlacement placement)
        {
            if (ADS == null || !enabledModule)
            {
                return false;
            }

            if (!placement.enabled.Value)
                return false;

            if ((!NoADS || (NoADS && placement.activeWhenNoAds)) && CanShowAds)
            {
                return ADS.IsInterstitialReady(placement.Key);
            }
            else
            {
                return false;
            }
        }

        public void CacheRewarded(AdsPlacement placement)
        {
            if (ADS == null || !enabledModule)
            {
                return;
            }
            if (!NoADS || (NoADS && placement.activeWhenNoAds))
            {
                ADS.CacheRewarded(placement.Key);
            }
        }

        public void ShowRewarded(AdsPlacement placement, Action<bool> action, Action fail)
        {
            if (ADS == null || !enabledModule)
            {
                return;
            }

            if (!placement.IsEnabled)
                return;

            if (!NoADS || (NoADS && placement.activeWhenNoAds))
            {
                ADS.ClearRewardedCallbacks();
                ADS.AddRewardedCloseCallback(fail);
                ADS.AddRewardedCompleteCallback(action);
                ADS.ShowRewardedVideo(placement.Key);
            }
        }

        public bool RewardedIsReady(AdsPlacement placement)
        {
            if (ADS == null || !enabledModule)
            {
                Debug.Log("ADS is null");
                return false;
            }

            if (!placement.enabled.Value)
                return false;

            if (!NoADS || (NoADS && placement.activeWhenNoAds))
            {
                return ADS.IsRewardedReady(placement.Key);
            }
            else
            {
                return false;
            }
        }
    }

}