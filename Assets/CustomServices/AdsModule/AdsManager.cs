using System;
using UnityEngine;

namespace CustomService
{
    public class AdsManager : IAdsProvider
    {
        public event Action<string> BannerClick;
        public event Action InterstitialShow;
        public event Action<string> InterstitialShown;
        public event Action<string> InterstitialClick;
        public event Action InterstitialClosed;
        public event Action<string> RewardedShown;
        public event Action<string> RewardedComplete;
        public event Action<string> RewardedClick;
        public event Action RewardedClose;

        public event Action AdsStart;
        public event Action AdsStop;

        private Action<bool> _intersitialFinishedCallback;
        public Action<bool> _rewardedCompleteCallback;

        private IAdsProvider _provider;
        private const string ADS_KEY = "AdsEnabled";

        public static bool IsInternetAvailable
        {
            get
            {

#if UNITY_EDITOR
                return true;
#else
                return Application.internetReachability != NetworkReachability.NotReachable;
#endif
            }
        }


        public AdsManager(IAdsProvider provider)
        {
            if (provider == null)
                throw new ArgumentNullException();

            _provider = provider;
            _provider.BannerClick += OnBannerClick;
            _provider.InterstitialShow += OnInterstitialShow;
            _provider.InterstitialShown += OnInterstitialShown;
            _provider.InterstitialClick += OnInterstitialClick;
            _provider.InterstitialClosed += OnInterstitialClosed;
            _provider.RewardedShown += OnRewardedShown;
            _provider.RewardedComplete += OnRewardedComplete;
            _provider.RewardedClose += OnRewardedClose;

            _intersitialFinishedCallback += (bool b) =>
            {
#if DEV_AN
                DevLogs.LogFormat("Dev: Interstitial finished");
#endif
            };
        }

        public void InitModule()
        {
            _provider.InitModule();           
        }

        public bool IsBannerHidden(string placement)
        {
            return _provider.IsBannerHidden(placement);
        }

        public void CacheInterstitial(string placement)
        {
            _provider.CacheInterstitial(placement);
        }

        public bool IsInterstitialReady(string placement)
        {
            return _provider.IsInterstitialReady(placement);
        }

        public virtual bool ShowInterstitial(string adKey)
        {
            if (!IsInternetAvailable)
            {
                return false;
            }

            var result = _provider.ShowInterstitial(adKey);

            return result;
        }

        public void CacheRewarded(string placement)
        {
            _provider.CacheRewarded(placement);
        }

        public bool IsRewardedReady(string placement)
        {
            if (!IsInternetAvailable)
                return false;
            return _provider.IsRewardedReady(placement);
        }

        public void ShowRewardedVideo(string placement)
        {
            if (!IsInternetAvailable)
            {
                return;
            }


            _provider.ShowRewardedVideo(placement);
        }

        public void SetAdsSoundsMuted(bool mute)
        {
            _provider.SetAdsSoundsMuted(mute);
        }

        public void SetAdsEnable(bool enable)
        {
            _provider.SetAdsEnable(enable);
        }

        public void ShowBanner(string adkey)
        {
            _provider.ShowBanner(adkey);
        }

        public void HideBanner(string placement)
        {
            _provider.HideBanner(placement);
        }

        //Banner callbacks

        void OnBannerClick(string networkName)
        {
            if (BannerClick != null)
                BannerClick(networkName);
        }


        //Interstitial callbacks

        void OnInterstitialClosed()
        {
            if (AdsStop != null)
                AdsStop();
            if (InterstitialClosed != null)
                InterstitialClosed();
            if (_intersitialFinishedCallback != null)
                _intersitialFinishedCallback(true);
        }

        void OnInterstitialShow()
        {
            if (AdsStart != null)
                AdsStart();
            if (InterstitialShow != null)
                InterstitialShow();
        }

        protected virtual void OnInterstitialShown(string value)
        {
            if (InterstitialShown != null)
                InterstitialShown(value);
        }


        void OnInterstitialClick(string networkName)
        {
            if (InterstitialClick != null)
                InterstitialClick(networkName);
        }

        public void ClearInterstitialCallbacks()
        {
            if (InterstitialShown != null)
                InterstitialShown = null;
            if (InterstitialClick != null)
                InterstitialClick = null;
            if (InterstitialShow != null)
                InterstitialShow = null;
        }

        //Rewarded callbacks

        void OnRewardedShown(string networkName)
        {
            if (AdsStart != null)
                AdsStart();
            if (RewardedShown != null)
                RewardedShown(networkName);
        }

        public void AddRewardedCompleteCallback(Action<bool> act)
        {
            _rewardedCompleteCallback += act;
        }

        public void AddRewardedCloseCallback(Action act)
        {
            RewardedClose += act;
        }

        void OnRewardedComplete(string networkName)
        {
            if (AdsStop != null)
                AdsStop();
            if (RewardedComplete != null)
                RewardedComplete(networkName);
            if (_rewardedCompleteCallback != null)
            {
                _rewardedCompleteCallback(true);
                _rewardedCompleteCallback = null;
            }
        }

        void OnRewardedClick(string networkName)
        {
            if (RewardedClick != null)
                RewardedClick(networkName);
        }

        void OnRewardedClose()
        {
            if (AdsStop != null)
                AdsStop();
            if (RewardedClose != null)
            {
                RewardedClose();
                RewardedClose = null;                
            }
            if (_rewardedCompleteCallback != null)
            {
                _rewardedCompleteCallback = null;
            }
        }

        public void ClearRewardedCallbacks()
        {
            if (RewardedClose != null)
            {
                RewardedClose = null;
            }
            if (_rewardedCompleteCallback != null)
            {
                _rewardedCompleteCallback = null;
            }
        }
    }
}