﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using CustomService;
using DefaultUtility;
using System;

[CreateAssetMenu(fileName = "AdsPlacement", menuName = "Service/placement")]
[System.Serializable]
public class AdsPlacement : ScriptableObject
{
    public string placementKey = "PlaceName";
    public PlatformBool enabled;

    [Space()]
    public bool activeWhenNoAds = false;
    public string androidKey = "test";
    public string iosKey = "test";
    public string desktopKey = "test";

    public event Action<bool> load;
    private event Func<bool> isReady;
    private bool inited = false;

    public string Key
    {
        get
        {
#if TEST_ADS
            return desktopKey;
#elif UNITY_IOS
            return iosKey;
#elif UNITY_ANDROID
            return androidKey;
#else 
            return desktopKey;
#endif
        }
    }

    public bool IsReadyToShow
    {
        get
        {
            if (isReady != null)
                return isReady();
            return false;
        }
    }

    public void Init(Func<bool> isReadyFun)
    {
        if (inited)
            return;

        inited = true;
        isReady += isReadyFun;
    }

    public void ShowBanner()
    {
        AdsController.Instance.SetActiveBanner(true, this);
    }

    public void ShowInterstitial(Action start, Action finish)
    {
        AdsController.Instance.ShowInterstitial(this, start, finish);
    }

    public void ShowRewarded(Action<bool> resultCallback, Action fail)
    {
        AdsController.Instance.ShowRewarded(this, resultCallback, fail);
    }

    public bool IsEnabled
    {
        get
        {
            return enabled.Value;
        }

        set
        {
            enabled.Value = value;
        }
    }

    [NonSerialized]
    private bool isLoaded = false;

    public bool IsLoaded
    {
        get
        {
            return isLoaded;
        }

        set
        {
            isLoaded = value;
            if (AdsController.NoADS)
            {
                if (activeWhenNoAds)
                {
                    if (load != null)
                        load(value);
                }
                else
                {
                    if (load != null)
                        load(false);
                }
            }
            else
            {
                if (load != null)
                    load(value);
            }
        }
    }

    [NonSerialized]
    public bool InProcess = false;
}
