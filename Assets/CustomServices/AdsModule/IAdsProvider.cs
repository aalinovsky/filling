﻿using System;

namespace CustomService
{
    public interface IAdsProvider
    {
        event Action<string> BannerClick;
        event Action InterstitialShow;
        event Action<string> InterstitialShown;
        event Action<string> InterstitialClick;
        event Action InterstitialClosed;
        event Action<string> RewardedShown;
        event Action<string> RewardedComplete;
        event Action<string> RewardedClick;
        event Action RewardedClose;

        /// <summary>
        /// Initialize module at the start of application
        /// </summary>
        void InitModule();

        /// <summary>
        /// Set ads enabed or not enabled (if user bought removeAds iap)
        /// </summary>
        void SetAdsEnable(bool enable);

        /// <summary>
        /// Indicate banner state
        /// </summary>
        bool IsBannerHidden(string placement);

        /// <summary>
        /// Show banner
        /// </summary>
        void ShowBanner(string placement);

        /// <summary>
        /// Hide banner
        /// </summary>
        void HideBanner(string placement);

        void CacheInterstitial(string placement);

        /// <summary>
        /// Return true if there is interstitial to show
        /// </summary>
        /// <returns></returns>
        bool IsInterstitialReady(string placement);

        /// <summary>
        /// Show Interstitial. Return true if interstitial will be realy shown (placement can be disabled in config).
        /// Callback received the same bool parameter and it will be invoked when ads will be closed (or not shown)
        /// </summary>
        bool ShowInterstitial(string placement);

        /// <summary>
        /// This method requests rewarded ads. It should be invoked after initialization and after completion of the current rewarded
        /// If rewarded loading failed the module themself will try to cache rewarded
        /// </summary>
        void CacheRewarded(string placement);

        /// <summary>
        /// Return true if there is rewarded ads to show
        /// </summary>
        bool IsRewardedReady(string placement);


        /// <summary>
        /// Show Rewarded.
        /// Callback  will be invoked when ads will be closed (or not shown). In current implementation callback always received true
        /// </summary>
        void ShowRewardedVideo(string placement);


        /// <summary>
        /// Set ads sounds muted or not
        /// </summary>
        void SetAdsSoundsMuted(bool mute);

    }
}