﻿using UnityEngine;
using System.Collections;
using CustomService;
using System;

namespace CustomService
{
    public class MoPubAdsProvider : MonoBehaviour, IAdsProvider
    {
        public string testAdUnit = "b195f8dd8ded45fe847ad89ed1d016da";
        [Space()]
        public AdsData data;
#if MO_PUB
        public MoPubBase.AdPosition bannerPosition;
#endif

        protected bool isActiveAds = false;
        protected static bool isInited = false;

        public AdsPlacement banner
        {
            get
            {
                return data.banner;
            }
        }
        public AdsPlacement[] interstitials
        {
            get
            {
                return data.interstitials;
            }
        }
        public AdsPlacement[] rewarded
        {
            get
            {
                return data.rewardeds;
            }
        }

        public event Action<string> BannerClick;
        public event Action InterstitialShow;
        public event Action<string> InterstitialShown;
        public event Action<string> InterstitialClick;
        public event Action InterstitialClosed;
        public event Action<string> RewardedShown;
        public event Action<string> RewardedComplete;
        public event Action<string> RewardedClick;
        public event Action RewardedClose;

        public void InitModule()
        {
            if (isInited)
                return;
#if MO_PUB
            InitCallbacks();
            MoPub.InitializeSdk(new MoPub.SdkConfiguration()
            {
                AdUnitId = banner.Key,

                LogLevel = MoPubBase.LogLevel.None,

                MediatedNetworks = new MoPubBase.MediatedNetwork[]
                {
                    new MoPub.SupportedNetwork.AdMob(),
                    //new MoPub.SupportedNetwork.Facebook(),
                    new MoPub.SupportedNetwork.Unity()
                }
            });
                               
            RequestInitPlacements();
            isInited = true;
            DevLogs.Log("Dev: MoPub.initAdsModule()", Color.green);
#endif
        }

        protected void RequestInitPlacements()
        {
#if MO_PUB
            string[] inters = new string[interstitials.Length];
            string[] rewards = new string[rewarded.Length];

            for (int i = 0; i < interstitials.Length; i++)
            {
                inters[i] = interstitials[i].Key;
            }

            for (int i = 0; i < rewarded.Length; i++)
            {
                rewards[i] = rewarded[i].Key;
            }

            MoPub.LoadBannerPluginsForAdUnits(new string[] { banner.Key });
            MoPub.LoadInterstitialPluginsForAdUnits(inters);
            MoPub.LoadRewardedVideoPluginsForAdUnits(rewards);
#endif
        }

        protected void RequestLoadPlacements()
        {
#if MO_PUB
            if (data.banner.enabled.Value)
                AdsController.Instance.SetActiveBanner(banner.enabled.Value, banner);

            for (int i = 0; i < interstitials.Length; i++)
            {
                CacheInterstitial(interstitials[i].Key);
            }

            for (int i = 0; i < rewarded.Length; i++)
            {
                CacheRewarded(rewarded[i].Key);
            }
#endif
        }

        protected void InitCallbacks()
        {
#if MO_PUB
            DevLogs.Log("Dev: Add event listeners to MoPub", Color.green);

            MoPubManager.OnSdkInitializedEvent += onSdkInited;

            MoPubManager.OnInterstitialClickedEvent += onInterstitialClicked;
            MoPubManager.OnInterstitialDismissedEvent += onInterstitialDismissed;
            MoPubManager.OnInterstitialExpiredEvent += interstitialDidExpire;
            MoPubManager.OnInterstitialFailedEvent += onInterstitialFailed;
            MoPubManager.OnInterstitialLoadedEvent += onInterstitialLoaded;
            MoPubManager.OnInterstitialShownEvent += onInterstitialShown;

            MoPubManager.OnRewardedVideoClickedEvent += onRewardedVideoClick;
            MoPubManager.OnRewardedVideoClosedEvent += onRewardedVideoClosed;
            MoPubManager.OnRewardedVideoExpiredEvent += onRewardedVideoExpired;
            MoPubManager.OnRewardedVideoFailedEvent += onRewardedVideoFailed;
            MoPubManager.OnRewardedVideoFailedToPlayEvent += onRewardedVideoFailedToPlay;
            MoPubManager.OnRewardedVideoLeavingApplicationEvent += onRewardedVideoLeavingApplication;
            MoPubManager.OnRewardedVideoLoadedEvent += onRewardedVideoLoaded;
            MoPubManager.OnRewardedVideoReceivedRewardEvent += onRewardedVideoReceivedReward;
            MoPubManager.OnRewardedVideoShownEvent += onRewardedVideoShown;
#endif
        }

        public bool IsBannerHidden(string placement)
        {
            return true;
        }

        public void SetAdsEnable(bool enable)
        {
#if MO_PUB
            isActiveAds = enable;
            if (enable && !MoPub.IsSdkInitialized)
            {
                InitModule();
            }

            if (!isActiveAds)
            {
                HideBanner(data.banner.Key);

                
            }
#endif
        }

        public void ShowBanner(string placement)
        {
#if MO_PUB
            MoPub.CreateBanner(banner.Key, bannerPosition);
            MoPub.ShowBanner(banner.Key, true);
            DevLogs.Log(string.Format("Dev: MoPub.bannerShow({0})", placement), Color.green);
#endif
        }

        public void HideBanner(string placement)
        {
#if MO_PUB
            MoPub.DestroyBanner(banner.Key);
            MoPub.ShowBanner(banner.Key, false);
            DevLogs.Log(string.Format("Dev: MoPub.bannerHide({0})", placement), Color.green);
#endif
        }

        public bool IsInterstitialReady(string placement)
        {
            bool result = false;
            if (!isActiveAds)
                return false;
#if MO_PUB
            AdsPlacement inter = InterPlacement(placement);
            result = inter.IsLoaded;
            DevLogs.Log(string.Format("Dev: IsInterstitialReady() = {0}, {1}", result, placement), Color.green);
            CacheInterstitial(placement);
#else
            result = false;
#endif
            return result;
        }

        public void CacheInterstitial(string placement)
        {
#if MO_PUB
            DevLogs.Log(string.Format("Dev: TryStart_CacheInterstitial, {0}", placement), Color.green);
            AdsPlacement inter = InterPlacement(placement);
            if (inter == null)
            {
                DevLogs.Log(string.Format("Dev: OnStart_CacheRewarded(PlacementNotFound), {0}", placement), Color.green);
            }
            else if (!inter.InProcess && !inter.IsLoaded)
            {
                InterPlacement(placement).InProcess = true;
                DevLogs.Log(string.Format("Dev: OnStart_CacheInterstitial, {0}", placement), Color.green);
                MoPub.RequestInterstitialAd(placement);
            }
#endif
        }

        public bool ShowInterstitial(string adKey)
        {
            bool result = false;
#if MO_PUB
            if (!isActiveAds)
                return false;
            AdsPlacement inter = InterPlacement(adKey);
            if (inter.IsLoaded)
            {
                result = true;
                InterstitialShow();
                MoPub.ShowInterstitialAd(adKey);
                DevLogs.Log(string.Format("Dev: ShowInterstitial, {0}", adKey), Color.green);
            }
            else
            {
                DevLogs.Log(string.Format("Dev: ShowInterstitial_fail, {0}", adKey), Color.green);
                CacheInterstitial(adKey);
            }
            return result;
#else
            return result;
#endif
        }

        //////////////////////Rewarded///////////////////////////////

        public void CacheRewarded(string placement)
        {
#if MO_PUB
            AdsPlacement rewarded = RewardedPlacement(placement);
            DevLogs.Log(string.Format("Dev: TryStart_CacheRewarded, {0}", placement), Color.green);
            if(rewarded == null)
            {
                DevLogs.Log(string.Format("Dev: OnStart_CacheRewarded(PlacementNotFound), {0}", placement), Color.green);
            }
            else if (!rewarded.IsLoaded && !rewarded.InProcess)
            {
                rewarded.InProcess = true;
                DevLogs.Log(string.Format("Dev: OnStart_CacheRewarded, {0}", placement), Color.green);
                MoPub.RequestRewardedVideo(placement);
            }
#endif
        }

        public bool IsRewardedReady(string placement)
        {
            bool result = false;
#if MO_PUB
            if (!isActiveAds)
                return false;
            result = MoPub.HasRewardedVideo(placement);
            AdsPlacement rewarded = RewardedPlacement(placement);
            rewarded.IsLoaded = result;
            DevLogs.Log(string.Format("Dev: IsRewardedReady({1}) = {0}", result, placement));
#else
            result = false;
#endif
            return result;
        }

        public void ShowRewardedVideo(string placement)
        {
#if MO_PUB
            MoPub.ShowRewardedVideo(placement);
            DevLogs.Log(string.Format("Dev: ShowRewardedVideo({0})", placement), Color.green);
#endif
        }

        public void SetAdsSoundsMuted(bool mute)
        {
#if MO_PUB
            DevLogs.Log(string.Format("Dev: SetAdsSoundsMuted({0})", mute), Color.green);
#endif
        }

        
        //////////////////////////Utils/////////////////////////////////
        
        protected AdsPlacement InterPlacement(string Key)
        {
            AdsPlacement placement = null;
            for (int i = 0; i < interstitials.Length; i++)
            {
                if (interstitials[i].Key.Equals(Key))
                {
                    placement = interstitials[i];
                    break;
                }
            }
            return placement;
        }

        protected AdsPlacement RewardedPlacement(string Key)
        {
            AdsPlacement placement = null;
            for (int i = 0; i < rewarded.Length; i++)
            {
                if (rewarded[i].Key.Equals(Key))
                {
                    placement = rewarded[i];
                    break;
                }
            }
            return placement;
        }

        /////////////////////////SDKcallbacks////////////////////////////////

        void onSdkInited(string msg)
        {
            DevLogs.Log(string.Format("[MOPUB_event] SDK INITED {0}", msg),Color.green);
            RequestLoadPlacements();
        }

        /////////////////////////bannercallbacks////////////////////////////////

#region banner_callbacks

        void OnBannerLoaded(string message)
        {
            DevLogs.Log(string.Format("OnBannerLoaded({0})", message), Color.green);
        }

        void OnBannerClick(string networkName)
        {
            DevLogs.Log(string.Format("OnBannerClick({0})", networkName), Color.green);
            if (BannerClick != null)
                BannerClick(networkName);
        }

#endregion

        /////////////////////////Interstitial CALLBACKS///////////////////////////

#region inter_callbacks
        void onInterstitialLoaded(string adUnitId)
        {
            DevLogs.Log(string.Format("[MOPUB_event] inter loaded {0}", adUnitId), Color.green);
            AdsPlacement placement = InterPlacement(adUnitId);
            if(placement != null)
            {
                placement.IsLoaded = true;
                placement.InProcess = false;
            }
        }

        void onInterstitialFailed(string adUnitId, string errorCode)
        {
            DevLogs.Log(string.Format("[MOPUB_event] inter failed {0} err = {1}", adUnitId, errorCode), Color.green);
#if MO_PUB
            AdsPlacement placement = InterPlacement(adUnitId);
            if (placement != null)
            {
                placement.IsLoaded = false;
                placement.InProcess = false;
                this.WaitAndStart(1.0f, () => { CacheInterstitial(adUnitId); });
            }
#endif
        }

        void onInterstitialDismissed(string adUnitId)
        {
            DevLogs.Log(string.Format("[MOPUB_event] inter Dismissed {0}", adUnitId), Color.green);
            AdsPlacement placement = InterPlacement(adUnitId);
            if (placement != null)
            {
                placement.IsLoaded = false;
                placement.InProcess = false;
                this.WaitAndStart(1.0f, () => { CacheInterstitial(adUnitId); });
            }
        }

        void interstitialDidExpire(string adUnitId)
        {
            DevLogs.Log(string.Format("[MOPUB_event] inter expired {0}", adUnitId), Color.green);
            AdsPlacement placement = InterPlacement(adUnitId);
            if (placement != null)
            {
                placement.IsLoaded = false;
                placement.InProcess = false;
                this.WaitAndStart(1.0f, () => { CacheInterstitial(adUnitId); });
            }
        }

        void onInterstitialShown(string adUnitId)
        {            
            DevLogs.Log(string.Format("[MOPUB_event] inter Shown {0}", adUnitId), Color.green);
#if MO_PUB
            AdsPlacement placement = InterPlacement(adUnitId);
            if (placement != null)
            {
                placement.IsLoaded = false;
                placement.InProcess = false;
                if (InterstitialShown != null)
                    InterstitialShown(adUnitId);
                this.WaitAndStart(1, () => { CacheInterstitial(adUnitId); });
            }
#endif                  

        }

        void onInterstitialClicked(string adUnitId)
        {
            DevLogs.Log(string.Format("[MOPUB_event] inter click {0}", adUnitId), Color.green);
            if (InterstitialClick != null)
                InterstitialClick(adUnitId);
        }
#endregion

        /////////////////////////Rewarded callbacks////////////////////////////////

#region rewarded_callbacks
        void onRewardedVideoClick(string adUnitId)
        {
            DevLogs.Log(string.Format("[MOPUB_event] rewarded click {0}", adUnitId), Color.green);
            if (RewardedClick != null)
                RewardedClick(adUnitId);
        }

        void onRewardedVideoLoaded(string adUnitId)
        {
            DevLogs.Log(string.Format("[MOPUB_event] rewarded loaded {0}", adUnitId), Color.green);
            AdsPlacement placement = RewardedPlacement(adUnitId);
            if (placement != null)
            {
                placement.IsLoaded = true;
                placement.InProcess = false;
            }
        }

        void onRewardedVideoFailed(string adUnitId, string errorMsg)
        {
            DevLogs.Log(string.Format("[MOPUB_event] rewarded failed {0}", adUnitId), Color.green);
            AdsPlacement placement = RewardedPlacement(adUnitId);
            if (placement != null)
            {
                placement.IsLoaded = false;
                placement.InProcess = false;
                this.WaitAndStart(1,()=> { CacheRewarded(adUnitId); });
            }
        }

        void onRewardedVideoExpired(string adUnitId)
        {
            DevLogs.Log(string.Format("[MOPUB_event] rewarded expired {0}", adUnitId), Color.green);
            AdsPlacement placement = RewardedPlacement(adUnitId);
            if (placement != null)
            {
                placement.IsLoaded = false;
                placement.InProcess = false;
                this.WaitAndStart(1, () => { CacheRewarded(adUnitId); });
            }
        }

        void onRewardedVideoShown(string adUnitId)
        {
            DevLogs.Log(string.Format("[MOPUB_event] rewarded shown {0}", adUnitId), Color.green);
            AdsPlacement placement = RewardedPlacement(adUnitId);
            if(placement != null)
            {
                placement.IsLoaded = false;
                placement.InProcess = false;
                this.WaitAndStart(1, () => { CacheRewarded(adUnitId); });
            }
            if (RewardedShown != null)
                RewardedShown(adUnitId);
        }

        void onRewardedVideoFailedToPlay(string adUnitId, string errorMsg)
        {
            DevLogs.Log(string.Format("[MOPUB_event] rewarded failed play {0} err = {1}", adUnitId, errorMsg), Color.green);
            AdsPlacement placement = RewardedPlacement(adUnitId);
            if (placement != null)
            {
                placement.IsLoaded = false;
                placement.InProcess = false;
                this.WaitAndStart(1, () => { CacheRewarded(adUnitId); });
            }
        }

        void onRewardedVideoReceivedReward(string p1, string p2, float amount)
        {
            DevLogs.Log(string.Format("[MOPUB_event] rewarded complete {0}", p1), Color.green);
            if (RewardedComplete != null)
                RewardedComplete("CompleteRewardedVideo");
        }

        void onRewardedVideoClosed(string adUnitId)
        {
            DevLogs.Log(string.Format("[MOPUB_event] rewarded closed {0}", adUnitId), Color.green);
            CacheRewarded(adUnitId);
        }

        void onRewardedVideoLeavingApplication(string adUnitId)
        {

        }
#endregion
    }
}
