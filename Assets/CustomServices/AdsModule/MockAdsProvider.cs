﻿using System;
using UnityEngine;

namespace CustomService
{
    public class MockAdsProvider : MonoBehaviour, IAdsProvider
    {
#pragma warning disable
        public event Action<string> BannerClick;
        public event Action InterstitialShow;
        public event Action<string> InterstitialShown;
        public event Action<string> InterstitialClick;
        public event Action InterstitialClosed;
        public event Action<string> RewardedShown;
        public event Action<string> RewardedComplete;
        public event Action<string> RewardedClick;
        public event Action RewardedClose;
#pragma warning restore
        private bool _isBannerHidden = true;


        public void InitModule()
        {
            Debug.Log("InitModule()");
        }

        public void HideBanner(string placement)
        {
            _isBannerHidden = true;
            Debug.Log("HideBanner()");
        }

        public bool IsInterstitialReady(string placement)
        {
            Debug.LogFormat("IsInterstitialReady()");
            return true;
        }

        public void CacheInterstitial(string placement)
        {
            Debug.LogFormat("CacheInterstitial()");
        }

        public void CacheRewarded(string placement)
        {
            Debug.LogFormat("CacheRewarded()");
        }

        public bool IsRewardedReady(string placement)
        {
            Debug.LogFormat("IsRewardedReady({0})", placement);
            return true;
        }

        public void SetAdsEnable(bool enable)
        {
            Debug.LogFormat("SetAdsEnable({0})", enable);
        }

        public bool ShowInterstitial(string placement)
        {
            Debug.LogFormat("ShowAdVideo({0}) = true", placement);
            if (InterstitialShow != null)
                InterstitialShow();

            if (InterstitialShown != null)
                InterstitialShown(placement);
            // this.InvokeAction(OnInterstitialClosed, 1);
            return true;
        }


        public void ShowBanner(string placement)
        {
            _isBannerHidden = false;
            Debug.LogFormat("ShowBanner({0})", placement);
        }

        public void ShowRewardedVideo(string placement)
        {
            Debug.LogFormat("ShowRewardedVideo({0})", placement);
            OnRewardedCompelte();
        }

        public void SetAdsSoundsMuted(bool mute)
        {
            Debug.LogFormat("SetAdsSoundsMuted({0})", mute);
        }

        public bool IsBannerHidden(string placement)
        {
            return _isBannerHidden;
        }

        void OnRewardedCompelte()
        {
            Debug.Log("OnRewardedCompelte()");
            if (RewardedComplete != null)
                RewardedComplete("");
        }

        void OnInterstitialClosed()
        {
            Debug.Log("OnInterstitialClosed()");
            if (InterstitialClosed != null)
                InterstitialClosed();
        }        
    }
}