﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CreateAssetMenu(fileName = "promo", menuName = "Game/Crosspromo/promo")]
public class PromoObject : ScriptableObject
{
    public Color btnColor;
    public LocalizedFrase appName;
    public LocalizedFrase promoText;
    public string bundleId;
    public Sprite promoImg;
}

//#if UNITY_EDITOR
//[CustomPropertyDrawer(typeof(PromoObject))]
//public class PlatformStyleDrawer : PropertyDrawer
//{
//    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
//    {
//        EditorGUI.BeginProperty(position, label, property);
//        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
//        int indent = EditorGUI.indentLevel;
//        EditorGUI.indentLevel = 0;

//        Rect rect1 = new Rect(
//            position.x,
//            position.y,
//            position.width,
//            EditorGUIUtility.singleLineHeight);
//        Rect rect2 = new Rect(
//            position.x,
//            position.y + EditorGUIUtility.singleLineHeight * 1,
//            position.width,
//            EditorGUIUtility.singleLineHeight);
//        Rect rect3 = new Rect(
//            position.x,
//            position.y + EditorGUIUtility.singleLineHeight * 2,
//            position.width,
//            EditorGUIUtility.singleLineHeight);
//        Rect rect4 = new Rect(
//            position.x,
//            position.y + EditorGUIUtility.singleLineHeight * 3,
//            position.width,
//            EditorGUIUtility.singleLineHeight);
//        Rect rect5 = new Rect(
//            position.x,
//            position.y + EditorGUIUtility.singleLineHeight * 4,
//            position.width,
//            EditorGUIUtility.singleLineHeight);
//        Rect rect6 = new Rect(
//            position.x,
//            position.y + EditorGUIUtility.singleLineHeight * 5,
//            position.width,
//            EditorGUIUtility.singleLineHeight);


//        EditorGUI.PropertyField(rect1, property.FindPropertyRelative("enableStatus"));//, GUIContent.none);
//        EditorGUI.PropertyField(rect2, property.FindPropertyRelative("btnColor"));//, GUIContent.none);
//        EditorGUI.PropertyField(rect3, property.FindPropertyRelative("appName"));//, GUIContent.none);
//        EditorGUI.PropertyField(rect4, property.FindPropertyRelative("promoText"));//, GUIContent.none);
//        EditorGUI.PropertyField(rect5, property.FindPropertyRelative("bundleId"));//, GUIContent.none);
//        EditorGUI.PropertyField(rect6, property.FindPropertyRelative("promoImg"));//, GUIContent.none);
//        EditorGUI.indentLevel = indent;
//        EditorGUI.EndProperty();
//    }

//    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
//    {
//        float extraHeight = EditorGUIUtility.singleLineHeight * 6;
//        return base.GetPropertyHeight(property, label) + extraHeight;
//    }

//}
//#endif
