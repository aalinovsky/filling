﻿using UnityEngine;
using System.Collections;
using TMPro;
using UnityEngine.UI;

public class PromoPanel : MonoBehaviour
{
    public Button watchCrosspromo;
    public Image promoImg;
    public Image btnImg;
    public TextMeshProUGUI appName;
    public TextMeshProUGUI promoDescription;

    protected PromoObject selected;

    private void Awake()
    {
        watchCrosspromo.onClick.AddListener(Open);
    }

    public void InitPromo(PromoObject obj)
    {
        selected = obj;
        promoImg.sprite = selected.promoImg;
        btnImg.color = selected.btnColor;
        appName.text = selected.appName.Value;
        promoDescription.text = selected.promoText.Value;
    }

    public void Open()
    {
        if (selected != null)
        {
#if UNITY_ANDROID && !UNITY_EDITOR
            Application.OpenURL("market://details?id=" + selected.bundleId);
#elif UNITY_IOS && !UNITY_EDITOR
            Application.OpenURL("itms-apps://itunes.apple.com/app/" + selected.bundleId);
#else
            DevLogs.Log("Click crosspromo " + selected.bundleId, Color.yellow);
#endif
        }
    }
}
