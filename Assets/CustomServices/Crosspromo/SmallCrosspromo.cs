﻿using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SmallCrosspromo : MonoBehaviour {

    public bool randomize = false;
    public bool isRepeate = false;  
    [Space()]
    public PromoPanel panel;
    public List<PromoObject> promotion = new List<PromoObject>();

    protected PromoObject selected;

    private void Awake()
    {
        if(promotion.Count == 0)
        {
            panel.gameObject.SetActive(false);
            return;
        }
        UpdatePromo();
    }

    int index = -1;
    public void UpdatePromo()
    {
        if (promotion.Count == 0)
        {        
            return;
        }

        if (!randomize)
        {
            index++;
            if (index >= promotion.Count)
            {
                index = 0;
            }
            selected = promotion[index];
        }
        else
        {
            PromoObject newPromo = promotion[UnityEngine.Random.Range(0, promotion.Count)];
            while(!isRepeate && promotion.Count > 1 && newPromo.Equals(selected))
            {
                newPromo = promotion[UnityEngine.Random.Range(0, promotion.Count)];
            }
            selected = newPromo;
        }
        panel.InitPromo(selected);
    }    
}