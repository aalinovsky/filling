﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GDPRController : MonoBehaviour
{
    public int mainSceneIndex = 1;
    public LoadSceneMode loadMode = LoadSceneMode.Single;

    private void Awake()
    {
        LoadMainScene();
    }

    protected void LoadMainScene()
    {
        StartCoroutine(LoadScene());
    }

    IEnumerator LoadScene()
    {
        Application.backgroundLoadingPriority = ThreadPriority.High;
        AsyncOperation AO = SceneManager.LoadSceneAsync(mainSceneIndex, loadMode);
        //AO.allowSceneActivation = false;
        while (AO.progress < 0.9f)
        {
            yield return null;
        }

        Application.backgroundLoadingPriority = ThreadPriority.Normal;
    }
}
