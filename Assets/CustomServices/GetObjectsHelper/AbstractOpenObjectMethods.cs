﻿using UnityEngine;
using System.Collections;
using CustomService.Analytics;
using CustomService;
using System.Collections.Generic;

public abstract class AbstractOpenObjectMethods : Singletone<AbstractOpenObjectMethods>
{
    public GetObjectType AvailableGetType(GetObjectSettings sett)
    {
        if (AdsController.NoADS && !sett.placement.activeWhenNoAds)
        {
            return GetObjectType.None;
        }

        if (!AdsManager.IsInternetAvailable)
        {
            EasyBrainAnalytics.SendEvent(AnalyticsSystems.Client, "g_rewarded_potential", new Dictionary<string, string>() {
                { "placement", sett.placement.Key },
                { "result", "error_offline" }
            });
        }

        switch (sett.getType)
        {
            case GetObjectType.Both:
                if (VideoIsReady(sett.placement))
                {
                    return GetObjectType.OnlyVideo;
                }
                if (HasCoins(sett.cost))
                {
                    return GetObjectType.OnlyCoins;
                }
                break;
            case GetObjectType.OnlyCoins:
                if (HasCoins(sett.cost))
                {
                    return GetObjectType.OnlyCoins;
                }
                break;
            case GetObjectType.OnlyVideo:
                if (VideoIsReady(sett.placement))
                {
                    return GetObjectType.OnlyVideo;
                }
                break;
        }
        return GetObjectType.None;
    }

    protected bool VideoIsReady(AdsPlacement placement)
    {
        bool ready = AdsController.Instance.RewardedIsReady(placement);
        //EasyBrainAnalytics.SendEvent(AnalyticsSystems.Client, "g_rewarded_potential", new Dictionary<string, string>() {
        //{ "placement", placement.key },
        //{ "result", ready ? "impression_cached" : "error_onload" }
        //});
        return ready;
    }

    protected abstract bool HasCoins(int cost);
}

[System.Serializable]
public class GetObjectSettings
{
    public GetObjectType getType;
    public AdsPlacement placement;
    public int cost;
}

public enum GetObjectType
{
    Both,
    OnlyCoins,
    OnlyVideo,
    None
}