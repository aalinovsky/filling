﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;
#if GA
using GameAnalyticsSDK;
#endif
#if FB
using Facebook.Unity;
#endif

public class Analytics : Singletone<Analytics>
{
    public void Start()
    {
#if FB
        FB.Init();
#endif
    }

    public static void SendEvent(string eventId, float value, Dictionary<string, object> args)
    {
        SendEvent(AnalyticsSystems.All, eventId, value, args);
    }

    public static void SendEvent(AnalyticsSystems analyticsSystems, string eventId, float value,
        Dictionary<string, object> args)
    {
        string json = args != null ? DictionaryToJson(args) : null;
        DevLogs.LogEvent(string.Format("Dev : Event->{0},{1},val={2},{3}", analyticsSystems, eventId,value, json));

        switch (analyticsSystems)
        {
            case AnalyticsSystems.EventOptionFacebook:
                FacebookEvent(eventId, value, args);
                break;
            case AnalyticsSystems.EventOptionAppMetrica:
                AppMetricEvent(eventId, args);
                break;
            case AnalyticsSystems.EventOptionGameAnalytic:
#if GA
                GameAnalytics.NewDesignEvent(eventId, value);
#endif
                break;
        }
    }

    public static void LevelEvent(AnalyticsSystems analyticsSystems, ProgressionType progress, int level, int score)
    {
        DevLogs.LogEvent(string.Format("Dev : Event->{0},Level {2} was {1},Score:{3}", analyticsSystems, progress, level, score));
        switch (analyticsSystems)
        {
            case AnalyticsSystems.EventOptionGameAnalytic:
#if GA
                GAProgressionStatus status = GAProgressionStatus.Start;
                switch (progress)
                {
                    case ProgressionType.Start:
                        status = GAProgressionStatus.Start;
                        break;
                    case ProgressionType.Fail:
                        status = GAProgressionStatus.Fail;
                        break;
                    case ProgressionType.Complete:
                        status = GAProgressionStatus.Complete;
                        break;
                }

                GameAnalytics.NewProgressionEvent(status, "game", "level", level.ToString(), score);                
#endif
                break;
        }
    }

    public static void FacebookEvent(string eventId, float value, Dictionary<string, object> args)
    {
#if FB
        FB.LogAppEvent(eventId, value, args);
#endif
    }

    public static void AppMetricEvent(string eventId, Dictionary<string, object> args)
    {
#if YM
        AppMetrica.Instance.ReportEvent(eventId, args);
#endif
    }

    static string DictionaryToJson(Dictionary<string, object> args)
    {
        int count = args.Count;
        if (count == 0)
            return null;
        var builder = new StringBuilder("{");
        using (var enumerator = args.GetEnumerator())
        {
            for (int i = 0; i < count; i++)
            {
                enumerator.MoveNext();
                var pair = enumerator.Current;
                builder.AppendFormat("\"{0}\":\"{1}\"", pair.Key, pair.Value);
                if (i != count - 1)
                    builder.Append(",");
            }
        }

        builder.Append("}");

        return builder.ToString();
    }
}

public enum ProgressionType
{
    Start,
    Fail,
    Complete
}

[Flags]
public enum AnalyticsSystems
{
    EventOptionAdjust = 1 << 0, // bits: 0001
    EventOptionFacebook = 1 << 1, // bits: 0010
    EventOptionFirebase = 1 << 2, // bits: 0100
    EventOptionGameAnalytic = 1 << 3, // bits: 1000
    EventOptionAppMetrica = 1 << 4, // bits: 1000
    Client = EventOptionAdjust | EventOptionFirebase,
    All = EventOptionAdjust | EventOptionFacebook | EventOptionFirebase
}
