﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppsFlyerController : MonoBehaviour
{
    public bool enabledSDK = false;
         
    public string dev_key = "8MAzUC3B2BHYVi2uYVHaSd";
    [Space()]
    public string ios_app_id = "1448992978";
    public string androidPackage = "com.alinouski.";

    void Awake()
    {
        if (enabledSDK)
        {
#if AF
            AppsFlyer.setAppsFlyerKey(dev_key);
            #if UNITY_IOS
                  AppsFlyer.setAppID (ios_app_id);
                  AppsFlyer.trackAppLaunch ();
            #elif UNITY_ANDROID
                /* Mandatory - set your Android package name */
                AppsFlyer.setAppID(androidPackage);
                /* For getting the conversion data in Android, you need to add the "AppsFlyerTrackerCallbacks" listener.*/
                AppsFlyer.init(dev_key, "AppsFlyerTrackerCallbacks");
            #endif
#endif
        }
    }

    public static void Purchase(string content_id, string content_type, string cost)
    {
#if AF
        cost = cost.Replace(',','.');
        AppsFlyer.trackRichEvent(AFInAppEvents.LEVEL_ACHIEVED, new Dictionary<string, string>(){
            {AFInAppEvents.CONTENT_ID, content_id},
            {AFInAppEvents.CONTENT_TYPE, content_type},
            {AFInAppEvents.REVENUE, cost},
            {AFInAppEvents.CURRENCY, "USD"}
        });
#endif
    }
}
