﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using UnityEngine;

namespace CustomService.Analytics
{
    public static class EasyBrainAnalytics
    {
#if UNITY_ANDROID && MODULE
        private static AndroidJavaClass _nativeClass;
#endif
        public static void SendEvent(string eventId, Dictionary<string, string> args)
        {
            SendEvent(AnalyticsSystems.All, eventId, args);
        }

        public static void SendEvent(AnalyticsSystems analyticsSystems, string eventId,
            Dictionary<string, string> args)
        {
            string json = args != null ? DictionaryToJson(args) : null;
            DevLogs.LogEvent(string.Format("{0},{1},{2}", analyticsSystems, eventId, json));

#if UNITY_EDITOR || !MODULE

#elif UNITY_IOS
       EasyBrainAnalyticsIOS.iosLogEventNameParamsJSON((int)analyticsSystems,eventId,json);
#elif UNITY_ANDROID
            if (_nativeClass == null)
            {
                _nativeClass = new AndroidJavaClass("com.easybrain.analytics.Analytics");
            }
            _nativeClass.CallStatic("AnalyticsSendEvent",(int)analyticsSystems,eventId,json);
#else
            throw new NotImplementedException();
#endif
        }


        static string DictionaryToJson(Dictionary<string, string> args)
        {
            int count = args.Count;
            if (count == 0)
                return null;
            var builder = new StringBuilder("{");
            using (var enumerator = args.GetEnumerator())
            {
                for (int i = 0; i < count; i++)
                {
                    enumerator.MoveNext();
                    var pair = enumerator.Current;
                    builder.AppendFormat("\"{0}\":\"{1}\"", pair.Key, pair.Value);
                    if (i != count - 1)
                        builder.Append(",");
                }
            }

            builder.Append("}");

            return builder.ToString();
        }
    }

#if UNITY_IOS && MODULE
    static class EasyBrainAnalyticsIOS
    {
        private const string __module__ = "__Internal";

        [DllImport(__module__)]
        public static extern void iosLogEventNameParamsJSON(int analyticsSystems, string eventId, string paramsJson);
    }
#endif    
}