﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if GA
using GameAnalyticsSDK;
#endif

public class GameAnalyticsInitialize : MonoBehaviour {

    private void Awake()
    {
#if GA
        GameAnalytics.Initialize();
#endif
    }
}
