﻿using UnityEngine;
using System.Collections;
using System;

public class AndroidNotificationProvider : INotificationProvider
{
    protected int notificationId = 1;

    public void Init()
    {
        DevLogs.Log("Dev: NotificationManager => init");
#if UNITY_ANDROID
#endif
    }

    public void CancelAll()
    {
        notificationId = 1;
        DevLogs.Log("Dev: NotificationManager => cancel all");
#if UNITY_ANDROID && NOTIFICATIONS
        LocalNotification.ClearNotifications();
#endif
    }

    public void Schedule(DateTime date, string content, string tittle)
    {
        DevLogs.Log("Dev: NotificationManager => Schedule");
        ScheduleNotification(date, tittle, content);
    }

    
    protected void ScheduleNotification(DateTime notificationTime, string title, string message)
    {
#if UNITY_ANDROID && NOTIFICATIONS
        LocalNotification.SendNotification(notificationId, notificationTime - DateTime.Now, title, message, new Color32(0xff, 0x44, 0x44, 255), true, true);
#endif
        notificationId++;
    }
}
