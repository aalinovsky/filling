﻿using System;

public interface INotificationProvider
{
    void Init();
    void CancelAll();
    void Schedule(DateTime date, string content, string tittle);
}
