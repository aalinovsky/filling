﻿using UnityEngine;
using System.Collections;
using System;
using DefaultUtility;

public class DesktopNotificationProvider : INotificationProvider
{
    public void CancelAll()
    {
#if DEV_AN
        DevLogs.Log("Dev: DesktopNotificationProvider -> cancel all notifications".SetColor(Color.cyan));
#endif
    }

    public void Init()
    {
#if DEV_AN
        DevLogs.LogFormat("Dev: DesktopNotificationProvider -> init notification service".SetColor(Color.cyan));
#endif
    }

    public void Schedule(DateTime date, string content, string tittle)
    {
#if DEV_AN
        DevLogs.LogFormat("Dev: DesktopNotificationProvider -> reg notification\ndate:{0}\ncontent:{1}\ntittle:{2}", date, content, tittle);
#endif
    }
}
