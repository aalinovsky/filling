﻿using UnityEngine;
using System.Collections;
using System;
#if UNITY_IOS
using UnityEngine.iOS;
#endif

public class IosNotificationProvider : INotificationProvider
{

    public void Init()
    {
#if DEV_AN
        DevLogs.LogFormat("Dev: NotificationManager => init");
#endif
#if UNITY_IOS && NOTIFICATIONS
        UnityEngine.iOS.NotificationServices.RegisterForNotifications(NotificationType.Alert | NotificationType.Badge | NotificationType.Sound);
#endif
    }

    public void CancelAll()
    {
#if DEV_AN
        DevLogs.LogFormat("Dev: NotificationManager => cancel all");
#endif
#if UNITY_IOS && NOTIFICATIONS
        UnityEngine.iOS.NotificationServices.CancelAllLocalNotifications();
#endif
    }

    public void Schedule(DateTime date, string content, string tittle)
    {
#if DEV_AN
        DevLogs.LogFormat("Dev: NotificationManager => Schedule");
#endif
        ScheduleNotification(date, tittle, content);
    }

    protected void ScheduleNotification(DateTime notificationTime, string title, string message)
    {
#if UNITY_IOS && NOTIFICATIONS
        UnityEngine.iOS.LocalNotification notification = new UnityEngine.iOS.LocalNotification();

        notification.fireDate = notificationTime;
        notification.alertBody = message;
        notification.alertAction = "Alert";
        notification.hasAction = false;

        UnityEngine.iOS.NotificationServices.ScheduleLocalNotification(notification);
#endif

    }
}
