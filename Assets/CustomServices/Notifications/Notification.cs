﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using UnityEngine.Events;
using System;

[CreateAssetMenu(fileName = "notification", menuName = "Service/Notifications/Notification")]
public class Notification : ScriptableObject , INotifiaction
{    
    public Condition IsNeedEvent;

    public bool IsNeedToSchedule()
    {
        if (IsNeedEvent.target == null)
            return false;

        return IsNeedEvent.Invoke();
    }
}

[Serializable]
public class Condition : SerializableCallback<bool> { }