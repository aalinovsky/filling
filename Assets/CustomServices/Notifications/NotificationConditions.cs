﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "NotificationConditions", menuName = "Service/Notifications/Conditions")]
public class NotificationConditions : ScriptableObject
{
    public float percentFormissionCondition = 90.0f;

    public bool AlwaysTrue()
    {
        return true;
    }

    public bool HasCoinsForSkin()
    {
        //int coins = ColorSettings.Game.Coins.Get();
        //foreach (Skin s in ColorSettings.Data.skins.objectsList)
        //{
        //    if (s.openObjectMethod == OpenMethod.Coins && s.coins.coinsCost <= coins)
        //    {
        //        return true;
        //    }
        //}
        return false;
    }

    public bool CloseToHighScore()
    {
        //return ColorSettings.Game.CloseToBestScore.Get();
        return false;
    }

    public bool CloseToCompleteMission()
    {
        //foreach (CollectMission cm in MissionManager.Instance.mis)
        //{
        //    if (!cm.IsCompleted && cm.GetProgressPercents() >= percentFormissionCondition)
        //        return true;
        //}
        return false;
    }
}
