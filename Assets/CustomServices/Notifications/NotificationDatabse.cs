﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu(fileName = "NotificationDatabase", menuName = "Service/Notifications/Database")]
public class NotificationDatabse : ScriptableObject
{
    public NotificationDay[] notificationDays;
}

[System.Serializable]
public struct NotificationDay
{
    public int dayNumber;

    public NotificationContainer[] containers;

    public LocalizedFrase GetContent()
    {

        for (int i = 0; i < containers.Length; i++)
        {
            if (containers[i].notification.IsNeedToSchedule())
            {
                return containers[i].frases[UnityEngine.Random.Range(0, containers[i].frases.Length)];
            }
        }

        return null;
    }
}

[System.Serializable]
public struct NotificationContainer
{
    public Notification notification;
    public LocalizedFrase[] frases;
}
