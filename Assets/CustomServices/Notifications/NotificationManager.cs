﻿using UnityEngine;
using System.Collections;
using DefaultUtility;
using System;
using System.Collections.Generic;

public class NotificationManager : Singletone<NotificationManager>
{
    public Vector2 allowTimeSpace = new Vector2(10, 22);
    public int zeroDayScheduleHour = 3;
    public int defaultScheduleHour = 19;
    public NotificationDatabse db;
    protected INotificationProvider provider;

    public INotificationProvider Provider
    {
        get
        {
            if (provider == null)
                InitProvider();
            return provider;
        }
    }

    private void Awake()
    {
        InitProvider();
        Init();
    }

    void Start()
    {
        UpdateAllNotification();
    }

    protected virtual void InitProvider()
    {
#if UNITY_IOS && !UNITY_EDITOR
        provider = new IosNotificationProvider();
#elif UNITY_ANDROID && !UNITY_EDITOR
        provider = new AndroidNotificationProvider();
#else
        provider = new DesktopNotificationProvider();
#endif
    }

    public void Init()
    {
        if (Provider != null)
            Provider.Init();
    }

    public void UpdateAllNotification()
    {
        if(Provider != null)
            Provider.CancelAll();
        day1Frase = "";
        if (db != null)
        {
            foreach (NotificationDay nd in db.notificationDays)
            {
                int findFraseAttempts = 0;
                LocalizedFrase frase = nd.GetContent();
                while (day1Frase.Equals(frase.Value) && findFraseAttempts < 5)
                {
                    frase = nd.GetContent();
                    findFraseAttempts++;
                }
                RegitserNotification(nd.dayNumber, frase);
            }
        }
        DevLogs.Log("Dev: NotificationManager => Notification registration complete");
    }

    protected string day1Frase = "";

    protected void RegitserNotification(int day, LocalizedFrase contentFrase)
    {
        DevLogs.Log(string.Format("Dev: NotificationManager => Start register day:{0}", day));

        if (contentFrase == null)
        {
            DevLogs.Log(string.Format("Dev: NotificationManager => Error day:{0} no message", day));
            return;
        }
        bool isAllowableTime = IsAllowableTime(DateTime.Now);

        DateTime scheduleNotifTime = DateTime.Now;

        if (day == 0)
        {
            if (isAllowableTime)
            {
                scheduleNotifTime = scheduleNotifTime.AddHours(zeroDayScheduleHour);
                if (Provider != null && IsAllowableTime(scheduleNotifTime))
                {                    
                    Provider.Schedule(scheduleNotifTime, contentFrase.Value, "");
                    DevLogs.Log(string.Format("Dev: NotificationManager => register day:{0} tittle:{1} date:{2}", day, contentFrase.Value, scheduleNotifTime));
                }
            }
        }
        else
        {            
            if(day == 1)
            {
                day1Frase = contentFrase.Value;
            }

            if (isAllowableTime)
            {
                scheduleNotifTime = scheduleNotifTime.AddDays(day);

                if (Provider != null)
                    Provider.Schedule(scheduleNotifTime, contentFrase.Value, "");
            }
            else
            {
                scheduleNotifTime = scheduleNotifTime.AddHours(23 - scheduleNotifTime.Hour + defaultScheduleHour);
                scheduleNotifTime = scheduleNotifTime.AddDays(day - 1);
                scheduleNotifTime = scheduleNotifTime.AddMinutes(60 - scheduleNotifTime.Minute);
                if (Provider != null)
                    Provider.Schedule(scheduleNotifTime, contentFrase.Value, "");
            }
            DevLogs.Log(string.Format("Dev: NotificationManager => register day:{0} tittle:{1} date:{2}", day, contentFrase.Value, scheduleNotifTime));
        }
    }

    protected bool IsAllowableTime(DateTime time)
    {
        if (time.Hour < allowTimeSpace.x || time.Hour >= allowTimeSpace.y)
            return false;
        return true;
    }
}
