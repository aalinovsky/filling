﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PrivacyButtons : MonoBehaviour
{
    public ServiceData data;

    public Button[] privacy;
    public Button[] terms;

    private void Start()
    {
        for (int i = 0; i < privacy.Length; i++)
        {
            privacy[i].onClick.AddListener(Privacy);
        }

        for (int i = 0; i < terms.Length; i++)
        {
            terms[i].onClick.AddListener(Terms);
        }
    }

    public void Privacy()
    {
        this.OpenUrl(data.privacyUrl);
    }

    public void Terms()
    {
        this.OpenUrl(data.termsUrl);
    }
}
