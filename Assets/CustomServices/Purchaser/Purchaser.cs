﻿using CustomService;
using System;
using System.Collections.Generic;
using UnityEngine;
#if IN_APP
using UnityEngine.Purchasing;
#endif

public class Purchaser : Singletone<Purchaser>
#if IN_APP
    , IStoreListener
#endif
{
#if IN_APP
    private static IStoreController m_StoreController;          // The Unity Purchasing system.
    private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.
#endif

    public BuyProduct NoAds;

    void Start()
    {
#if IN_APP
        if (m_StoreController == null)
        {
            InitializePurchasing();
        }
#endif
    }

    #if IN_APP
    public void InitializePurchasing()
    {
        if (IsInitialized())
        {
            return;
        }
        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

        builder.AddProduct(NoAds.Key, ProductType.NonConsumable);
        UnityPurchasing.Initialize(this, builder);
    }

    private bool IsInitialized()
    {
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }
#endif

    public void BuyConsumable(string productKey)
    {
        BuyProductID(productKey);
    }

    public void BuyNoAds()
    {
        BuyProductID(NoAds.Key);
    }

    void BuyProductID(string productId)
    {
#if IN_APP
        if (IsInitialized())
        {
            Product product = m_StoreController.products.WithID(productId);

            if (product != null && product.availableToPurchase)
            {
                DevLogs.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                m_StoreController.InitiatePurchase(product);
            }
            else
            {
                DevLogs.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
            }
        }
        else
        {
            DevLogs.Log("BuyProductID FAIL. Not initialized.");
        }
#endif
    }

    public void RestorePurchases()
    {
#if IN_APP
        if (!IsInitialized())
        {
            DevLogs.Log("RestorePurchases FAIL. Not initialized.");
            return;
        }

        if (Application.platform == RuntimePlatform.IPhonePlayer ||
            Application.platform == RuntimePlatform.OSXPlayer)
        {
            DevLogs.Log("RestorePurchases started ...");

            var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
            apple.RestoreTransactions((result) => {
                DevLogs.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
            });
        }
        else
        {
            DevLogs.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
        }
#endif
    }

#if IN_APP
    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        DevLogs.Log("OnInitialized: PASS");

        m_StoreController = controller;
        m_StoreExtensionProvider = extensions;
    }


    public void OnInitializeFailed(InitializationFailureReason error)
    {
        // Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
        DevLogs.Log("OnInitializeFailed InitializationFailureReason:" + error);
    }
#endif

#if IN_APP
    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        if (String.Equals(args.purchasedProduct.definition.id, NoAds.Key, StringComparison.Ordinal))
        {
            DevLogs.Log(string.Format("ProcessPurchase: PASS. NoAds: '{0}'", args.purchasedProduct.definition.id));
            if (!AdsController.NoADS)
            {
                Analytic.Instance.BuyNoAds();
            }
            AdsController.NoADS = true;    
        }
        return PurchaseProcessingResult.Complete;
    }
#endif

#if IN_APP
    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        // A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
        // this reason with the user to guide their troubleshooting actions.
        DevLogs.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
    }
#endif
}

[System.Serializable]
public class BuyProduct
{
    [Space()]
    public string cost;
    [SerializeField]
    protected string keyIos;
    [SerializeField]
    protected string keyAndroid;
    [SerializeField]
    protected string keyDesktop;

    public string Key
    {
        get
        {
#if UNITY_EDITOR
            return keyDesktop;
#elif UNITY_IOS
            return keyIos;
#elif UNITY_ANDROID
            return keyAndroid;
#endif
        }
    }
}