﻿using UnityEngine;
using System.Collections;

public class RateController : MonoBehaviour
{
    public ServiceData data;
    public static bool NeedShow = false;

    public void Rate()
    {
        RateController.IsRated = true;
#if UNITY_EDITOR
        DevLogs.Log("Application is rated", Color.yellow);
#elif UNITY_ANDROID
        Application.OpenURL("market://details?id=" + Application.identifier);
#elif UNITY_IOS
        Application.OpenURL(string.Format("itms-apps://itunes.apple.com/app/id{0}", data.appleId));
#endif
    }

    public void Rate(int stars)
    {
        //To do analytic
        Rate();
    }

    public static bool IsRated
    {
        get
        {
            return PlayerPrefsUtility.GetBool("GameIsRated", false);
        }

        set
        {
            PlayerPrefsUtility.SetBool("GameIsRated", value);
        }
    }
}
