﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RateStarsPanel : MonoBehaviour
{
    public Sprite selected;
    public Sprite disabled;
    public Button[] stars = new Button[5];
    
    protected int currentRate = 0;
    protected bool isRated = false;

    public bool IsRated
    {
        get
        {
            return isRated;
        }
    }

    public int RateNumber
    {
        get
        {
            return currentRate + 1;
        }
    }

    private void Awake()
    {
        Clear();
        InitButtons();
    }

    protected void InitButtons()
    {
        stars[0].onClick.AddListener(() => { Rate(0); });
        stars[1].onClick.AddListener(() => { Rate(1); });
        stars[2].onClick.AddListener(() => { Rate(2); });
        stars[3].onClick.AddListener(() => { Rate(3); });
        stars[4].onClick.AddListener(() => { Rate(4); });
    }

    public void Reset()
    {
        currentRate = 0;
        isRated = false;
        Clear();
    }

    public void Rate(int index)
    {
        currentRate = index;
        isRated = true;
        SelectStars(index);
    }

    protected void SelectStars(int count)
    {
        Clear();
        for (int i = 0; i < stars.Length; i++)
        {
            if (i > count)
            {
                break;
            }
            else
            {
                stars[i].image.sprite = selected;
            }
        }
    }

    protected void Clear()
    {
        for (int i = 0; i < stars.Length; i++)
        {
            stars[i].image.sprite = disabled;
        }
    }
}
