﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "ServiceData", menuName = "Service/Data")]
public class ServiceData : ScriptableObject
{
    public string privacyUrl = "https://alinouski.github.io/privacypolicy.html";
    public string termsUrl = "https://alinouski.github.io/termsofuse.html";

    [Header("IOS market")]
    public string appleId;
    public string leaderboardId;

    [Header("Rate dialog")]
    public int showRateDialogOnLevel = 3;   

    private bool isShowRateToday = false;

    public bool IsShowRate
    {
        get
        {
            return isShowRateToday;
        }

        set
        {
            isShowRateToday = value;
        }
    }
}
