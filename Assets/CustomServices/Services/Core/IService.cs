﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IService {
    void Login();
    void Logout();
    void ShowTop();
    void SetTop(int i);
    void ShowAchivs();
    void Rate();
    void Share();
    void Share(string message);
    void Share(string message, string textureName);
}
