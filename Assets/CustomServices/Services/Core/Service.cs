﻿using UnityEngine;
using System.Collections;

public abstract class Service : MonoBehaviour, IService
{
    public abstract void Login();

    public abstract void Logout();

    public abstract void Rate();

    public abstract void SetTop(int i);

    public abstract void Share();

    public abstract void Share(string message);

    public abstract void Share(string message, string textureName);

    public abstract void ShowAchivs();

    public abstract void ShowTop();
}
