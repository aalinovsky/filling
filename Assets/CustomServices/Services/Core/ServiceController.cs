﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ServiceController : Singletone<ServiceController>, IService {

    public bool autoLogin;
    public AppleService ios;
    public AndroidService android;
    public Service desktop;

    [Space()]
    public Button[] showTopButtons;


    protected IntStorageData shareCount;

    public int ShareCount
    {
        get
        {
            InitShareCounter();
            return shareCount.Value;
        }

        set
        {
            InitShareCounter();
            shareCount.Value = value;
        }
    }

    public static bool RateState
    {
        get
        {
            return PlayerPrefsUtility.GetBool("RateState", true);
        }

        set
        {
            PlayerPrefsUtility.SetBool("RateState", value);
        }
    }

    private void Awake()
    {
        InitShareCounter();
    }

    protected void InitShareCounter()
    {
        if (shareCount == null)
            shareCount = new IntStorageData("shareCount");
    }

    private static IService service = null;

    void Start()
    {
        foreach (var btn in showTopButtons)
        {
            btn.onClick.AddListener(ShowTop);
        }

#if UNITY_EDITOR
        service = desktop;
#elif UNITY_ANDROID
        service = android;
#elif UNITY_IOS
        service = ios;    
#endif
        if(autoLogin)
            Login();
    }

    public virtual void Init(IService serv)
    {
        service = serv;
    }

    public virtual void Login()
    {
        if (service != null)
            service.Login();
    }

    public virtual void Logout()
    {
        if (service != null)
            service.Logout();
    }

    public virtual void ShowTop()
    {
        if (service != null)
            service.ShowTop();
    }

    public virtual void SetTop(int i)
    {
        if (service != null)
            service.SetTop(i);
    }

    public virtual void ShowAchivs()
    {
        if (service != null)
            service.ShowAchivs();
    }

    public virtual void Rate()
    {
        if (service != null)
        {
            service.Rate();
            RateState = true;      
        }
    }

    public virtual void Share()
    {
        if (service != null)
        {
            service.Share();
            ShareCount++;
        }
    }

    public void Share(string message)
    {
        if (service != null)
        {
            service.Share(message);
            ShareCount++;
        }
    }

    public void Share(string message, string textureName)
    {
        if (service != null)
        {
            service.Share(message, textureName);
            ShareCount++;
        }
    }
}
