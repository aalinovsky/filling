﻿#if UNITY_ANDROID && GPGS
    using GooglePlayGames;
    using GooglePlayGames.BasicApi;
    using UnityEngine.SocialPlatforms;
#endif
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class AndroidService : Service {

    public ServiceData data;
    public GameObject loginToGpgsButton;
    public GameObject logoutToGpgsButton;
    [Header("Symbol {0} is highscore")]
    public string sharePattern;
    public LocalizedFrase[] shareFrases;

    private string shareTextMesage = "";

    public override void Login()
    {
#if UNITY_ANDROID && GPGS
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()     
        // requests a server auth code be generated so it can be passed to an
        //  associated back end server application and exchanged for an OAuth token.
        //.RequestServerAuthCode(false)
        // requests an ID token be generated.  This OAuth token can be used to
        //  identify the player to other services such as Firebase.
        //.RequestIdToken()
        .Build();

        PlayGamesPlatform.InitializeInstance(config);
        // recommended for DevLogsging:
        PlayGamesPlatform.DebugLogEnabled = true;
        // Activate the Google Play Games platform
        PlayGamesPlatform.Activate();
        Social.localUser.Authenticate((bool success) =>
        {
            if (loginToGpgsButton != null)
            {
                loginToGpgsButton.SetActive(!success);
            }
            if (logoutToGpgsButton != null)
            {
                logoutToGpgsButton.SetActive(success);
            }
        Debug.Log("Social user auth state is:" + success.ToString());
        });
#endif
    }

    public override void Logout()
    {
#if UNITY_ANDROID && GPGS
        PlayGamesPlatform.Instance.SignOut();
#endif
    }

    public override void ShowTop()
    {
#if UNITY_ANDROID && GPGS
        PlayGamesPlatform.Instance.ShowLeaderboardUI(GPGSIds.leaderboard_top_level);
#endif
    }

    public override void SetTop(int i)
    {
#if UNITY_ANDROID && GPGS
		    Social.ReportScore(i, GPGSIds.leaderboard_top_level, (bool success) => {
            // handle success or failure
		    });
#endif
    }

    public override void ShowAchivs()
    {
        
    }

    protected virtual string GetLoadUrl()
    {
        return string.Format("https://play.google.com/store/apps/details?id={0}", Application.identifier);
    }

    public override void Rate()
    {
        Application.OpenURL("market://details?id=" + Application.identifier);
    }

    public override void Share()
    {
        CreateText();
        AndroidShare(CreateText(), "");
    }

    public override void Share(string message)
    {
        AndroidShare(message, "");
    }

    public override void Share(string message, string filename)
    {
        AndroidShare(message, filename);
    }

    public string CreateText()
    {
#if UNITY_ANDROID
        List<string> arr = new List<string>();

        arr.Add(GameSettings.TopLevelNumber.Value.ToString());//Not impl
        arr.AddRange(shareFrases.Select(s => s.Value).ToArray());

        shareTextMesage = string.Format(sharePattern, arr.ToArray());
        shareTextMesage = string.Concat(shareTextMesage, "\n", GetLoadUrl());
#endif
        Debug.Log(shareTextMesage);
        return shareTextMesage;
    }

    void AndroidShare(string message, string filename)
    {
#if UNITY_ANDROID       
        if (!Application.isEditor)
        {
            string destination = Path.Combine(Application.persistentDataPath, filename);

            AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
            AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");
            intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));

            if (!string.IsNullOrEmpty(filename))
            {
                AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
                AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse", "file://" + destination);
                intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);
            }

            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), message);
            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_SUBJECT"), Application.productName);

            intentObject.Call<AndroidJavaObject>("setType", "image/jpeg");
            AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");

            currentActivity.Call("startActivity", intentObject);
            // option two WITH chooser:
            /*AndroidJavaObject jChooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, "YO BRO! WANNA SHARE?");
			currentActivity.Call("startActivity", jChooser);*/
        }
#endif
    }
}