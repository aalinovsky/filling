﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using System.IO;
using DefaultUtility;
using UnityEditor;
#if UNITY_IOS && GC
using UnityEngine.SocialPlatforms;
using UnityEngine.SocialPlatforms.GameCenter;
#endif

public class AppleService : Service {

    [Header("ITunce")]
    public ServiceData data;
    [Header("Symbol {0} is highscore")]
    public string sharePattern;
    public LocalizedFrase[] shareFrases;

    private string shareTextMesage;
    
    public override void Login()
    {
#if UNITY_IOS && GC
            Social.localUser.Authenticate ( 
            success => { 
            if (success) 
            { 
                //DevLogs.Log("==iOS GC authenticate OK");
            } 
            else
            {
                //DevLogs.Log("==iOS GC authenticate Failed");
            } } );
#endif
    }

    public override void ShowTop()
    {
#if UNITY_IOS && GC
        GameCenterPlatform.ShowLeaderboardUI(data.leaderboardId, UnityEngine.SocialPlatforms.TimeScope.AllTime);
#endif
    }

    public override void SetTop(int score)
    {
#if UNITY_IOS && GC
        bool isGCAuthenticated = Social.localUser.authenticated;
        if (isGCAuthenticated)
        {
            Social.ReportScore(score, data.leaderboardId, success => { 
            if (success)
            { 
                //DevLogs.Log("==iOS GC report score ok: " + score + "\n");
            } 
            else
            {
                //DevLogs.Log("==iOS GC report score Failed: " + iOS_LeaderboardID + "\n");
            } });
        }
        else
        {
            //DevLogs.Log("==iOS GC can't report score, not authenticated\n");
        }
#endif
    }

    public override void Logout()
    {
        
    }

    public override void ShowAchivs()
    {
#if UNITY_IOS && GC
        Social.ShowAchievementsUI();
#endif
    }

    protected virtual string GetLoadUrl()
    {
        return string.Format("https://itunes.apple.com/app/id{0}", data.appleId);
    }

    public override void Rate()
    {
        Application.OpenURL(string.Format("itms-apps://itunes.apple.com/app/id{0}", data.appleId));
    }

    public override void Share()
    {
        ShareMsg(CreateText());
    }

    public override void Share(string message)
    {
        ShareMsg(message);
    }

    public override void Share(string message, string filepath)
    {
        ShareMsgAndTexture(message, filepath);
    }

    protected void ShareMsg(string message)
    {
        new NativeShare().SetText(message).Share();
    }

    protected void ShareMsgAndTexture(string msg, string filename = "default.png")
    {
        string filePath = Path.Combine(Application.persistentDataPath, filename);
       
        new NativeShare().AddFile(filePath).SetText(msg).Share();
    }

    [Button()]
    public string CreateText()
    {
        List<string> arr = new List<string>();
        
        arr.Add(GameSettings.TopLevelNumber.Value.ToString());//Not impl
        arr.AddRange(shareFrases.Select(s => s.Value).ToArray());

        shareTextMesage = string.Format(sharePattern, arr.ToArray());
        shareTextMesage = string.Concat(shareTextMesage, "\n", GetLoadUrl());
        Debug.Log(shareTextMesage);
        return shareTextMesage;
    }    
}
