﻿using UnityEngine;
using DefaultUtility;
using System.Collections;

public class DesktopService : Service
{
    public override void Login()
    {
        DevLogs.Log("PC_SERVICE_login".SetColor(Color.yellow));
    }

    public override void Logout()
    {
        DevLogs.Log("PC_SERVICE_logout".SetColor(Color.yellow));
    }

    public override void Rate()
    {
        DevLogs.Log("PC_SERVICE_rate".SetColor(Color.yellow));
    }

    public override void SetTop(int i)
    {
        DevLogs.Log("PC_SERVICE_set_top".SetColor(Color.yellow));
    }

    public override void Share()
    {
        DevLogs.Log("PC_SERVICE_share".SetColor(Color.yellow));
    }

    public override void Share(string message)
    {
        DevLogs.Log(string.Format("PC_SERVICE_share_message : {0}", message).SetColor(Color.yellow));
    }

    public override void Share(string message, string filename)
    {
        DevLogs.Log(string.Format("PC_SERVICE_share_texture : {0}",message).SetColor(Color.yellow));
    }

    public override void ShowAchivs()
    {
        DevLogs.Log("PC_SERVICE_show_achives".SetColor(Color.yellow));
    }

    public override void ShowTop()
    {
        DevLogs.Log("PC_SERVICE_show_top".SetColor(Color.yellow));
    }
}
