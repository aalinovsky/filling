﻿using UnityEngine;
using System.Collections;
using DefaultUtility;

public abstract class DBObject : Reward
{
    [Header("Доступен ли этот объект при первом запуске игры")]
    public bool isOpenOnStart = false;
            
    public bool IsOpen
    {
        get
        {
            if (isOpenOnStart)
            {
                return true;
            }
            else
            {
                return PlayerPrefsUtility.GetBool(SecretUniqueKey, false);
            }
        }

        set
        {
            PlayerPrefsUtility.SetBool(SecretUniqueKey, value);
        }
    }

#if UNITY_EDITOR    
    private void OnValidate()
    {
        Validate();
    }
#endif

    public virtual void Validate()
    {

    }
}

//[System.Serializable]
//public class CoinsOpenMethod
//{
//    public int coinsCost;
//}

//[System.Serializable]
//public class MoneyOpenMethod
//{
//    public string purchaseTag;
//}

//[System.Serializable]
//public class SocialNetworksOpenMethod
//{
//    public SocialAction action;
//    public SocialNetwork subscribeNetwork;
//}

//[System.Serializable]
//public class VideoAdsOpenMethod
//{
//    public int videoCount;
//}

//[System.Serializable]
//public class MissionOpenMethod
//{
//    public Mission mission;
//}