﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;

public abstract class Database<T> : ScriptableObject where T : DBObject
{
    #region Update current object event
    protected event Action setObject;

    public void SubscribeToUpdateObject(Action act)
    {
        setObject += act;
    }

    public void UnsubscribeToUpdateObject(Action act)
    {
        setObject -= act;
    }

    public void SetObject()
    {
        if(setObject != null)
            setObject();
    }
    #endregion

    public List<T> objectsList = new List<T>();

    public virtual int CurrentObjectIndex
    {
        get
        {
            return objectsList.IndexOf(CurrentObject);
        }
    }

    public virtual T CurrentObject
    {
        get
        {
            return objectsList.Find(o => o.SecretUniqueKey == CurrentObjectKey);
        }

        set
        {
            if (CurrentObjectKey != value.SecretUniqueKey)
            {
                CurrentObjectKey = value.SecretUniqueKey;
                SetObject();
            }
        }
    }

    public bool IsSelectedObject(T obj)
    {
        return CurrentObjectKey.Equals(obj.SecretUniqueKey);
    }

    protected virtual string CurrentObjectKey
    {
        get
        {
            return PlayerPrefsUtility.GetEncryptedString(GetCurrentObjectTag(), objectsList.First(o=>o.isOpenOnStart).SecretUniqueKey);
        }

        set
        {
            PlayerPrefsUtility.SetEncryptedString(GetCurrentObjectTag(), value);
        }
    }

    protected abstract string GetCurrentObjectTag();

    //protected abstract string GetCurrentDatabaseTag();
}
