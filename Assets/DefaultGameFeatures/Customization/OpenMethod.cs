﻿public enum OpenMethod
{
    None,
    Coins,
    Mission,
    VideoAds,
    Money,
    Roulette,
    DailyReward,
    Claim
}
