﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

public abstract class Reward : GuidObject
{
    public RewardType type;
    public Sprite icon;

    public abstract void ClaimReward(OpenMethod method);
    public abstract string GetIdentificator();
}

public enum RewardType
{
    Skin
}

#if UNITY_EDITOR 
[CustomEditor(typeof(Reward))]
class RewardInspector : GuidObjectInspector
{
    public float imageWidth = 150;
    public float imageHeight = 150;

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        Reward reward = (Reward)target;
        if (reward.icon != null)
        { 
            EditorGUI.DrawTextureTransparent(new Rect(EditorGUIUtility.currentViewWidth / 2.0f - imageWidth / 2.0f, 700, imageWidth, imageHeight), reward.icon.texture);
        }
    }
}
#endif
