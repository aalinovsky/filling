﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using DefaultUtility;

public abstract class Mission : GuidObject
{
    [Header("key for analytic")]
    public string key;
    [SerializeField]
    protected bool isActive;
    [Header("Timer settings")]
    public bool useTimer = false;
    public bool startOnFirstLaunch = false;
    public int timerDelay;
    public Timer dailyTimer;
    [Space()]
    public Sprite icon;
    [Header("Показывать всплывающее окно - оповещение о выполнении миссии")]
    public bool showPopUp = false;
    [Header("Короткое название мисии")]
    public LocalizedFrase tittle;
    [Header("Полное описание мисии")]
    public LocalizedFrase description;

    [SerializeField]
    protected List<Reward> rewards = new List<Reward>();

    public List<Reward> Rewards
    {
        get
        {
            return rewards;
        }
    }

    public MissionType Type
    {
        get
        {
            return CurrenntType();
        }
    }

    public bool IsCompleted
    {
        get
        {
            return PlayerPrefsUtility.GetBool(SecretUniqueKey);
        }

        set
        {
            if(useTimer)
            {
                if(dailyTimer.IsStarted && dailyTimer.Seconds < 0)
                return;
            }

            if (value && !IsCompleted && IsActive)
            {
                MissionComplete();
            }
        }
    }

    public bool IsActive
    {
        get
        {
            if (isActive)
                return isActive;

            return IsActiveCondition();
        }
    }

    ///////////////////Methods///////////////////

#if UNITY_EDITOR
    [Button()]
    public void CompleteMissionDevLogs()
    {
        IsCompleted = true;
    }
#endif

#if UNITY_EDITOR
    private void OnValidate()
    {
        Generate();
    }
#endif

    public virtual void Init()
    {
        if (useTimer)
        {
            dailyTimer = new Timer(string.Format("timer_{0}", SecretUniqueKey), timerDelay, false, false);
            dailyTimer.timerTriggerAction += () => { TimerTriggered(); };
            if (IsActive)
            {
                if (dailyTimer.IsFirstLaunch)
                {
                    if (startOnFirstLaunch)
                    {
                        StartTimer();
                    }
                }
                else
                {
                    dailyTimer.Start();
                }
            }
        }
    }

    protected void StartTimer()
    {
        DateTime date = DateTime.Now;
        date.AddSeconds(timerDelay);
        dailyTimer.SetNextTriggerTime(date);
        dailyTimer.Start();
    }

    public abstract void CheckProgress();
    public abstract string GetProgress();
    public abstract int GetProgressPercents();
    protected abstract void TimerTriggered();
    protected abstract bool IsActiveCondition();
    public abstract void ClaimReward();

    protected virtual void MissionActivateTimerBehaviour()
    {
        if (useTimer && startOnFirstLaunch)
        {
            StartTimer();
        }
    }

    protected virtual void MissionCompleteTimerBehaviour()
    {
        if (useTimer)
        {
            StartTimer();
        }
    }

    public virtual void ActivateMission()
    {
        MissionManager.MissionActivate(this);
        MissionActivateTimerBehaviour();
    }    
    
    protected virtual void MissionComplete()
    {        
        MissionManager.MissionComplete(this);
        MissionCompleteTimerBehaviour();
    }

    protected abstract MissionType CurrenntType();
}

[Serializable]
public class MissionCondition : SerializableCallback<bool> { }