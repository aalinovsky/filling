﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public abstract class MissionManager : Singletone<MissionManager>
{
    public delegate void MissionDelegate(Mission m);

    public event Action missionComplete;
    public event MissionDelegate receiveMissionComplete;
    public event MissionDelegate receiveMissionActivate;

    [SerializeField]
    protected List<Mission> missionsDb;

    public List<Mission> Missions
    {
        get
        {
            return missionsDb;
        }
    }

    public List<Mission> ActiveMissions
    {
        get
        {
            return Missions.FindAll((m)=> { return m.IsActive; });
        }
    }

    protected bool isInited = false;

    private void Awake()
    {
        InitMissions();
    }

    protected void InitMissions()
    {
        if (isInited)
            return;

        missionComplete += CheckMissionsState;
        receiveMissionComplete += SendMissionCompleteMessage;
        receiveMissionActivate += SendMissionActivateMessage;

        for (int i = 0; i < Missions.Count; i++)
        {
            Missions[i].Init();
        }

        isInited = true;
    }

    public static void MissionComplete(Mission m)
    {
        if (m != null)
        {
            Instance.RegisterCompletedMission(m);
        }
    }

    public static void MissionActivate(Mission m)
    {
        if (m != null)
        {
            Instance.receiveMissionActivate(m);
        }
    }

    public Mission GetMission(string key)
    {
        foreach (Mission mission in Missions)
        {
            if (key.Equals(mission.SecretUniqueKey))
            {
                return mission;
            }
        }
        return null;
    }

    public void CheckMissionsState()
    {
        InitMissions();
        CheckCompletedMissions(Missions);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////

    public List<Mission> CompletedMissions
    {
        get
        {
            List<Mission> completed = new List<Mission>();
            for (int i = 0; i < SavedCompletedMissions.guidKeys.Count; i++)
            {
                completed.Add(GetMission(SavedCompletedMissions.guidKeys[i]));
            }
            return completed;
        }
    }

    /// <summary>
    /// Check complete missions
    /// </summary>
    /// <param name="missions"></param>
    protected void CheckCompletedMissions(List<Mission> missions)
    {
        if (missions == null)
            return;

        foreach (Mission m in missions)
        {
            if(m.IsActive)
                m.CheckProgress();
        }
    }

    protected SavedGuidObjectsKeys SavedCompletedMissions
    {
        get
        {
            SavedGuidObjectsKeys keys = (SavedGuidObjectsKeys)JsonUtility.FromJson(PlayerPrefsUtility.GetEncryptedString("CompletedMissions"), typeof(SavedGuidObjectsKeys));
            if (keys == null)
            {
                keys = new SavedGuidObjectsKeys();
            }
            return keys;
        }

        set
        {
            PlayerPrefsUtility.SetEncryptedString("CompletedMissions", JsonUtility.ToJson(value));
        }
    }

    public static Mission LastCompleted
    {
        get
        {
            return MissionManager.Instance.GetMission(PlayerPrefsUtility.GetEncryptedString("LastCompletedMissionKey"));
        }

        set
        {
            string key = "";
            if (value != null)
            {
                key = value.SecretUniqueKey;
            }
            PlayerPrefsUtility.SetEncryptedString("LastCompletedMissionKey", key);
        }
    }

    public bool HasRewardOnStack
    {
        get
        {
            if (SavedCompletedMissions != null && !SavedCompletedMissions.IsEmpty)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }    

    public void RegisterCompletedMission(Mission obj)
    {
        if (obj == null)
            return;        
        SavedGuidObjectsKeys list = SavedCompletedMissions;
        if (!list.guidKeys.Contains(obj.SecretUniqueKey))
        {
            list.guidKeys.Add(obj.SecretUniqueKey);
            SavedCompletedMissions = list;
            LastCompleted = obj;
            receiveMissionComplete(obj);
            missionComplete();
        }
    }

    public Mission GetMissionFromStack()
    {
        Mission mission = null;
        if (HasRewardOnStack)
        {
            mission = GetMission(SavedCompletedMissions.guidKeys[0]);
        }
        return mission;
    }

    public Mission GrabMissionFromStack()
    {
        Mission reward = GetMissionFromStack();
        RemoveMissionFromStack(reward);
        return reward;
    }

    public void RemoveMissionFromStack(Mission obj)
    {
        if (obj == null)
            return;
        SavedGuidObjectsKeys list = SavedCompletedMissions;
        if (list.guidKeys.Contains(obj.SecretUniqueKey))
        {
            list.guidKeys.Remove(obj.SecretUniqueKey);
            SavedCompletedMissions = list;
        }
    }

    //////////////////////////////////////////////////////////////

    protected abstract void SendMissionCompleteMessage(Mission m);
    protected abstract void SendMissionActivateMessage(Mission m);
}

[System.Serializable]
public class SavedGuidObjectsKeys
{
    [SerializeField]
    public List<string> guidKeys = new List<string>();

    public bool IsEmpty
    {
        get
        {
            if (guidKeys == null || guidKeys.Count == 0)
                return true;

            return false;
        }
    }
}
