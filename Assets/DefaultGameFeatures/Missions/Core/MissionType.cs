﻿using UnityEngine;
using System.Collections;

public enum MissionType
{
    Action,
    Composite,
    Resource,    
    Social
}
