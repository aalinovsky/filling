﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "MissionsDatabase", menuName = "Missions/Missions database")]
public class MissionsDatabase : ScriptableObject
{
    public List<Mission> missions = new List<Mission>();
}
