﻿using UnityEngine;
using System.Collections;

public abstract class ActionMission : Mission
{
    public override string GetProgress()
    {
        return string.Empty;
    }

    public override int GetProgressPercents()
    {
        return 0;
    }

    protected override MissionType CurrenntType()
    {
        return MissionType.Action;
    }

    protected override void TimerTriggered()
    {
        
    }
}
