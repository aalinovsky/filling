﻿using UnityEngine;
using System.Collections;

//[CreateAssetMenu(fileName = "MissionCollect", menuName = "Missions/Collect", order = 1)]
public abstract class CollectMission : Mission
{
    public CollectedResource resourceType;
    [Header("Отсчет количества ресурсов после активации и выполнения мисси")]
    public bool fixResourceOnActivate = false;
    [SerializeField]
    protected bool multiplieMission = false;
    [SerializeField]
    protected int targetResourceCount;
    protected IntStorageData fixedResourceCount;

    public int TargetResourceValue
    {
        get
        {
            return targetResourceCount;
        }
    }

    public int ResourceValue
    {
        get
        {
            if (fixResourceOnActivate)
            {
                if(fixedResourceCount == null)
                {
                    fixedResourceCount = new IntStorageData(string.Format("fixedResourceMission_{0}", SecretUniqueKey));
                }
                return GetResourceCount() - fixedResourceCount.Value;
            }
            return GetResourceCount();
        }
    }

    public override void CheckProgress()
    {
        if (IsCompleted || !isActive)
            return;
        if (ResourceValue >= TargetResourceValue)
        {
            IsCompleted = true;
        }
    }

    public override string GetProgress()
    {
        return string.Format("{0}/{1}", ResourceValue, TargetResourceValue);
    }

    public override int GetProgressPercents()
    {
        return (int)(((float)ResourceValue / (float)TargetResourceValue) * 100);
    }

    protected override MissionType CurrenntType()
    {
        return MissionType.Resource;
    }

    protected abstract int GetResourceCount();

    public override void Init()
    {
        fixedResourceCount = new IntStorageData(string.Format("fixedResourceMission_{0}", SecretUniqueKey));
        base.Init();        
    }

    public override void ActivateMission()
    {
        base.ActivateMission();
        if (fixResourceOnActivate)
        {
            fixedResourceCount.Value = GetResourceCount();
        }
    }

    protected override void MissionComplete()
    {        
        PlayerPrefsUtility.SetBool(SecretUniqueKey, true);
        if (fixResourceOnActivate)        
        {
            fixedResourceCount.Value = GetResourceCount();
        }
        base.MissionComplete();
    }

    protected override void TimerTriggered()
    {
        if (fixResourceOnActivate)
        {
            fixedResourceCount.Value = GetResourceCount();
        }
    }
}

