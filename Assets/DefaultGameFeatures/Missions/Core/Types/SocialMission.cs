﻿using UnityEngine;
using System.Collections;

//[CreateAssetMenu(fileName = "SocialMission", menuName ="Missions/Social", order = 1)]
public abstract class SocialMission : Mission
{
    [Space()]
    public SocialNetwork network;
    public SocialAction action;
    public int actionsCount = 1;

    public override void CheckProgress()
    {
        if (IsCompleted)
            return;
    }

    public override string GetProgress()
    {
        return string.Format("0 / 0");
    }

    public override int GetProgressPercents()
    {
        return 0;
    }

    protected override MissionType CurrenntType()
    {
        return MissionType.Social;
    }

    void OnValidate()
    {
        if (action == SocialAction.Subscribe)
        {
            actionsCount = 1;
        }
    }
}
