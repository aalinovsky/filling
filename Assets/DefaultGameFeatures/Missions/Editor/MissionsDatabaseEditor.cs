﻿//using UnityEngine;
//using UnityEditor;
//using System.Collections;
//using UnityEditorInternal;
//using DefaultUtility;
//using System;
//using System.Collections.Generic;

//[CustomEditor(typeof(MissionsDatabase))]
//public class MissionsDatabaseEditor : Editor
//{
//    private ReorderableList list;

//    private MissionsDatabase routetarget
//    {
//        get
//        {
//            return target as MissionsDatabase;
//        }
//    }

//    protected List<float> heights = new List<float>();
//    protected float focusedHeightCounter = 3;
//    protected float unfocusedHeightCounter = 1.0f;

//    private void OnEnable()
//    {
//        //list = new ReorderableList(routetarget.missions, typeof(Mission), true, true, true, true);

//        list = new ReorderableList(serializedObject, serializedObject.FindProperty("missions"), true, true, true, true);

//        list.drawHeaderCallback += DrawHeader;
//        list.drawElementCallback += DrawElement;

//        list.elementHeightCallback = (index) => {
//            Repaint();

//            if (index < heights.Count)
//            {
//                return heights[index];
//            }

//            return EditorGUIUtility.singleLineHeight;
//        };

//        list.onReorderCallback += (list) =>
//        {
//            for (int i = 0; i < heights.Count; i++)
//            {
//                heights[i] = EditorGUIUtility.singleLineHeight * unfocusedHeightCounter;
//            }
//        };
//    }

//    private void OnDisable()
//    {
//        list.drawElementCallback -= DrawElement;
//        list.drawHeaderCallback -= DrawHeader;
//    }

//    private void DrawHeader(Rect rect)
//    {
//        GUI.Label(rect, "Missions");
//    }

//    private void DrawElement(Rect rect, int index, bool active, bool focused)
//    {
//        Mission item = null;
//        if(routetarget.missions.Count > index)
//            item = routetarget.missions[index];
//        var element = list.serializedProperty.GetArrayElementAtIndex(index);

//        EditorGUI.PropertyField(new Rect(rect.x, rect.y, 100, rect.height), element, GUIContent.none);
//        EditorGUI.BeginChangeCheck();
//        CheckHeight(index, focused);
//        if (element != null && item != null)
//        {
//            //item.IsActive = EditorGUI.Toggle(new Rect(rect.x + 110, rect.y, 30, rect.height), item.IsActive);
//            item.key = EditorGUI.TextField(new Rect(rect.x + 150, rect.y, 100, rect.height), item.key);

//            if (!focused)
//            {
//                if (item.GetType() == typeof(CollectMission))
//                {
//                    DrawCollectMission(new Rect(rect.x + 250, rect.y, rect.width - 330, rect.height), (CollectMission)item, active, focused);
//                }
//                else if (item.GetType() == typeof(CompositeMission))
//                {
//                    DrawCompositMission(new Rect(rect.x + 250, rect.y, rect.width - 330, rect.height), (CompositeMission)item, active, focused);
//                }
//            }
//            else
//            {

//            }
//        }

//        if (EditorGUI.EndChangeCheck())
//        {
//            EditorUtility.SetDirty(target);
//        }
//    }

//    protected void CheckHeight(int index, bool focused)
//    {
//        if(index >= heights.Count)
//        {
//            heights.Add(0);
//        }

//        heights[index] = focused ? EditorGUIUtility.singleLineHeight * focusedHeightCounter : EditorGUIUtility.singleLineHeight * unfocusedHeightCounter;
//    }

//    protected void DrawCollectMission(Rect rect, CollectMission cm, bool active, bool focused)
//    {
//        EditorGUI.TextField(new Rect(rect.x, rect.y, rect.width, rect.height), "Collect res mission");
//    }

//    protected void DrawCompositMission(Rect rect, CompositeMission cm, bool active, bool focused)
//    {
//        EditorGUI.TextField(new Rect(rect.x, rect.y, rect.width, rect.height), "Composit mission");
//    }

//    public override void OnInspectorGUI()
//    {
//        //base.OnInspectorGUI();

//        // Actually draw the list in the inspector
//        list.DoLayoutList();
//    }
//}
