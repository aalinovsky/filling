﻿using UnityEngine;
using System.Collections;
using DefaultUtility;

[CreateAssetMenu(fileName = "MissionCollect", menuName = "Missions/Collect", order = 1)]
public class CollectResourceMission : CollectMission
{
    public LocalizedFrase actionMessage;

    public override void ClaimReward()
    {
        for (int i = 0; i < Rewards.Count; i++)
        {
            Rewards[i].ClaimReward(OpenMethod.Mission);
        }
        MissionManager.Instance.RemoveMissionFromStack(this);
        if(multiplieMission)
            PlayerPrefsUtility.SetBool(SecretUniqueKey, false);
        if (useTimer)
            StartTimer();
    }

    protected override int GetResourceCount()
    {
        switch (resourceType)
        {
            case CollectedResource.Share:
                return ServiceController.Instance.ShareCount;
            //case CollectedResource.CollectCoins:
            //    return GameSettings.Game.Coins.Get();
            //case CollectedResource.CollectScore:
            //    return GameSettings.Game.BestScore.Get();
            //case CollectedResource.Dead:
            //    return GameSettings.Game.DeathCount.Get();
            //case CollectedResource.ReturnToGameTotalDays:
            //    return DailyVisit.GetNumberDaysVisited();
            //case CollectedResource.ReturnToGameInARow:
            //    return DailyVisit.GetNumberDaysVisitedInRow();
            //case CollectedResource.Level:
            //    return GameSettings.Game.CurrentLevelNumber.Get() + 1;
        }
        return 0;
    }

    protected override bool IsActiveCondition()
    {
        return false; 
    }
}
