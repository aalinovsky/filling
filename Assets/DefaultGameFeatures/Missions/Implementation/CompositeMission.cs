﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[CreateAssetMenu(fileName = "CompositeMission", menuName = "Missions/Composite", order = 1)]
public class CompositeMission : Mission
{
    [Header("Какие миссии нужно пройти что-бы эта выполнилась")]
    public List<Mission> missions = new List<Mission>();

    public int CompleteMissions
    {
        get
        {
            return missions.TakeWhile(o => o.IsCompleted).Count();
        }
    }

    public int TotalMissions
    {
        get
        {
            return missions.Count;
        }
    }

    public override void Init()
    {
        
    }

    public override void CheckProgress()
    {
        if (IsCompleted)
            return;

        if(CompleteMissions == TotalMissions)
        {
            IsCompleted = true;
        }
    }

    public override string GetProgress()
    {
        return string.Format("{0}/{1}",CompleteMissions, TotalMissions);
    }

    public override int GetProgressPercents()
    {
        return (int)(((float)CompleteMissions / (float)TotalMissions) * 100);
    }

    protected override MissionType CurrenntType()
    {
        return MissionType.Composite;
    }

    protected override void MissionComplete()
    {
        PlayerPrefsUtility.SetBool(SecretUniqueKey, true);
        base.MissionComplete();
    }

    protected override void TimerTriggered()
    {
        
    }

    protected override bool IsActiveCondition()
    {
        return false;
    }

    public override void ClaimReward()
    {
        
    }
}
