﻿using UnityEngine;
using System.Collections;

public class MissionsController : MissionManager
{

    protected override void SendMissionActivateMessage(Mission m)
    {
        
    }

    protected override void SendMissionCompleteMessage(Mission m)
    {

    }

    protected void GetRewardForMission(Mission m)
    {
        for (int i = 0; i < m.Rewards.Count; i++)
        {
            m.Rewards[i].ClaimReward(OpenMethod.Mission);
        }
    }
}
