﻿using UnityEngine;
using System.Collections;

public class SocialNetworksUtility
{

}

public enum SocialNetwork
{
    Any,
    Facebook,
    Twitter,
    Instagram,
    YouTube,
    VK
}

public enum SocialAction
{
    Subscribe,
    Share,
    InviteFriend
}