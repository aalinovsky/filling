﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;

public static class AssetsUtility
{
    public static string GetAssetFolder(Object o)
    {
        string[] splitedPath = AssetDatabase.GetAssetPath(o).Split('/');
        string path = AppPath();

        if (splitedPath.Length >= 2)
        {
            for (int i = 1; i < splitedPath.Length - 1; i++)
            {
                path += splitedPath[i] + "/";
            }
        }
        return path;
    }

    public static string GetFullPath(Object o)
    {
        string[] splitedPath = AssetDatabase.GetAssetPath(o).Split('/');
        string path = AppPath();

        if (splitedPath.Length >= 2)
        {
            for (int i = 1; i < splitedPath.Length; i++)
            {
                if (i != splitedPath.Length - 1)
                {
                    path += splitedPath[i] + "/";
                }
                else
                {
                    path += splitedPath[i];
                }
            }
        }
        return path;
    }

    public static string GetRelativePath(Object o)
    {
        string[] splitedPath = AssetDatabase.GetAssetPath(o).Split('/');
        string path = "";

        if (splitedPath.Length >= 2)
        {
            for (int i = 0; i < splitedPath.Length - 1; i++)
            {
                path += splitedPath[i] + "/";
            }
        }
        return path;
    }

    public static string AppPath()
    {
        return Application.dataPath + "/";
    }
}
#endif