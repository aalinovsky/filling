﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class MusicButtonController : MonoBehaviour
{
    public Sprite on;
    public Sprite off;

    public Image[] jointSprites;
    public bool useColor = false;
    public Color onColor;
    public Color offColor;

    private void Awake()
    {
        CheckImage();
    }

    public void Click()
    {
        if (MusicManager.Music)
        {
            MusicManager.Instance.Mute();
        }
        else
        {
            MusicManager.Instance.UnMute();
        }
        CheckImage();
    }

    private void CheckImage()
    {
        if (MusicManager.Music)
        {
            foreach (Image s in jointSprites)
            {
                if (useColor)
                    s.color = onColor;
                s.sprite = on;
            }
        }
        else
        {
            foreach (Image s in jointSprites)
            {
                if (useColor)
                    s.color = offColor;
                s.sprite = off;
            }
        }
    }
}