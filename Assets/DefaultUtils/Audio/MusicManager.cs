﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class MusicManager : Singletone<MusicManager> {

    public static bool Music
    {
        get
        {
            return PlayerPrefsUtility.GetBool("Music", true);
        }

        set
        {
            PlayerPrefsUtility.SetBool("Music", value);
        }
    }
    [SerializeField]
    AudioSource a;

    private void Awake()
    {
        if (a == null)
        {
            a = GetComponent<AudioSource>();
        }
        DontDestroyOnLoad(this.gameObject);
    }

    void Start () {        
		
        Check();
    }

    public void Pause()
    {
        a.Pause();
    }

    public void UnPause()
    {
        a.UnPause();
    }

    public void Mute()
    {
        SetMute(false);
    }

    public void UnMute()
    {
        SetMute(true);
    }

    protected void SetMute(bool isMuted)
    {
        a.mute = !isMuted;
        MusicManager.Music = isMuted;
    }

    private void Check()
    {
        if (MusicManager.Music)
        {
            UnMute();
        }
        else
        {
            Mute();
        }
    }

    protected bool lastState;
    public void AdsStart()
    {
        lastState = MusicManager.Music;
        Mute();
    }

    public void AdsFinish()
    {
        MusicManager.Music = lastState;
        Check();
    }
}
