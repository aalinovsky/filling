﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundButtonController : MonoBehaviour {

    public Sprite on;
    public Sprite off;

    public Image[] jointSprites;
    public bool useColor = false;
    public Color onColor;
    public Color offColor;

    private void Awake()
    {
        CheckImage();
    }

    public void Click()
    {
        if (SoundManager.Sounds)
        {
            SoundManager.Instance.Mute();
        }
        else
        {
            SoundManager.Instance.UnMute();
        }
        CheckImage();
    }

    public void ClickAll()
    {
        if (SoundManager.Sounds)
        {
            MusicManager.Instance.Mute();
            SoundManager.Instance.Mute();
        }
        else
        {
            MusicManager.Instance.UnMute();
            SoundManager.Instance.UnMute();
        }
        CheckImage();
    }

    private void CheckImage()
    {
        if (SoundManager.Sounds)
        {
            foreach (Image s in jointSprites)
            {
                if (useColor)   
                    s.color = onColor;
                s.sprite = on;
            }
        }
        else
        {
            foreach (Image s in jointSprites)
            {
                if(useColor)
                    s.color = offColor;
                s.sprite = off;
            }
        }
    }
}
