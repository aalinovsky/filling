﻿using UnityEngine;
using System.Collections;
using System;
using DefaultUtility;

[RequireComponent(typeof(AudioSource))]
public class SoundController : AbstractSoundController
{
    public AudioClip[] clips;

    [SerializeField]
    protected AudioSource audioVal;
    public AudioSource Audio
    {
        get
        {
            if(audioVal == null)
            {
                audioVal = GetComponent<AudioSource>();
            }
            return audioVal;
        }
    }

    void Awake()
    {
        InitSound();
    }

    private void InitSound()
    {
        if (nameSound == "")
        {
            nameSound = gameObject.name;
        }
    }

    [Button(ButtonMode.EnabledInPlayMode)]
    public override void Play()
    {
        if (Audio.enabled)
        {
            if (clips.Length > 0)
            {
                Play(clips[UnityEngine.Random.Range(0, clips.Length)]);
            }
            else
            {
                DevLogs.Log(String.Format("You try play {0} clip, but it value has not assigned",name));
            }
        }
    }

    private void Play(AudioClip clip)
    {
        Audio.PlayOneShot(clip);
    }

    public override void Mute()
    {
        Audio.mute = true;   
    }        

    public override void UnMute()
    {
        Audio.mute = false;
    }

    public override void Pause()
    {
        Audio.Pause();
    }

    public override void UnPause()
    {
        Audio.UnPause();
    }

    public override void Play(float pitch)
    {
        audioVal.pitch = pitch;
        Play();
    }
}


public abstract class AbstractSoundController : MonoBehaviour
{
    public string nameSound;

    public abstract void Play();
    public abstract void Play(float pitch);
    public abstract void Mute();
    public abstract void UnMute();
    public abstract void Pause();
    public abstract void UnPause();
}
