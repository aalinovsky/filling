﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : Singletone<SoundManager> {

    public AudioSource[] sources;

    public AbstractSoundController[] sounds;

    public static bool Sounds
    {
        get
        {
            return PlayerPrefsUtility.GetBool("Sounds", true);
        }

        set
        {
            PlayerPrefsUtility.SetBool("Sounds", value);
        }
    }

    void Awake () {
        DontDestroyOnLoad(Instance.gameObject);
        Check();
    }

    public void Pause()
    {
        foreach (AbstractSoundController abstrS in sounds)
        {
            abstrS.Pause();
        }
        for (int i = 0; i < sources.Length; i++)
        {
            sources[i].Pause();
        }
    }

    public void UnPause()
    {
        foreach (AbstractSoundController abstrS in sounds)
        {
            abstrS.UnPause();
        }
        for (int i = 0; i < sources.Length; i++)
        {
            sources[i].UnPause();
        }
    }

    public void Mute()
    {
        SoundManager.Sounds = false;
        foreach (AbstractSoundController abstrS in sounds)
        {
            abstrS.Mute();
        }

        for (int i = 0; i < sources.Length; i++)
        {
            sources[i].mute = true;
        }
    }

    public void UnMute()
    {
        SoundManager.Sounds = true;
        foreach (AbstractSoundController abstrS in sounds)
        {
            abstrS.UnMute();
        }

        for (int i = 0; i < sources.Length; i++)
        {
            sources[i].mute = false;
        }
    }

    public static void PlaySound(string sound)
    {
        Instance.Play(sound);
    }

    public static void PlaySound(string sound, float pitch)
    {
        Instance.Play(sound, pitch);
    }

    public void Play(string sound)
    {
        foreach (AbstractSoundController abstrS in sounds)
        {
            if (abstrS.nameSound.ToLower().Equals(sound.ToLower()))
            {
                abstrS.Play(1);
                break;
            }
        }
    }

    public void Play(string sound, float pitch)
    {
        foreach (AbstractSoundController abstrS in sounds)
        {
            if (abstrS.nameSound.ToLower().Equals(sound.ToLower()))
            {
                abstrS.Play(pitch);
                break;
            }
        }
    }

    private void Check()
    {
        if (SoundManager.Sounds)
        {
            UnMute();
        }
        else
        {
            Mute();
        }
    }

    protected bool lastState;
    public void AdsStart()
    {
        lastState = SoundManager.Sounds;
        Mute();
    }

    public void AdsFinish()
    {
        SoundManager.Sounds = lastState;
        Check();
    }
}
