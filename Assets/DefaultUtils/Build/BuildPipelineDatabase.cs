﻿#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using UnityEditor;

[CreateAssetMenu(fileName = "BuildPipelineDatabase", menuName = "Game/BuildPipeline/BuildPipelineDB")]
public class BuildPipelineDatabase : ScriptableObject
{
    public CustomBuildSettings pipelineIos;
    public CustomBuildSettings pipelineAndroid;
    public CustomBuildSettings pipelineDefault;

    public CustomBuildSettings Pipeline
    {
        get
        {
            switch (UnityEditor.EditorUserBuildSettings.selectedBuildTargetGroup)
            {
                case BuildTargetGroup.Android:
                    return pipelineAndroid;
                case BuildTargetGroup.iOS:
                    return pipelineIos;
                default:
                    return pipelineDefault;
            }            
        }
    }

    [EasyButtons.Button()]
    public void InitializeData()
    {
        if (Pipeline != null)
            Pipeline.InitPrebuildParameters(EditorUserBuildSettings.selectedBuildTargetGroup);
    }
}
#endif