﻿#if UNITY_EDITOR
using System.IO;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEngine;

public class CustomBuildPipeline : IPreprocessBuildWithReport, IPostprocessBuildWithReport
{
    static string databasePath = "CustomBuildPipeline/BuildPipelineDatabase";

    public int callbackOrder
    {
        get
        {
            return 0;
        }
    }

    protected static BuildPipelineDatabase pipelineDb;
    public static BuildPipelineDatabase PipelineDb
    {
        get
        {
            if (pipelineDb == null)
            {
                pipelineDb = (BuildPipelineDatabase)Resources.Load(databasePath, typeof(BuildPipelineDatabase));
                if (pipelineDb == null)
                {
                    if (!Directory.Exists(Application.dataPath + "/Resources"))
                    {
                        Directory.CreateDirectory(Application.dataPath + "/Resources");
                    }
                    if (!Directory.Exists(Application.dataPath + "/Resources/CustomBuildPipeline"))
                    {
                        Directory.CreateDirectory(Application.dataPath + "/Resources/CustomBuildPipeline");
                    }
                    pipelineDb = ScriptableObject.CreateInstance<BuildPipelineDatabase>();
                    AssetDatabase.CreateAsset(pipelineDb, string.Format("Assets/Resources/{0}.asset", databasePath));
                }
            }
            return pipelineDb;
        }
    }

    public static CustomBuildSettings Pipeline
    {
        get
        {
            if (PipelineDb.Pipeline != null)
            {
                return pipelineDb.Pipeline;
            }
            return null;
        }
    }

    public void OnPreprocessBuild(BuildReport report)
    {
        if (Pipeline != null)
        {
            Pipeline.InitPrebuildParameters(report.summary.platformGroup);
        }
    }

    public void OnPostprocessBuild(BuildReport report)
    {
        if (Pipeline != null)
        {
            Pipeline.InitPostbuildParameters(report.summary.platformGroup);
        }
    }
}
#endif