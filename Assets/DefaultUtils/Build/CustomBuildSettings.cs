﻿#if UNITY_EDITOR
using DefaultUtility;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Build.Reporting;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

public abstract class CustomBuildSettings : ScriptableObject
{
    public string projectName = "alinouski";
    public string version = "0.0.0.0";
    public string bundleIdentifier = "com.alinouski.";
    public IconSettings icons;

    public virtual void InitPrebuildParameters(BuildTargetGroup target)
    {
        Debug.LogWarning(string.Format("Start build preparings for {0}", projectName).SetColor(Color.red));
        PlayerSettings.productName = projectName;
        PlayerSettings.bundleVersion = version;
        PlayerSettings.SetApplicationIdentifier(target, bundleIdentifier);
        icons.Init(target);
        Debug.LogWarning(string.Format("Stop build preparings for {0}", projectName).SetColor(Color.red));
        EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
    }

    public abstract void InitPostbuildParameters(BuildTargetGroup target);

    [DefaultUtility.Button()]
    public void InitIcons()
    {
        icons.InitIcons();
    }
}

[System.Serializable]
public class IconSettings
{
    public Texture2D icon_1024;
    public Texture2D[] icons_application;
    public Texture2D[] icons_notification;
    public Texture2D[] icons_settings;
    public Texture2D[] icons_spotlight;
    public Texture2D[] icons_store;

    public Texture2D[] allIcons;

    public void Init(BuildTargetGroup targetGroup)
    {
        PlayerSettings.SetIconsForTargetGroup(BuildTargetGroup.Unknown, GetIcons(IconKind.Any));
        if (targetGroup == BuildTargetGroup.Android)
        {
            SetIcons(targetGroup, IconKind.Any);
        }
        else if (targetGroup == BuildTargetGroup.iOS)
        {
            SetIcons(targetGroup, IconKind.Any);
            SetIcons(targetGroup, IconKind.Application);
            SetIcons(targetGroup, IconKind.Notification);
            SetIcons(targetGroup, IconKind.Settings);
            SetIcons(targetGroup, IconKind.Spotlight);
            SetIcons(targetGroup, IconKind.Store);
        }
    }

    protected void SetIcons(BuildTargetGroup targetGroup, IconKind kind)
    {        
        PlayerSettings.SetIconsForTargetGroup(targetGroup, GetIcons(kind), kind);
    }
    
    public void InitIcons()
    {
        InitIconsForGroup(IconKind.Application);
        InitIconsForGroup(IconKind.Notification);
        InitIconsForGroup(IconKind.Settings);
        InitIconsForGroup(IconKind.Spotlight);
        InitIconsForGroup(IconKind.Store);
    }

    protected void InitIconsForGroup(IconKind kind)
    {
        int[] sizes = PlayerSettings.GetIconSizesForTargetGroup(BuildTargetGroup.iOS, kind);
        Texture2D[] icons = new Texture2D[sizes.Length];

        for (int i = 0; i < sizes.Length; i++)
        {
            icons[i] = GetIconBySize(sizes[i]);
        }

        switch (kind)
        {
            case IconKind.Application:
                icons_application = icons;
                break;
            case IconKind.Notification:
                icons_notification = icons;
                break;
            case IconKind.Settings:
                icons_settings = icons;
                break;
            case IconKind.Spotlight:
                icons_spotlight = icons;
                break;
            case IconKind.Store:
                icons_store = icons;
                break;
        }
    }

    protected Texture2D GetIconBySize(int size)
    {
        for (int i = 0; i < allIcons.Length; i++)
        {
            if (allIcons[i].height == size)
                return allIcons[i];
        }
        return null;
    }

    public Texture2D[] GetIcons(IconKind kind)
    {
        if (kind == IconKind.Any)
        {
            List<Texture2D> icons = new List<Texture2D>();
            icons.Add(icon_1024);

            return icons.ToArray();
        }

        switch (kind)
        {
            case IconKind.Application:
                return icons_application;
            case IconKind.Notification:
                return icons_notification;
            case IconKind.Settings:
                return icons_settings;
            case IconKind.Spotlight:
                return icons_spotlight;
            case IconKind.Store:
                return icons_store;
            default:
                return new Texture2D[] { icon_1024 };
        }
    }
}
#endif