﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class CustomBuildPipelineMenu
{
    [MenuItem("CustomBuild/Select Settings", false, 0)]
    static void SelectGASettings()
    {
        Selection.activeObject = CustomBuildPipeline.PipelineDb;
    }
}
