﻿#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Build.Reporting;
using UnityEngine;

[CreateAssetMenu(menuName = "Game/BuildPipeline/DefaultSettings", fileName = "DefaultSettings")]
public class DefaultBuildSettings : CustomBuildSettings
{
    public override void InitPrebuildParameters(BuildTargetGroup target)
    {
        base.InitPrebuildParameters(target);
    }

    public override void InitPostbuildParameters(BuildTargetGroup target)
    {
        
    }
}
#endif