﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace DefaultUtility
{
    public class UIRotateObject : MonoBehaviour
    {
        public List<Transform> objectsForRotate;

        public bool playOnAwake = true;
        public bool loop = true;
        [ConditionalField("loop", true)]
        public LoopType loopType;

        public RotateMode rotateMode;

        [ConditionalField("rotateMode", RotateMode.Speed)]
        public Vector3 rotateDirection;

        [ConditionalField("rotateMode", RotateMode.Angles)]
        public Vector3 startAngle;
        [ConditionalField("rotateMode", RotateMode.Angles)]
        public Vector3 finalAngle;

        public float animationLength;

        [ConditionalField("rotateMode", RotateMode.Speed)]
        public MoveType type;
                
        [ConditionalField("type", MoveType.Curve)]
        public CurvesData curves;

        protected CurveData curve;
        public CurveData Curve
        {
            get
            {
                if(curve == null)
                {
                    type = MoveType.Linear;
                }
                return curve;
            }

            set
            {
                curve = value;
            }
        }

        public UnityEvent startEvent;
        public event Action startAction;

        public UnityEvent stopEvent;
        public event Action finishAction;

        protected IEnumerator coroutine;

        private void Awake()
        {
            if(objectsForRotate.Count == 0)
            {
                objectsForRotate.Add(transform);
            }
            startAction += startEvent.Invoke;
            finishAction += StopCoroutine;
            finishAction += stopEvent.Invoke;
        }

        private void OnEnable()
        {
            if (playOnAwake)
            {
                StartRotation();
            }
        }

        protected void StopCoroutine()
        {
            if (coroutine != null)
            {
                StopCoroutine(coroutine);
                coroutine = null;
            }
        }

        public void StartRotation(float duration)
        {
            animationLength = duration;
            StartRotation();
        }

        public void StartRotation()
        {
            if (coroutine == null)
            {
                if (type == MoveType.Curve)
                {
                    Curve = curves.curves[UnityEngine.Random.Range(0, curves.curves.Count)];
                }
                coroutine = RotateLoop(animationLength);
                StartCoroutine(coroutine);
            }
        }

        public void Rotate(float dlt, bool forward)
        {
            Vector3 newRotation = Vector3.zero;
            Quaternion newQuaternion = Quaternion.identity;

            if (rotateMode == RotateMode.Speed)
            {
                newRotation = rotateDirection;
                if (type == MoveType.Curve)
                {
                    newRotation *= Curve.GetDltValue(dlt);
                }
                else if (!forward)
                {
                    newRotation = -newRotation;
                }
            }
            else
            {
                newQuaternion = Quaternion.Euler(Vector3.Lerp(startAngle, finalAngle, dlt));
            }

            foreach (Transform t in objectsForRotate)
            {
                if (rotateMode == RotateMode.Speed)
                {
                    t.Rotate(newRotation);
                }
                else
                {
                    t.rotation = newQuaternion;
                }
            }
        }

        IEnumerator RotateLoop(float loopDuration)
        {
            startAction();

            float currentTime = 0;
            bool rotateForward = true;
            float dlt = 0;

            while (gameObject.activeInHierarchy)
            {
                dlt = currentTime / loopDuration;
                Rotate(dlt, rotateForward);
                yield return null;

                if (rotateForward)
                {
                    currentTime += Time.deltaTime;
                    if (currentTime >= loopDuration)
                    {
                        currentTime = loopDuration;
                        if (loop)
                        {
                            switch (loopType)
                            {
                                case LoopType.PingPong:
                                    rotateForward = !rotateForward;
                                    break;
                                case LoopType.Repeat:
                                    currentTime = 0;
                                    break;
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                else
                {
                    currentTime -= Time.deltaTime;
                    if (currentTime <= 0)
                    {
                        currentTime = 0;
                        if (loop)
                        {
                            switch (loopType)
                            {
                                case LoopType.PingPong:
                                    rotateForward = !rotateForward;
                                    break;
                                case LoopType.Repeat:
                                    //Repeat type must scale only on forward
                                    break;
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }

            finishAction();
        }

        private void OnValidate()
        {
            if(rotateMode == RotateMode.Angles && type == MoveType.Curve)
            {
                type = MoveType.Linear;
            }
        }
    }
}
