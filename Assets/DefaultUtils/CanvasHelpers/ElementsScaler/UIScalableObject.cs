﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace DefaultUtility
{
    public class UIScalableObject : MonoBehaviour
    {
        public Transform[] objectsForScale;

        public bool playOnAwake = true;
        public bool loop = true;
        [ConditionalField("loop", true)]
        public LoopType loopType;

        public Vector3 startScale;
        public Vector3 finalScale;

        public float animationLength;

        public MoveType type;

        [ConditionalField("type", MoveType.Curve)]
        public CurveData curve;
        [ConditionalField("type", MoveType.Curve)]
        public Vector3 curveSeed;


        public UnityEvent stopEvent;
        public event Action finishAction;

        protected IEnumerator coroutine;

        private void Awake()
        {
            finishAction += StopCoroutine;
            finishAction += stopEvent.Invoke;
        }

        private void OnEnable()
        {
            if (playOnAwake)
            {
                StartScaling();
            }
        }

        protected void StopCoroutine()
        {
            if (coroutine != null)
            {
                StopCoroutine(coroutine);
                coroutine = null;
            }
        }

        public void StartScaling(float duration)
        {
            animationLength = duration;
            StartScaling();
        }

        public void StartScaling()
        {
            if(coroutine == null)
            {
                coroutine = ScaleLoop(animationLength);
                StartCoroutine(coroutine);
            }
        }

        public void Scale(float dlt)
        {
            Vector3 currentScale = Vector3.Lerp(startScale, finalScale, dlt);

            if(type == MoveType.Linear)
            {
                //transform.localScale = currentScale;
            }
            else
            {
                currentScale += curveSeed * curve.GetDltValue(dlt);
            }

            if(objectsForScale.Length == 0)
            {
                transform.localScale = currentScale;
            }
            else
            {
                foreach (Transform t in objectsForScale)
                {
                    t.localScale = currentScale;
                }
            }
        }

        IEnumerator ScaleLoop(float loopDuration)
        {            
            float currentTime = 0;
            bool scaleForward = true;
            float dlt = 0;

            while(gameObject.activeInHierarchy)
            {
                dlt = currentTime / loopDuration;
                Scale(dlt);
                yield return null;

                if (scaleForward)
                {
                    currentTime += Time.deltaTime;
                    if(currentTime >= loopDuration)
                    {
                        currentTime = loopDuration;
                        if (loop)
                        {
                            switch (loopType)
                            {
                                case LoopType.PingPong:
                                    scaleForward = !scaleForward;
                                    break;
                                case LoopType.Repeat:
                                    currentTime = 0;
                                    break;
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                else
                {
                    currentTime -= Time.deltaTime;
                    if (currentTime <= 0)
                    {
                        currentTime = 0;
                        if (loop)
                        {
                            switch (loopType)
                            {
                                case LoopType.PingPong:
                                    scaleForward = !scaleForward;
                                    break;
                                case LoopType.Repeat:
                                    //Repeat type must scale only on forward
                                    break;
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }

            finishAction();
        }
    }
}
