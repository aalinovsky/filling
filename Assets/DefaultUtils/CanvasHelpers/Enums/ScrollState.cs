﻿namespace DefaultUtility
{
    public enum ScrollState
    {
        Idle,
        Move,
        Default,
        MoveToNext,
        MoveToConcrete
    }
}