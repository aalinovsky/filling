﻿using UnityEngine;
using System.Collections;
using DefaultUtility;

[RequireComponent(typeof(RectTransform))]
public class UiMoveToCenter : MonoBehaviour
{
    public bool fixedPosition = true;
    public DefaultUtility.ResultAxis axisType = DefaultUtility.ResultAxis.X;
    public RectTransform container;

    protected RectTransform myTransform;
    protected Vector2 mySize
    {
        get
        {
            return myTransform.sizeDelta;
        }
    }
    protected Vector2 HorizontalBorders
    {
        get
        {
            float xPos = container.position.x;
            float halfwidth = err * (mySize.x / 2.0f);
            Vector2 borders = new Vector2(xPos + halfwidth, xPos + err * container.sizeDelta.x - halfwidth);
            //DevLogs.LogFormat("HorizontalBorders : {0}".SetColor(Color.red), borders);
            return borders;
        }
    }

    protected float err = 1;

    private void Awake()
    {
        myTransform = GetComponent<RectTransform>();
        //mySize = myTransform.sizeDelta;
        err = ((float)Screen.width / 1080.0f);
        //DevLogs.LogFormat("Error {0}".SetColor(Color.green), err);
    }

    private void Update()
    {
        UpdatePosition();
    }

    protected void UpdatePosition()
    {
        if (fixedPosition)
        {
            MoveToCenterOfParent();
        }
        else
        {
            Vector2 screenCenter = UIUtils.ScreenCenter;
            Vector2 borders = HorizontalBorders;
            if (screenCenter.x > borders.x && screenCenter.x < borders.y)
            {
                MoveToCenterOfScreen();
            }
            else if (screenCenter.x < borders.x)
            {
                MoveToLeftOfParent();
            }
            else if (screenCenter.x > borders.y)
            {
                MoveToRightOfParent();
            }
        }
    }

    protected void MoveToCenterOfScreen()
    {
        Vector2 screenCenter = UIUtils.ScreenCenter;
        //DevLogs.LogFormat("ScreenCenter : {0}".SetColor(Color.red), screenCenter);
        MoveTo(screenCenter);
    }

    protected void MoveToLeftOfParent()
    {
        Vector3 centerOfParent = container.position;
        centerOfParent.x += err * mySize.x / 2.0f;
        //DevLogs.LogFormat("ParentLeft : {0}".SetColor(Color.red), centerOfParent);
        MoveTo(centerOfParent);
    }

    protected void MoveToRightOfParent()
    {
        Vector3 centerOfParent = container.position;
        centerOfParent.x += err * container.sizeDelta.x - err * mySize.x / 2.0f;
        //DevLogs.LogFormat("ParentRight : {0}".SetColor(Color.red), centerOfParent);
        MoveTo(centerOfParent);
    }

    protected void MoveToCenterOfParent()
    {
        Vector3 centerOfParent = err * container.GetCenterPosition(err);
        //DevLogs.LogFormat("ParentCenter : {0}".SetColor(Color.red), centerOfParent);
        MoveTo(centerOfParent);
    }

    protected void MoveTo(Vector3 centerOfParent)
    {
        switch (axisType)
        {
            case DefaultUtility.ResultAxis.X:
                MoveToPosition(centerOfParent.x);

                break;
            case DefaultUtility.ResultAxis.Y:
                MoveToPosition(centerOfParent.y);
                break;
        }
    }

    protected void MoveToPosition(float axisPos)
    {
        Vector3 newPos = myTransform.position;
        //DevLogs.LogFormat("MoveTo : {0}".SetColor(Color.green), axisPos);
        switch (axisType)
        {
            case DefaultUtility.ResultAxis.X:
                newPos.x = axisPos;// - mySize.x / 2.0f;
                break;
            case DefaultUtility.ResultAxis.Y:
                newPos.y = axisPos;// - mySize.y / 2.0f;
                break;
        }
        //DevLogs.LogFormat("Result : {0}".SetColor(Color.blue), newPos);
        myTransform.position = newPos;
    }
}
