﻿#if UNITY_EDITOR
using UnityEngine;

namespace DefaultUtility {
    public class MovableImagesExampleInput : MonoBehaviour {

        public UIMoveObjectToPointController c;

        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                c.CreateMovableObject(Input.mousePosition);
            }
        }
    }
}
#endif