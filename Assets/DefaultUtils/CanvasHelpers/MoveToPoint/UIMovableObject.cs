﻿using UnityEngine;
using System.Collections;
using System;
using DefaultUtility;
using UnityEngine.Events;

namespace DefaultUtility
{
    public class UIMovableObject : MonoBehaviour
    {
        public MoveMethod moveMethod;
        [ConditionalField("moveMethod", MoveMethod.MoveToWards)]
        public float minDistance = 1;

        protected Vector3 startPosition;
        protected Vector3 finalPosition;
        protected Transform target;
        protected CurveData curve;

        protected ResultAxis axis;
        protected float currentDuration;
        public float CurrentDuration
        {
            get
            {
                return currentDuration;
            }
        }

        protected float moveTowardsSpeed;
        public float MoveTowardsSpeed
        {
            get
            {
                return moveTowardsSpeed;
            }
        }

        public UnityEvent startEvent;
        public UnityEvent stopEvent;

        public event Action startMove;
        public event Action stopMove;

        public Vector3 FinalPosition
        {
            get
            {
                if (target != null)
                    return target.position;

                return finalPosition;
            }
        }

        private void Awake()
        {
            startMove += startEvent.Invoke;
            stopMove += stopEvent.Invoke;
        }

        public void Create(Vector3 s, Transform f, float duration)
        {
            target = f;
            Create(s, target.position, duration);
        }

        public void Create(Vector3 s, Vector3 f, float duration)
        {
            currentDuration = duration;
            startPosition = s;
            finalPosition = f;
            moveTowardsSpeed = (transform.position - FinalPosition).magnitude / (currentDuration / Time.deltaTime);

            Vector3 resVector = startPosition - finalPosition;

            if (Mathf.Abs(resVector.x) > Mathf.Abs(resVector.y))
            {
                axis = ResultAxis.Y;
            }
            else
            {
                axis = ResultAxis.X;
            }

            SetPosition(startPosition);
        }

        public void Create(Vector3 s, Vector3 f, float duration, CurveData cd)
        {
            curve = cd;
            Create(s, f, duration);
        }

        public void Create(Vector3 s, Transform f, float duration, CurveData cd)
        {
            curve = cd;
            Create(s, f, duration);
        }

        public void StartMovement()
        {
            startMove();
        }

        public void StopMovement()
        {
            stopMove();
        }

        public bool IsFinish
        {
            get
            {
                if(Mathf.Abs(transform.position.magnitude - (target != null? target.position.magnitude : finalPosition.magnitude)) <= minDistance)
                {
                    return true;
                }

                return false;
            }
        }

        public void Move(float dlt)
        {
            Vector3 newPosition;

            if (moveMethod == MoveMethod.Lerp)
            {
                newPosition = Vector3.Lerp(startPosition, FinalPosition, dlt);
                if (curve != null)
                {
                    if (axis == ResultAxis.X)
                    {
                        newPosition.x = newPosition.x + Screen.width * curve.GetDltValue(dlt);
                    }
                    else
                    {
                        newPosition.y = newPosition.y + Screen.height * curve.GetDltValue(dlt);
                    }
                }
            }
            else
            {
                newPosition = Vector3.MoveTowards(transform.position, FinalPosition, curve != null? MoveTowardsSpeed * curve.GetDltValue(dlt): MoveTowardsSpeed);
            }

            SetPosition(newPosition);
        }

        protected void SetPosition(Vector3 newPosition)
        {
            transform.position = newPosition;            
        }
    }
}
