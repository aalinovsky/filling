﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DefaultUtility
{
    public class UIMoveObjectToPointController : MonoBehaviour
    {
        public bool targetIsStatis = false;
        public Transform target;
        public Transform parentContainer;
        public UIMovableObject prefab;
        public bool isRandomMoveDuration;

        [ConditionalField("isRandomMoveDuration", false)]
        public float staticMoveDuration;
        [ConditionalField("isRandomMoveDuration", true)]
        public Vector2 randomMoveDuration;

        [Header("Destroy object on end of path")]
        public bool destroyOnFinish = true;
        public bool authoStartMovement = true;
        public MoveType moveType;

        [ConditionalField("moveType", MoveType.Curve)]
        public bool randomCurves;

        [ConditionalField("moveType,randomCurves", new object[] { MoveType.Curve, true })]
        public CurvesData curves;
        [ConditionalField("moveType,randomCurves", new object[] { MoveType.Curve, false })]
        public CurveData curve;

        public virtual UIMovableObject CreateMovableObject(Vector3 startPos)
        {
            return CreateMovableObject(startPos, target);
        }

        public virtual UIMovableObject CreateMovableObject(Vector3 startPos, Transform targ)
        {
            UIMovableObject objectMovable = Instantiate(prefab, parentContainer);

            InitPositions(objectMovable, startPos, targ);
            InitStartEvents(objectMovable);
            InitStopEvents(objectMovable);

            if (authoStartMovement)
            {
                StartMovement(objectMovable);
            }

            return objectMovable;
        }

        protected virtual void InitStartEvents(UIMovableObject objectMovable)
        {
            objectMovable.startMove += () => { };
        }

        protected virtual void InitStopEvents(UIMovableObject objectMovable)
        {
            if (destroyOnFinish)
            {
                objectMovable.stopMove += () => { Destroy(objectMovable.gameObject); };
            }
            else
            {
                objectMovable.stopMove += () => { };
            }
        }

        protected void InitPositions(UIMovableObject objectMovable, Vector3 startPos, Transform targ)
        {
            float duration = 0;

            if (isRandomMoveDuration)
            {
                duration = UnityEngine.Random.Range(randomMoveDuration.x, randomMoveDuration.y);
            }
            else
            {
                duration = staticMoveDuration;
            }

            if (moveType == MoveType.Linear)
            {
                if (targetIsStatis)
                {
                    objectMovable.Create(startPos, targ.position, duration, null);
                }
                else
                {
                    objectMovable.Create(startPos, targ, duration, null);
                }
            }
            else
            {
                if (randomCurves)
                {                    
                    if (targetIsStatis)
                    {
                        objectMovable.Create(startPos, targ.position, duration, curves.curves[UnityEngine.Random.Range(0, curves.curves.Count)]);
                    }
                    else
                    {
                        objectMovable.Create(startPos, targ, duration, curves.curves[UnityEngine.Random.Range(0, curves.curves.Count)]);
                    }
                }
                else
                {
                    if (targetIsStatis)
                    {
                        objectMovable.Create(startPos, targ.position, duration, curve);
                    }
                    else
                    {
                        objectMovable.Create(startPos, targ, duration, curve);
                    }                    
                }
            }
        }

        public virtual void StartMovement(UIMovableObject objectMovable)
        {
            StartCoroutine(MoveCoinToScore(objectMovable, objectMovable.CurrentDuration));
        }

        IEnumerator MoveCoinToScore(UIMovableObject objectMovable, float moveTime)
        {
            float t = 0;
            objectMovable.StartMovement();
            
            
            if (objectMovable.moveMethod == MoveMethod.Lerp)
            {
                while (t < moveTime)
                {
                    objectMovable.Move(t / moveTime);
                    yield return null;
                    t += Time.deltaTime;
                }
            }
            else
            {
                while (!objectMovable.IsFinish)
                {
                    objectMovable.Move(t / moveTime);
                    yield return null;
                    t += Time.deltaTime;
                }
            }

            objectMovable.StopMovement();
        }
    }
}