﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace DefaultUtility
{
    public abstract class AbstractScroll : MonoBehaviour
    {
        protected abstract void CheckComponents();

        public abstract void InitScrollContainer<T>(List<T> data) where T : ScrollElementData;
    }
}
