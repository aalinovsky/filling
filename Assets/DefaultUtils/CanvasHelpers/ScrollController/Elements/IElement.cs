﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DefaultUtility
{
    public abstract class IElement : MonoBehaviour
    {
        protected bool isSelected = false;
        public bool IsSelected
        {
            get
            {
                return isSelected;
            }

            set
            {
                isSelected = value;
            }
        }

        public virtual void Init(object data)
        {

        }

        public abstract void Select();

        public abstract void UnSelect();

        public abstract void Click();
    }
}