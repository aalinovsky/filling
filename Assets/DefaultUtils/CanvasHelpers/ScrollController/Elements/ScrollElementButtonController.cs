﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DefaultUtility
{
    public class ScrollElementButtonController : IElement
    {
        public override void Init(object data)
        {
            Button btn = transform.GetComponentInChildren<Button>();
            if (btn != null)
            {
                btn.onClick.AddListener(Click);
            }
        }

        public override void Click()
        {
            throw new System.NotImplementedException();
        }

        public override void Select()
        {
            throw new System.NotImplementedException();
        }

        public override void UnSelect()
        {
            throw new System.NotImplementedException();
        }
    }
}
