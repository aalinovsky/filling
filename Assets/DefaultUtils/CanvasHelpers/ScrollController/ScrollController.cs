﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace DefaultUtility
{
    public class ScrollController : TriggerScrollController
    {        
        private void HandleSwipe()
        {
//            if (!Active)
//                return;

//#if !UNITY_EDITOR
//            if (Input.touchCount > 0)
//            {
//                if (Input.GetTouch(0).phase == TouchPhase.Began)
//                {
//                    Drag = true;
//                    Vector2 touchDeltaPosition = Input.GetTouch(0).position;
//                    startpos = new Vector3(touchDeltaPosition.x, 0, touchDeltaPosition.y);
//                    oldpos = startpos;
//                }

//                if (Input.GetTouch(0).phase == TouchPhase.Moved)
//                {
//                    Vector2 touchDeltaPosition = Input.GetTouch(0).position;
//                    Vector3 pos = new Vector3(touchDeltaPosition.x, 0, touchDeltaPosition.y);

//                    if aligment == AligmentType.Horizontal)
//                    {
//                        if (pos.x != oldpos.x)
//                        {
//                            Vector3 f = pos - oldpos;

//                            float l = f.x < 0 ? (f.magnitude * Time.deltaTime) : -(f.magnitude * Time.deltaTime);
//                            MoveScrollContainer(-l * swipeSpeed);
//                        }
//                    }
//                    else
//                    {
//                        if (pos.z != oldpos.z)
//                        {
//                            Vector3 f = pos - oldpos;

//                            float l = f.z < 0 ? (f.magnitude * Time.deltaTime) : -(f.magnitude * Time.deltaTime);
//                            MoveScrollContainer(-l * swipeSpeed);
//                        }
//                    }

//                    oldpos = pos;
//                }

//                if (Input.GetTouch(0).phase == TouchPhase.Canceled || Input.GetTouch(0).phase == TouchPhase.Ended)
//                {
//                    Drag = false;
//                }
//            }
//            else if (Drag)
//            {
//                Drag = false;
//            }
//#endif
//#if UNITY_EDITOR

            if (Input.GetMouseButtonDown(0))
            {
                BeginDrag();
            }

            if (Input.GetMouseButton(0))
            {
                if (!Drag)
                {
                    BeginDrag();
                }
                else
                {
                    StartDrag();
                }
            }

            if (Input.GetMouseButtonUp(0) || (Drag && !Input.GetMouseButton(0)))
            {
                EndDrag();
            }
//#endif
        }

        protected override void DefaultState()
        {
            HandleSwipe();
            ClaculateDistance();
            UpdateSelectedElement();
            LerpToElement(SelectedElementId);
        }

        protected override void MoveState()
        {
            HandleSwipe();
            ClaculateDistance();
            UpdateSelectedElement();
        }
    }
}