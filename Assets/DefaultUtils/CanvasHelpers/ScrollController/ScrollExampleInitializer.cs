﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace DefaultUtility {
    public class ScrollExampleInitializer : MonoBehaviour
    {
        public AbstractScroll scroll;
        [Range(2,15)]
        public int elements;
        public bool autoInit = false;

        private void Start()
        {
            if (autoInit)
                Init();
        }

        [Button(ButtonMode.EnabledInPlayMode)]
        public void Init()
        {
            List<ScrollElementData> data = new List<ScrollElementData>();
            for (int i = 0; i < elements; i++)
            {
                data.Add(new ScrollElementData());
            }
            scroll.InitScrollContainer(data);
        }

        private void OnValidate()
        {
            if(elements < 2)
            {
                elements = 2;
            }
        }
    }
}