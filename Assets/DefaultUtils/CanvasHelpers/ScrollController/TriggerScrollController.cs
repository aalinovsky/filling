﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace DefaultUtility
{
    public class TriggerScrollController : AbstractScroll
    {
        [Header("Направление скролла")]
        public AligmentType aligment = AligmentType.Vertical;
        [Header("Объект относительно которого считается расстояние")]
        public Transform scroll;
        [Header("Контейнер для объектов скролла")]
        public Transform scrollContainer;
        [Header("Префаб элемента скролла")]
        public IElement scrollElement;
        [Header("Скорость авто доводки")]
        public float scrollSpeed;
        [Header("Скорость свайпа пальцем")]
        public float swipeSpeed = 85;
        [Header("Минимальная дистанция после которой отключается доводка до конкретного элемента")]
        public float minDistanceForMoveToElement = 5;
        [Header("Использовать Lerp для скролла да/нет")]
        public bool lerp;
        [Header("Ширина элемента(префаба) скролла")]
        public float elementDistance;
        [Header("Событие которое произойдет при смене активного элемента")]
        public UnityEvent setSelectedEvents;

        protected ScrollState _state = ScrollState.Idle;
        protected List<IElement> elements = new List<IElement>();

        public List<IElement> Elements
        {
            get { return elements; }
        }

        public int CountElements
        {
            get { return elements.Count; }
        }

        protected List<float> distance = new List<float>();

        protected float minDistance = 0;
        public float MinDistance
        {
            get
            {
                return minDistance;
            }
        }

        protected int selectedElementId = int.MinValue;
        public int SelectedElementId
        {
            get
            {
                return selectedElementId;
            }

            set
            {
                if (selectedElementId == value)
                    return;

                if (selectedElementId >= 0 && selectedElementId < CountElements)
                {
                    SelectedElement.UnSelect();
                }

                selectedElementId = value;
                SelectedElement.Select();
                //invoke external events
                setSelectedEvents.Invoke();
            }
        }

        public IElement SelectedElement
        {
            get
            {
                return elements[selectedElementId];
            }
        }

        protected bool drag = false;
        public bool Drag
        {
            get { return drag; }
            set
            {
                drag = value;
                if (drag)
                {
                    _state = ScrollState.Move;
                }
                else
                {
                    _state = ScrollState.Default;
                }
            }
        }

        protected bool active = true;
        public bool Active
        {
            get
            {
                return active;
            }
            set
            {
                active = value;
            }
        }

        private void Awake()
        {
            CheckComponents();
        }

        private void Start()
        {
            
        }

        [Button("Добавить нужные компоненты")]
        protected override void CheckComponents()
        {
            DevLogs.Log("Add events to scroll");
            CheckImage();
            CheckEventTrigger();
        }

        protected virtual void CheckImage()
        {
            if (!scroll.gameObject.GetComponent<Image>())
            {
                scroll.gameObject.AddComponent<Image>();
            }
        }

        protected virtual void CheckEventTrigger()
        {
            EventTrigger et;
            if (scroll.gameObject.GetComponent<EventTrigger>() == null)
            {
                et = scroll.gameObject.AddComponent<EventTrigger>();
            }
            else
            {
                et = scroll.gameObject.GetComponent<EventTrigger>();
            }

            bool hasBeginDrag = false;
            bool hasEndDrag = false;
            bool hasOnDrag = false;

            foreach (EventTrigger.Entry e in et.triggers)
            {
                if (e.eventID == EventTriggerType.EndDrag)
                {
                    hasEndDrag = true;
                }
                if (e.eventID == EventTriggerType.BeginDrag)
                {
                    hasBeginDrag = true;
                }
                if (e.eventID == EventTriggerType.Drag)
                {
                    hasOnDrag = true;
                }
            }

            if (!hasBeginDrag)
            {
                EventTrigger.Entry e = new EventTrigger.Entry();
                e.eventID = EventTriggerType.BeginDrag;
                e.callback.AddListener((eventData) => { TouchDown(); });
                et.triggers.Add(e);
            }

            if (!hasEndDrag)
            {
                EventTrigger.Entry e = new EventTrigger.Entry();
                e.eventID = EventTriggerType.EndDrag;
                e.callback.AddListener((eventData) => { TouchUp(); });
                et.triggers.Add(e);
            }

            if (!hasOnDrag)
            {
                EventTrigger.Entry e = new EventTrigger.Entry();
                e.eventID = EventTriggerType.Drag;
                e.callback.AddListener((eventData) => { OnDrag(); });
                et.triggers.Add(e);
            }
        }

        protected bool scrollIsInited = false;

        public override void InitScrollContainer<T>(List<T> data)
        {
            if (scrollIsInited)
            {
                return;
            }

            foreach (T d in data)
            {
                IElement obj = Instantiate(scrollElement, scrollContainer);
                obj.gameObject.SetActive(true);
                obj.Init(d);
                elements.Add(obj);
            }

            if (elements.Count > 1)
            {
                foreach (IElement o in elements)
                {
                    if (aligment == AligmentType.Horizontal)
                    {
                        distance.Add(Mathf.Abs(scroll.position.x - o.transform.position.x));
                    }
                    else
                    {
                        distance.Add(Mathf.Abs(scroll.position.y - o.transform.position.y));
                    }
                }
                _state = ScrollState.Default;
            }           
            scrollIsInited = true;
        }

        private void FixedUpdate()
        {
            if (!Active)
                return;

            switch (_state)
            {
                case ScrollState.Default:
                    DefaultState();
                    break;
                case ScrollState.Move:
                    MoveState();
                    break;
                case ScrollState.MoveToNext:
                    MoveToNext();
                    break;
                case ScrollState.MoveToConcrete:
                    MoveToConcrete();
                    break;
            }       
        }

#region Move states
        protected virtual void DefaultState()
        {
            ClaculateDistance();
            UpdateSelectedElement();
            LerpToElement(SelectedElementId);
        }

        protected virtual void MoveState()
        {
            ClaculateDistance();
            UpdateSelectedElement();
        }

        protected virtual void MoveToNext()
        {
            ClaculateDistance();
            LerpToElement(SelectedElementId);
            if (minDistance == 0)
            {
                SetState(ScrollState.Default);
            }
        }

        protected virtual void MoveToConcrete()
        {
            ClaculateDistance();
            LerpToElement(SelectedElementId);
            if (distance[SelectedElementId] <= minDistanceForMoveToElement)
            {
                SetState(ScrollState.Default);
            }
        }
#endregion
        
        protected Vector3 oldpos;

        public void OnDrag()
        {
            StartDrag();         
        }

        public void TouchDown()
        {
            BeginDrag();
        }

        public void TouchUp()
        {
            EndDrag();
        }

        protected virtual void StartDrag()
        {
            if (!Active)
                return;

            Vector3 pos = Input.mousePosition;
            Vector3 dltPos;
            float l = 0;
            if (aligment == AligmentType.Vertical)
            {
                if (pos.y != oldpos.y)
                {
                    dltPos = pos - oldpos;

                    l = dltPos.y < 0 ? (Mathf.Abs(dltPos.y) * Time.deltaTime) : -(Mathf.Abs(dltPos.y) * Time.deltaTime);
                }
            }
            else
            {
                if (pos.x != oldpos.x)
                {
                    dltPos = pos - oldpos;

                    l = dltPos.x < 0 ? (Mathf.Abs(dltPos.x) * Time.deltaTime) : -(Mathf.Abs(dltPos.x) * Time.deltaTime);
                }
            }
            MoveScrollContainer(-l * swipeSpeed);
            oldpos = pos;
        }

        protected virtual void BeginDrag()
        {
            if (!Active)
                return;
            Drag = true;
            oldpos = Input.mousePosition;
        }

        protected virtual void EndDrag()
        {
            if (!Active)
                return;
            Drag = false;
        }

        protected virtual void MoveScrollContainer(float l)
        {
            if (aligment == AligmentType.Horizontal)
            {
                scrollContainer.transform.position += new Vector3(l, 0, 0);
            }
            else
            {
                scrollContainer.transform.position += new Vector3(0, l, 0);
            }
        }

        protected virtual void SetState(ScrollState s)
        {
            _state = s;
        }

        public virtual void SetIndexAndPosition(int id)
        {
            SelectedElementId = id;
            float newX = GetIndexPosition(id);
            UpdatePosition(newX);
            SetState(ScrollState.Default);
        }

        protected virtual void ClaculateDistance()
        {
            for (int i = 0; i < elements.Count; i++)
            {
                if (aligment == AligmentType.Horizontal)
                {
                    distance[i] = Mathf.Abs(scroll.position.x - elements[i].transform.position.x);
                }
                else
                {
                    distance[i] = Mathf.Abs(scroll.position.y - elements[i].transform.position.y);
                }
            }
        }

        protected virtual void UpdateSelectedElement()
        {
            SelectedElementId = GetSelectedIndex(ref minDistance);
        }

        protected virtual int GetSelectedIndex(ref float minDistance)
        {
            int id = 0;
            minDistance = float.MaxValue;

            for (int i = 0; i < distance.Count; i++)
            {
                if (minDistance > distance[i])
                {
                    minDistance = distance[i];
                    id = i;
                }
            }
            return id;
        }

        protected virtual void LerpToElement(int id)
        {
            float newAxisValue;
            if (aligment == AligmentType.Horizontal)
            {
                if (lerp)
                {
                    newAxisValue = Mathf.Lerp(scrollContainer.GetComponent<RectTransform>().anchoredPosition.x, GetIndexPosition(id), Time.deltaTime * scrollSpeed);
                }
                else
                {
                    newAxisValue = Mathf.MoveTowards(scrollContainer.GetComponent<RectTransform>().anchoredPosition.x, GetIndexPosition(id), Time.deltaTime * scrollSpeed);
                }
            }
            else
            {
                if (lerp)
                {
                    newAxisValue = Mathf.Lerp(scrollContainer.GetComponent<RectTransform>().anchoredPosition.y, -GetIndexPosition(id), Time.deltaTime * scrollSpeed);
                }
                else
                {
                    newAxisValue = Mathf.MoveTowards(scrollContainer.GetComponent<RectTransform>().anchoredPosition.y, -GetIndexPosition(id), Time.deltaTime * scrollSpeed);
                }
            }
            UpdatePosition(newAxisValue);
        }

        protected virtual float GetIndexPosition(int id)
        {
            if (Elements.Count % 2 == 0)
            {
                return ((id - Elements.Count / 2) * -elementDistance) - elementDistance / 2.0f;
            }
            else
            {
                return (id - (Elements.Count - 1) / 2) * -elementDistance;
            }
        }

        protected virtual void UpdatePosition(float newAxisValue)
        {
            Vector2 newPos;
            if (aligment == AligmentType.Horizontal)
            {
                newPos = new Vector2(newAxisValue, scrollContainer.GetComponent<RectTransform>().anchoredPosition.y);
            }
            else
            {
                newPos = new Vector2(scrollContainer.GetComponent<RectTransform>().anchoredPosition.x, newAxisValue);
            }
            scrollContainer.GetComponent<RectTransform>().anchoredPosition = newPos;
        }
    }
}