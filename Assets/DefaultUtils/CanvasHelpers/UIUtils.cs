﻿using UnityEngine;
using System.Collections;

public static class UIUtils
{
    public static Vector3 GetCenterPosition(this RectTransform tr, float sizeCoff)
    {
        Vector3 pos = tr.position;
        pos.x += (tr.sizeDelta.x / 2.0f);// * sizeCoff;
        pos.y += (tr.sizeDelta.y / 2.0f);// * sizeCoff;
        return pos;
    }

    public static Vector2 screenCenter = Vector2.zero;
    public static Vector2 ScreenCenter
    {
        get
        {
            if(screenCenter == Vector2.zero)
            {
                screenCenter = new Vector2(Screen.width / 2.0f, Screen.height / 2.0f);
            }
            return screenCenter;
        }
    }
}
