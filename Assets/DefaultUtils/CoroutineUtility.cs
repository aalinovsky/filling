﻿using UnityEngine;
using System.Collections;
using System;

namespace DefaultUtility
{
    public static class CoroutineUtility
    {
        public static void WaitAndStart(this MonoBehaviour behaviour, Action act, float delay)
        {
            behaviour.StartCoroutine(WaitAndStartCoroutine(act, delay));
        }

        public static IEnumerator WaitAndStartCoroutine(Action act, float delay)
        {
            yield return new WaitForSeconds(delay);
            act();
        }
    }
}