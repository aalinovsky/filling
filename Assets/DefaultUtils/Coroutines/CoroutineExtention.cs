﻿using UnityEngine;
using System.Collections;
using System;

public static class CoroutineExtention 
{
    public static IEnumerator WaitAndStart(this MonoBehaviour parent, float time, Action act)
    {
        IEnumerator cor = WaitAndPlay(time, act);
        parent.StartCoroutine(cor);
        return cor;
    }

    public static IEnumerator WaitEndOfFrameAndStart(this MonoBehaviour parent, Action act)
    {
        IEnumerator cor = WaitAndOfFramePlay(act);
        parent.StartCoroutine(cor);
        return cor;
    }

    public static IEnumerator StartLerp(this MonoBehaviour parent, float duration, Action<float> act, Action finalFunction)
    {
        IEnumerator lerp = Lerp(duration, act, finalFunction);
        parent.StartCoroutine(lerp);
        return lerp;
    }

    public static IEnumerator WaitAndPlay(float time, Action act)
    {
        yield return new WaitForSeconds(time);
        act();
    }

    public static IEnumerator WaitAndOfFramePlay(Action act)
    {
        yield return new WaitForEndOfFrame();
        act();
    }

    public static IEnumerator Lerp(float time, Action<float> func, Action endFunc)
    {
        var timer = 0f;
        var dlt = 0f;
        while (dlt < 1.0f)
        {
            dlt = Mathf.Min(timer / time, 1.0f);
            func(dlt);
            yield return null;
            timer += Time.deltaTime;
        }

        if (endFunc != null)
            endFunc();
    }
}
