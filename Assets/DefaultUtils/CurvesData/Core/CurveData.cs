﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DefaultUtility
{
    [CreateAssetMenu(fileName = "CurveData", menuName = "DefaultUtility/Curve", order = 51)]
    public class CurveData : ScriptableObject
    {
        [SerializeField]
        protected AnimationCurve curve;

        public bool loopCurve = false;

        [ConditionalFieldAttribute("loopCurve", true)]
        public float loopLength;

        public AnimationCurve Curve
        {
            get
            {
                return curve;
            }
        }

        public float GetValue(float time)
        {
            float t = time;
            if (loopCurve)
            {
                if (t > loopLength)
                {
                    t = time % loopLength;
                }
            }

            return curve.Evaluate(t);
        }

        public float GetDltValue(float dlt)
        {
            return GetValue(loopLength * dlt);
        }

#if UNITY_EDITOR
        [Button(ButtonMode.DisabledInPlayMode)]
        public void MoveFirstPointToZero()
        {
            Keyframe[] keys = curve.keys;
            if (keys.Length > 0)
            {
                keys[0].time = 0;
                curve.keys = keys;
            }
            else
            {
                if (UnityEditor.EditorUtility.DisplayDialog("Curve data", "Curve has no points. Create one?", "Yes", "No"))
                {
                    Keyframe[] key = new Keyframe[1];
                    key[0].time = 0;
                    key[0].value = 0;
                    curve.keys = key;
                }
            }
        }

        [Button(ButtonMode.DisabledInPlayMode)]
        public void MoveSecondPointToEnd()
        {
            if (!loopCurve)
            {
                UnityEditor.EditorUtility.DisplayDialog("Curve data", "This function work in loop mode only", "Ok");
                return;
            }

            Keyframe[] keys = curve.keys;
            if (keys.Length > 1)
            {
                keys[keys.Length-1].time = loopLength;
                curve.keys = keys;
            }
            else
            {
                if (UnityEditor.EditorUtility.DisplayDialog("Curve data", "Curve has no second point. Create?", "Yes", "No"))
                {
                    Keyframe[] resultKeys = new Keyframe[2];
                    keys.CopyTo(resultKeys, 0);
                    resultKeys[1] = new Keyframe(loopLength, 0);
                    curve.keys = resultKeys;
                }
            }
        }
#endif
    }

    [System.Serializable]
    public class CurvesData
    {
        public List<CurveData> curves = new List<CurveData>();
    }
}
