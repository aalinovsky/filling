﻿using System;
using UnityEngine;

namespace DefaultUtility
{
    public static class DailyVisit
    {
        private const string FIRST_VISIT = "firstVisit";
        private const string LAST_VISIT = "lastVisit";
        private const string NUMBER_DAYS_VISITED = "numberDaysVisited";
        private const string NUMBER_DAYS_VISITED_IN_ROW = "numberDaysVisitedInARow";

        public static DayType nowDayType = DayType.None;

        public static void SessionStarted()
        {
            DateTime first = FirstVisitDate;
            DateTime last = LastVisitDate;
        }

        public static int NumberDaysVisited
        {
            get
            {
                return PlayerPrefsUtility.GetEncryptedInt(NUMBER_DAYS_VISITED, 0);
            }

            set
            {
                PlayerPrefsUtility.SetEncryptedInt(NUMBER_DAYS_VISITED, value);
            }
        }

        public static int NumberDaysVisitedInARow
        {
            get
            {
                return PlayerPrefsUtility.GetEncryptedInt(NUMBER_DAYS_VISITED_IN_ROW, 1);
            }

            set
            {
                PlayerPrefsUtility.SetEncryptedInt(NUMBER_DAYS_VISITED_IN_ROW, value);
            }
        }

        public static int NumberDaysSinceTheFirstLaunch
        {
            get
            {
                int days = LastVisitDate.DayOfYear - FirstVisitDate.DayOfYear;
                int years = LastVisitDate.Year - FirstVisitDate.Year;
                if (years > 0)
                {
                    if (years == 1)
                    {
                        days = 365 - FirstVisitDate.DayOfYear + Math.Abs(days);                        
                    }
                    else
                    {
                        days = 365 - FirstVisitDate.DayOfYear + Math.Abs(days) + 365 * (years - 1);
                    }
                }
                return days + 1;
            }
        }

        public static int GetNumberDaysVisited(this MonoBehaviour mb)
        {
            InitDayType();
            return NumberDaysVisited;
        }

        public static int GetNumberDaysVisitedInRow(this MonoBehaviour mb)
        {
            InitDayType();
            return NumberDaysVisitedInARow;
        }

        public static int GetNumberDaysSinceTheFirstLaunch(this MonoBehaviour mb)
        {
            InitDayType();
            return NumberDaysSinceTheFirstLaunch;
        }

        public static DateTime GetLastVisitDate(this MonoBehaviour mb)
        {
            return LastVisitDate;
        }

        public static DateTime LastVisitDate
        {
            get
            {
                return PlayerPrefsUtility.GetDateTime(LAST_VISIT, DateTime.Now);
            }

            set
            {
                PlayerPrefsUtility.SetDateTime(LAST_VISIT, value);
            }
        }

        public static DateTime GetFirstVisitDate(this MonoBehaviour mb)
        {
            return FirstVisitDate;
        }

        public static DateTime FirstVisitDate
        {
            get
            {
                return PlayerPrefsUtility.GetDateTime(FIRST_VISIT, DateTime.Now);
            }

            set
            {
                PlayerPrefsUtility.SetDateTime(FIRST_VISIT, value);
            }
        }
        
        public static DayType CurrentDayType
        {
            get
            {
                return InitDayType();
            }
        }

        public static bool IsFirstLaunch
        {
            get
            {
                DateTime nowDate = DateTime.Now;
                DateTime lastVisit = LastVisitDate;
                if (lastVisit.DayOfYear == nowDate.DayOfYear && lastVisit.Minute == nowDate.Minute && lastVisit.Second == nowDate.Second)
                {
                    return true;
                }
                return false;
            }
        }

        public static DayType InitDayType()
        {
            if (nowDayType != DayType.None)
            {
                return nowDayType;
            }

            nowDayType = DayType.Default;
            DateTime nowDate = DateTime.Now;
            DateTime lastVisit = LastVisitDate;

            if (lastVisit.DayOfYear == nowDate.DayOfYear && lastVisit.Minute == nowDate.Minute && lastVisit.Second == nowDate.Second)
            {
                //DevLogs.Log(("First Launch today " + lastVisit.ToLongTimeString()).SetColor(Color.red));
                LastVisitDate = nowDate;
                NumberDaysVisited = 1;
                NumberDaysVisitedInARow = 1;
                nowDayType = DayType.DayInARow;
                return nowDayType;
            }

            nowDate.AddSeconds(-1);

            //if enter in game after new year            
            if (lastVisit.Year < nowDate.Year)
            {
                int daysInYear = DateTime.IsLeapYear(lastVisit.Year) ? 366 : 365;
                if (lastVisit.DayOfYear == daysInYear && nowDate.DayOfYear == 1)
                {
                    LastVisitDate = nowDate;
                    nowDayType = DayType.DayInARow;
                    NumberDaysVisited++;
                    NumberDaysVisitedInARow++;
                    return nowDayType;
                }
                else
                {
                    LastVisitDate = nowDate;
                    nowDayType = DayType.DaiInARowFail;
                    NumberDaysVisited++;
                    NumberDaysVisitedInARow = 1;
                    return nowDayType;
                }
            }

            if (lastVisit.DayOfYear == nowDate.DayOfYear)
            {
                //DevLogs.Log("Launch in thit day".SetColor(Color.red));
                return nowDayType;
            }
            else if (lastVisit.DayOfYear + 1 == nowDate.DayOfYear)
            {
                //DevLogs.Log("Day in a row lauunch".SetColor(Color.red));

                LastVisitDate = nowDate;
                nowDayType = DayType.DayInARow;
                NumberDaysVisited++;
                NumberDaysVisitedInARow++;
                return nowDayType;
            }
            else if (lastVisit.DayOfYear + 1 < nowDate.DayOfYear)
            {
                LastVisitDate = nowDate;
                nowDayType = DayType.DaiInARowFail;
                NumberDaysVisited++;
                NumberDaysVisitedInARow = 1;
                return nowDayType;
            }
            return nowDayType;
        }
    }

    public enum DayType
    {
        None,
        Default,
        DayInARow,
        DaiInARowFail
    }
}
