﻿using UnityEngine;
using System.Collections;

public class SessionsCounter : MonoBehaviour
{
    public bool sendData = true;

    public static int Sessions
    {
        get
        {
            return PlayerPrefsUtility.GetEncryptedInt("SessionsCountCounter");
        }

        set
        {
            PlayerPrefsUtility.SetEncryptedInt("SessionsCountCounter", value);
        }
    }

    private void Awake()
    {
        Sessions++;
    }

    private void Start()
    {
        if (sendData)
            SendDataToAnalytic();
    }

    protected virtual void SendDataToAnalytic()
    {

    }
}
