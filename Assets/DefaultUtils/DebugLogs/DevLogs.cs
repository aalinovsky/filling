﻿using UnityEngine;
using System.Collections;
using DefaultUtility;

public static class DevLogs
{
    public static void Log(string val, Color col)
    {
#if DEV_LOGS
        Debug.Log(val.SetColor(col));
#endif
    }

    public static void Log(string val)
    {
#if DEV_LOGS
        Debug.Log(val);
#endif
    }

    public static void LogError(string val)
    {
#if DEV_LOGS
        Debug.LogError(val);
#endif
    }

    public static void LogEvent(string val, Color col)
    {
#if DEV_EVENT
        Debug.Log(val.SetColor(col));
#endif
    }

    public static void LogEvent(string val)
    {
#if DEV_EVENT
        DevLogs.Log(val);
#endif
    }

    public static void LogData(string val, Color col)
    {
#if DEV_DATA
        Debug.Log(val.SetColor(col));
#endif
    }

    public static void LogData(string val)
    {
#if DEV_DATA
        Debug.Log(val);
#endif
    }
}
