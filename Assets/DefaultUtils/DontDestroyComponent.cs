﻿using UnityEngine;
using System.Collections;

public class DontDestroyComponent : MonoBehaviour
{
    private void Awake()
    {
        DontDestroyOnLoad(this);
    }
}
