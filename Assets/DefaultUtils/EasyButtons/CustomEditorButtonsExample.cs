﻿using UnityEngine;

namespace EasyButtons
{
    public class CustomEditorButtonsExample : MonoBehaviour
    {
        [Button("Custom Editor Example")]
        private void SayHello()
        {
            DevLogs.Log("Hello from custom editor");
        }
    }
}
