﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using DefaultUtility;

namespace DefaultUtility
{
    public abstract class TypeEditor : MonoBehaviour
    {
#if UNITY_EDITOR
        [HideInInspector]
        public Type enumType;
        [HideInInspector]
        public string path;

        public string[] arrayOne;

        public abstract void InitValues();

        //[ContextMenu("Fill")]
        [Button("Update enums")]
        public void Fill()
        {
            InitValues();
            var e1 = new CreateEnum.EnumClass(arrayOne.ToList(), enumType.Name);
            CreateEnum.CreateEnumsInFile(path, e1);
        }

        //[ContextMenu("Init")]
        [Button("Init enums")]
        public void Init()
        {
            InitValues();
            arrayOne = Enum.GetNames(enumType);
        }
#endif
    }
}
