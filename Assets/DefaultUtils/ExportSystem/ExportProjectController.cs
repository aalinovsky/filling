﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

namespace DefaultUtility
{
    public class ExportProjectController : MonoBehaviour
    {

        [MenuItem("Export/Start")]
        public static void Export()
        {
            EditorUtility.DisplayDialog("Export Package", "Unity start exporting you project", "OK");

            string[] projectContent = AssetDatabase.GetAllAssetPaths();
            AssetDatabase.ExportPackage(projectContent, string.Format("{0}_{2}_{1}", Application.productName, "Template.unitypackage", Application.version), ExportPackageOptions.Recurse | ExportPackageOptions.IncludeLibraryAssets | ExportPackageOptions.IncludeDependencies);
            EditorUtility.DisplayDialog("Export Package", "Exporting completed", "OK");
        }
    }
}
#endif