﻿using UnityEngine;
using System.Collections;

public static class GameObjectsUtility
{
    public static void UpOnHierarchy(this Transform obj)
    {
        Transform parent = obj.parent;
        obj.SetSiblingIndex(parent.childCount);
    }

    public static void DownOnHierarchy(this Transform obj)
    {
        //Transform parent = obj.parent;
        obj.SetSiblingIndex(0);
    }

    public static void UnderTargetOnHierarchy(this Transform obj, Transform target)
    {
        int index = target.GetSiblingIndex()-1;
        if (index < 0)
            index = 0;
        obj.SetSiblingIndex(index);
    }
}
