﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

public abstract class GuidObject : ScriptableObject
{
    #region SecretKey
    [ConditionalHide]
    public string SecretUniqueKey;
    protected System.Guid _guid;
    public System.Guid guid
    {
        get
        {
            if (_guid != null && _guid != System.Guid.Empty)
                return _guid;

            if (_guid == System.Guid.Empty || System.String.IsNullOrEmpty(SecretUniqueKey))
            {                
                _guid = System.Guid.NewGuid();
            }
            else
            {
                _guid = new System.Guid(SecretUniqueKey);
            }
            return _guid;
        }
    }

    [DefaultUtility.Button()]
    public void Generate()
    {
        _guid = System.Guid.NewGuid();
        SecretUniqueKey = _guid.ToString();
        if (System.String.IsNullOrEmpty(SecretUniqueKey))
        {
            SecretUniqueKey = guid.ToString();
        }
        else if (_guid == null || _guid == System.Guid.Empty)
        {
            _guid = new System.Guid(SecretUniqueKey);
        }
    }
    #endregion

    private void OnEnable()
    {
        if (guid == System.Guid.Empty)
        {
            Generate();
        }
    }

    void OnValidate()
    {
        if (guid == System.Guid.Empty)
        {
            Generate();
        }
        else if (string.IsNullOrEmpty(SecretUniqueKey))
        {
            SecretUniqueKey = guid.ToString();
        }
    }
}


#if UNITY_EDITOR 
[CustomEditor(typeof(GuidObject))]
class GuidObjectInspector : Editor
{
    void OnGui()
    {
        GuidObject guid = (GuidObject)target;

        if (guid.guid == System.Guid.Empty)
        {
            guid.Generate();
            EditorUtility.SetDirty(target);
        }
        else if (string.IsNullOrEmpty(guid.SecretUniqueKey))
        {
            guid.SecretUniqueKey = guid.ToString();
            EditorUtility.SetDirty(target);
        }
    }
}
#endif