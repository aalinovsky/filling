﻿using System;
using UnityEngine;

[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
public class ConditionalFieldAttribute : PropertyAttribute
{
    public string PropertyToCheck;

    public object CompareValue;

    public Field[] fields;

    public ConditionalFieldAttribute(string propertyToCheck, object compareValue = null)
    {
        PropertyToCheck = propertyToCheck;
        CompareValue = compareValue;
        fields = null;
    }

    public ConditionalFieldAttribute(string propertyToCheck, object[] compareValues = null)
    {
        string[] properties = propertyToCheck.Split(',');
        if (compareValues.Length == properties.Length)
        {
            fields = new Field[compareValues.Length];
            for (int i = 0; i < compareValues.Length; i++)
            {
                fields[i] = new Field(properties[i], compareValues[i]);
            }
        }

        PropertyToCheck = null;
        CompareValue = null;
    }
}

[System.Serializable]
public class Field
{
    public string PropertyToCheck;

    public object CompareValue;

    public Field(string propName, object propValue)
    {
        PropertyToCheck = propName;
        CompareValue = propValue;
    }
}