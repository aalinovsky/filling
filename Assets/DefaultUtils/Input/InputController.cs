﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System;

public class InputController : Singletone<InputController>
{
    public bool enableTouchOnUi = false;

    public float distanceToTouch = 10;

    GraphicRaycaster m_Raycaster;
    PointerEventData m_PointerEventData;
    [SerializeField]
    EventSystem m_EventSystem;

    protected event Action<float> swipeX;
    protected event Action<float> swipeY;
    protected event Action<Vector3> touch;
    protected event Action<Vector3> move;
    protected event Action touchDown;
    protected event Action<Vector3> touchUp;

    public void SubscribeOnSwipeX(Action<float> action)
    {
        swipeX += action;
    }

    public void UnsubscribeOnSwipeX(Action<float> action)
    {
        swipeX -= action;
    }

    public void SubscribeOnSwipeY(Action<float> action)
    {
        swipeY += action;
    }

    public void UnsubscribeOnSwipeY(Action<float> action)
    {
        swipeY -= action;
    }

    public void SubscribeOnTouch(Action<Vector3> action)
    {
        touch += action;
    }

    public void UnsubscribeOnTouch(Action<Vector3> action)
    {
        touch -= action;
    }

    public void SubscribeOnMove(Action<Vector3> action)
    {
        move += action;
    }

    public void UnsubscribeOnMove(Action<Vector3> action)
    {
        move -= action;
    }

    public void SubscribeOnTouchDown(Action action)
    {
        touchDown += action;
    }

    public void UnsubscribeOnTouchDown(Action action)
    {
        touchDown -= action;
    }

    public void SubscribeOnTouchUp(Action<Vector3> action)
    {
        touchUp += action;
    }

    public void UnsubscribeOnTouchUp(Action<Vector3> action)
    {
        touchUp -= action;
    }

    void Start()
    {
        m_Raycaster = GetComponent<GraphicRaycaster>();
        if(m_EventSystem == null)
            m_EventSystem = GetComponent<EventSystem>();
        touchDown += () => { };
        touchUp += (v) => { };
        swipeX += (a) => { };
        swipeY += (a) => { };
        touch += (v) => { };
    }
    Vector3 startPos;

    bool isTouch = false;
    Vector2 touchDeltaPosition;

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (!HasUiOnTouch())
            {
                isTouch = true;
                touchDown();
                startPos = new Vector3(Input.mousePosition.x, 0, 0);

                Vector3 touchPosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, distanceToTouch);
                touchPosition = Camera.main.ScreenToWorldPoint(touchPosition);
                touch(touchPosition);
            }
        }

        if (Input.GetMouseButton(0) && isTouch)
        {
            Vector3 pos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);

            Vector3 f = pos - startPos;
            float l = 0;
            if (pos.magnitude != startPos.magnitude)
            {
                if (pos.x != startPos.x)
                {
                    dltPos = f;
                    dltPos.y = 0;
                    l = f.x < 0 ? (dltPos.magnitude * Time.deltaTime) : -(dltPos.magnitude * Time.deltaTime);
                    swipeX(l);
                }

                if (pos.y != startPos.y)
                {
                    dltPos = f;
                    dltPos.x = 0;
                    l = f.y < 0 ? (dltPos.magnitude * Time.deltaTime) : -(dltPos.magnitude * Time.deltaTime);
                    swipeY(l);
                }
                startPos = pos;
                move(pos);
            }
        }

        if (Input.GetMouseButtonUp(0) && isTouch)
        {
            isTouch = false;
            Vector3 touchPosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, distanceToTouch);
            touchPosition = Camera.main.ScreenToWorldPoint(touchPosition);
            touchUp(touchPosition);
        }
    }

    Vector3 dltPos;

    private void FixedUpdate()
    {
        
    }

    protected bool HasUiOnTouch()
    {
        if (!enableTouchOnUi)
            return false;

        m_PointerEventData = new PointerEventData(m_EventSystem);
        m_PointerEventData.position = Input.mousePosition;

        List<RaycastResult> results = new List<RaycastResult>();

        m_Raycaster.Raycast(m_PointerEventData, results);

        if (results.Count == 0)
        {
            return false;
        }
        return true;
    }
}
