﻿using UnityEngine;

namespace DefaultUtility
{
    public class ButtonsExample : MonoBehaviour
    {
        // Example use of the ButtonAttribute
        [Button]
        public void SayMyName()
        {
            DevLogs.Log(name);
        }

        // Example use of the ButtonAttribute that is not shown in play mode
        [Button(ButtonMode.DisabledInPlayMode)]
        protected void SayHelloEditor()
        {
            DevLogs.Log("Hello from edit mode");
        }

        // Example use of the ButtonAttribute that is only shown in play mode
        [Button(ButtonMode.EnabledInPlayMode)]
        private void SayHelloInRuntime()
        {
            DevLogs.Log("Hello from play mode");
        }

        // Example use of the ButtonAttribute with custom name
        [Button("Special Name")]
        private void TestButtonName()
        {
            DevLogs.Log("Hello from special name button");
        }

        // Example use of the ButtonAttribute with custom name and button mode
        [Button("Special Name Editor Only", ButtonMode.DisabledInPlayMode)]
        private void TestButtonNameEditorOnly()
        {
            DevLogs.Log("Hello from special name button for editor only");
        }
    }
}
