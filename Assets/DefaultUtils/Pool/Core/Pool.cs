﻿using UnityEngine;
using System.Collections;

public abstract class Pool<T> : SimplePool<T> where T : Component
{
    public Transform parent;

    protected override T Instantiate()
    {
        T pooledObj = null;
        if (parent != null)
        {
            pooledObj = Instantiate(prefab, parent);
}
        else
        {
            pooledObj = Instantiate(prefab, null);
        }
        pool.Add(pooledObj);
        return pooledObj;
    }
}
