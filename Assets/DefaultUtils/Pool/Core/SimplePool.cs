﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class SimplePool<T> : MonoBehaviour where T : Component
{
    //public Transform parent;    
    public T prefab;
    [Space()]
    public bool initOnStart = false;
    public int countInstancedObjectsOnStart;

    protected List<T> pool = new List<T>();
    protected Vector3 nextPos;

    private void Start()
    {        
        Init();
    }

    protected void Init()
    {
        if (initOnStart)
        {
            for (int i = 0; i < countInstancedObjectsOnStart; i++)
            {
                Instantiate().gameObject.SetActive(false);
            }
        }
    }

    protected abstract T Instantiate();

    public virtual T Spawn(Vector3 pos, Quaternion rot)
    {
        T o = Spawn();
        o.transform.SetPositionAndRotation(pos, rot);
        return o;
    }

    public virtual T Spawn()
    {
        ClearAllEmptyObjects();
        T pooledObj = null;

        for (int i = 0; i < pool.Count; i++)
        {
            if (!pool[i].gameObject.activeInHierarchy)
            {
                pooledObj = pool[i];
                pooledObj.gameObject.SetActive(true);
                break;
            }
        }

        if (pooledObj == null)
        {
            pooledObj = Instantiate();
        }
        return pooledObj;
    }

    public virtual void Despawn(T o)
    {
        if (o != null)
        {
            o.gameObject.SetActive(false);
        }
        else
        {
            ClearAllEmptyObjects();
        }
    }

    protected void ClearAllEmptyObjects()
    {
        for (int i = pool.Count - 1; i >= 0; i--)
        {
            if (pool[i] == null)
                pool.RemoveAt(i);
        }
    }

    protected void ClearScene()
    {
        for (int i = 0; i < pool.Count; i++)
        {
            if (pool[i] != null && pool[i].gameObject.activeInHierarchy)
            {
                Despawn(pool[i]);
                break;
            }
        }
    }

    public virtual void Reset()
    {
        ClearScene();
    }
}
