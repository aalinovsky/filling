﻿using UnityEngine;
using System.Collections;

public class UiPool<T> : SimplePool<T> where T : Component
{
    public RectTransform parentRect;

    protected override T Instantiate()
    {
        T pooledObj = null;
        if (parentRect != null)
        {
            pooledObj = Instantiate(prefab.gameObject, parentRect).GetComponent<T>();
        }
        else
        {
            pooledObj = Instantiate(prefab.gameObject, null).GetComponent<T>();
        }
        pool.Add(pooledObj);
        return pooledObj;
    }
}
