﻿using UnityEngine;
using System.Collections;

public class ParticlesPool : Pool<ParticleSystem>
{
    public override ParticleSystem Spawn()
    {
        ParticleSystem ps = base.Spawn();
        ps.Simulate(0, true, true);
        ps.Play(true);
        return ps;
    }

    public override void Despawn(ParticleSystem o)
    {
        o.Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);
        base.Despawn(o);        
    }
}
