﻿using UnityEngine;
using System.Collections;

public static class RandomUtility
{
    public static object GetRandom(this object[] arr)
    {
        return arr[UnityEngine.Random.Range(0, arr.Length)];
    }

    public static float GetRandom(this float val)
    {
        return UnityEngine.Random.Range(-val, val);
    }
}
