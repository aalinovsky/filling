﻿using UnityEngine;
using System.Collections;

public static class ScreenOrientation
{
    public static bool IsVertical
    {
        get
        {
            if(Screen.orientation == UnityEngine.ScreenOrientation.Portrait || Screen.orientation == UnityEngine.ScreenOrientation.PortraitUpsideDown)
            {
                return true;
            }
            return false;
        }
    }
}
