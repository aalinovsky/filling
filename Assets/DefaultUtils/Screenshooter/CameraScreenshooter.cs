﻿using UnityEngine;
using System.Collections;
using System.IO;

[RequireComponent(typeof(Camera))]
public class CameraScreenshooter : MonoBehaviour
{
#if UNITY_EDITOR
    public float resWidth = 2550;
    public float resHeight = 3300;

    protected Camera myCamera;
    private bool takeHiResShot = false;

    protected int Width
    {
        get
        {
            return (int)resWidth;
        }
    }

    protected int Height
    {
        get
        {
            return (int)resHeight;
        }
    }

    private void Awake()
    {
        myCamera = GetComponent<Camera>();
    }

    public static string ScreenShotName(int width, int height)
    {
        if (!Directory.Exists(Application.dataPath + "/Screenshots"))
        {
            Directory.CreateDirectory(Application.dataPath + "/Screenshots");
        }

        return string.Format("{0}/Screenshots/screen_{1}x{2}_{3}.png",
                             Application.dataPath,
                             width, height,
                             System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"));
    }

    [DefaultUtility.Button(DefaultUtility.ButtonMode.EnabledInPlayMode)]
    public void TakeHiResShot()
    {
        if(!takeHiResShot)
         takeHiResShot = true;
    }

    void LateUpdate()
    {
        takeHiResShot |= Input.GetKeyDown("c");
        if (takeHiResShot)
        {
            RenderTexture rt = new RenderTexture(Width, Height, 24);
            myCamera.targetTexture = rt;
            Texture2D screenShot = new Texture2D(Width, Height, TextureFormat.RGB24, false);
            myCamera.Render();
            RenderTexture.active = rt;
            screenShot.ReadPixels(new Rect(0, 0, resWidth, resHeight), 0, 0);
            myCamera.targetTexture = null;
            RenderTexture.active = null; // JC: added to avoid errors
            Destroy(rt);
            byte[] bytes = screenShot.EncodeToPNG();
            string filename = ScreenShotName(Width, Height);
            System.IO.File.WriteAllBytes(filename, bytes);
            Debug.Log(string.Format("Took screenshot to: {0}", filename));
            takeHiResShot = false;
        }
    }
#endif
}
