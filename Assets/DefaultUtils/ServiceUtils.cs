﻿using UnityEngine;
using System.Collections;

public static class ServiceUtils
{
    public static void OpenUrl(this MonoBehaviour mono, string url)
    {
        Application.OpenURL(url);
    }
}
