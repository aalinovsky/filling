﻿using UnityEngine;
using System.Collections;

public class BoolStorageData : StorageData<bool>
{
    public BoolStorageData(string key) : base(key)
    {

    }

    public BoolStorageData(string key, bool autoSave) : base(key, autoSave)
    {

    }

    public BoolStorageData(string key, bool autoSave, bool defaultValue) : base(key, autoSave)
    {
        this.defaultValue = defaultValue;
    }

    public override void Init()
    {
        value = PlayerPrefsUtility.GetBool(datakey, defaultValue);
        base.Init();
    }

    public override void Save()
    {
        PlayerPrefsUtility.SetBool(datakey, value);
    }
}
