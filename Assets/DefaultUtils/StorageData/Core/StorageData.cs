﻿using UnityEngine;
using System.Collections;
using System;

public abstract class StorageData<T>
{
    public T defaultValue;
    public bool autoSave = true;
    protected string datakey;
    protected bool inited = false;
    protected T value;

    public delegate void SetsListenerDelegate();
    public delegate T GetterDelegate();
    public delegate void SetterDelegate(T t);

    protected event GetterDelegate getter;
    protected event SetterDelegate setter;
    protected event SetsListenerDelegate setValue;

    public T Value
    {
        get
        {
            if (!inited)
                Init();

            if (getter != null)
            {
                return getter();
            }
            else
            {
                return value;
            }
        }

        set
        {
            if (!inited)
                Init();

            this.value = value;

            if (setter != null)
            {
                setter(value);
            }

            if(setValue != null)
            {
                setValue();
            }

            if (autoSave)
            {
                Save();
            }
        }
    }
    
    public StorageData(string key, bool autoSave = true)
    {
        this.datakey = key;
        this.autoSave = autoSave;
    }

    public void RegisterSetValueCallback(SetsListenerDelegate a)
    {
        setValue += a;
    }

    public void UnregisterSetValueCallback(SetsListenerDelegate a)
    {
        setValue -= a;
    }

    public void RegisterGetter(GetterDelegate a)
    {
        getter += a;
    }

    public void UnregisterGetter(GetterDelegate a)
    {
        getter -= a;
    }

    public void RegisterSetter(SetterDelegate a)
    {
        setter += a;
    }

    public void UnregisterSetter(SetterDelegate a)
    {
        setter -= a;
    }

    public virtual void Init()
    {        
        inited = true;
    }

    public abstract void Save();
}
