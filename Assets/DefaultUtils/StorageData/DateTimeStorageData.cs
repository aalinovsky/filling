﻿using UnityEngine;
using System.Collections;
using System;

public class DateTimeStorageData : StorageData<DateTime>
{
    public DateTimeStorageData(string key) : base(key)
    {

    }

    public DateTimeStorageData(string key, bool autoSave) : base(key, autoSave)
    {

    }

    public override void Init()
    {
        value = PlayerPrefsUtility.GetDateTime(datakey);
        base.Init();
    }

    public override void Save()
    {
        PlayerPrefsUtility.SetDateTime(datakey, value);
    }
}
