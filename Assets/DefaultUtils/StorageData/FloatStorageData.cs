﻿using UnityEngine;
using System.Collections;

public class FloatStorageData : StorageData<float>
{
    public FloatStorageData(string key) : base(key)
    {
        this.datakey = key;
    }

    public FloatStorageData(string key, bool autoSave) : base(key, autoSave)
    {
        this.datakey = key;
        this.autoSave = autoSave;
    }

    public FloatStorageData(string key, bool autoSave,float defaultValue) : base(key, autoSave)
    {
        this.defaultValue = defaultValue;
    }

    public override void Init()
    {
        value = PlayerPrefsUtility.GetEncryptedFloat(datakey, defaultValue);
        base.Init();
    }

    public override void Save()
    {
        //DevLogs.LogData(string.Format("Dev : Save {0}, {1}", datakey, value));
        PlayerPrefsUtility.SetEncryptedFloat(datakey, value);
    }
}
