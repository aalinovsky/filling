﻿using UnityEngine;
using System.Collections;

public class IntStorageData : StorageData<int>
{
    public IntStorageData(string key, int defaultValue = 0, bool autoSave = true) : base(key, autoSave)
    {
        this.datakey = key;
        this.autoSave = autoSave;
        this.defaultValue = defaultValue;
    }

    public override void Init()
    {
        value = PlayerPrefsUtility.GetEncryptedInt(datakey, defaultValue);
        base.Init();
    }

    public override void Save()
    {
        //DevLogs.LogData(string.Format("Dev : Save {0}, {1}", datakey, value));
        PlayerPrefsUtility.SetEncryptedInt(datakey, value);
    }
}
