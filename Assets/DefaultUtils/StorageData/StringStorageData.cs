﻿using UnityEngine;
using System.Collections;

public class StringStorageData : StorageData<string>
{
    public StringStorageData(string key) : base(key)
    {

    }

    public StringStorageData(string key, bool autoSave) : base(key, autoSave)
    {

    }

    public StringStorageData(string key, bool autoSave, string defaultData) : base(key, autoSave)
    {
        defaultValue = defaultData;
    }

    public override void Init()
    {
        value = PlayerPrefsUtility.GetEncryptedString(datakey, defaultValue);
        base.Init();
    }

    public override void Save()
    {
        PlayerPrefsUtility.SetEncryptedString(datakey, value);
    }
}
