﻿using UnityEngine;
using System.Collections;

namespace DefaultUtility
{
    public static class StyledText
    {
        public static string SetColor(this string str, Color clr)
        {
            return string.Format("<color=#{1}>{0}</color>", str, ColorUtility.ToHtmlStringRGB(clr));
        }

        public static string SetSize(this string str, int size)
        {
            return string.Format("<size={1}>{0}</size>", str, size);
        }

        public static string Italic(this string str)
        {
            return string.Format("<i>{0}</i>", str);
        }

        public static string Bold(this string str)
        {
            return string.Format("<b>{0}</b>", str);
        }
    }
}
