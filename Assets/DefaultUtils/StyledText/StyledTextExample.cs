﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DefaultUtility
{
    public class StyledTextExample : MonoBehaviour
    {
        public string currentText;
        public Color clr;        
        public bool setSize;
        [ConditionalField("setSize", true)]
        public int size;
        public bool italic;
        public bool bold;

        [Button]
        public void ShowTextInConsole()
        {
            string res = currentText.SetColor(clr);

            res = setSize ? res.SetSize(size) : res;
            res = italic ? res.Italic() : res;
            res = bold ? res.Bold() : res;

            Debug.Log(res);
        }
    }
}