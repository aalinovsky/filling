﻿using UnityEngine;
using System.Collections;
using System;

public class Timer
{
    protected string timerName = "timerUniqueName";
    protected int delayInSeconds = 60;
    protected bool autoRegisterNextTrigger;
    protected bool authoStartTimer;
    protected DateTimeStorageData nextTimerTrigger;
    public DateTimeStorageData TriggerDate
    {
        get
        {
            return nextTimerTrigger;
        }
    }
    public event Action timerTriggerAction;
    public event Action<int> timerUpdateAction;
    
    public int Seconds
    {
        get
        {
            int seconds = (int)(DateTime.Now - nextTimerTrigger.Value).TotalSeconds;
            return seconds;
        }
    }

    protected bool isFirstLaunch = false;
    public bool IsFirstLaunch
    {
        get
        {
            return isFirstLaunch;
        }
    }

    public bool IsStarted
    {
        get
        {
            return TimerManager.Instance.IsStarted(this) && Seconds < delayInSeconds;
        }
    }

    public Timer(string timerName, int delayInSeconds, bool autoRegisterNextTrigger = true, bool authoStartTimer = false)
    {
        this.autoRegisterNextTrigger = autoRegisterNextTrigger;
        this.authoStartTimer = authoStartTimer;
        this.timerName = timerName;
        InitDate(delayInSeconds);
        InitEvents();
        if (authoStartTimer)
        {
            Start();
        }
    }

    public void Start()
    {
        TimerManager.Register(this);
    }

    public void Tick()
    {
        SecondsToTrigger();
    }

    int seconds;

    public int SecondsToTrigger()
    {
        seconds = Seconds;
        
        if (seconds <= delayInSeconds)
        {
            timerUpdateAction(seconds);
            if (seconds == delayInSeconds)
            {
                seconds = delayInSeconds;
                timerTriggerAction();
            }
        } 
        return seconds;
    }

    public void SetNextTriggerTime(DateTime date)
    {
        nextTimerTrigger.Value = date;
    }

    protected void InitDate(int delayInSeconds)
    {
        this.delayInSeconds = delayInSeconds;
        if (nextTimerTrigger == null)
        {
            nextTimerTrigger = new DateTimeStorageData(timerName, true);
            if (DateTime.Now.Year - nextTimerTrigger.Value.Year >= 2)
            {
                isFirstLaunch = true;
                if (autoRegisterNextTrigger)
                {
                    RegisterNextTriggerDate();
                }
                //else
                //{
                //    SetNextTriggerTime(DateTime.Now);
                //}
            }
        }
    }

    protected virtual void InitEvents()
    {
        timerTriggerAction += () => {
            if (autoRegisterNextTrigger)
            {
                RegisterNextTriggerDate();
            }
        };
        timerUpdateAction += (int i) => { };
    }

    protected void RegisterNextTriggerDate()
    {
        DateTime date = DateTime.Now;
        date.AddSeconds(delayInSeconds);
        SetNextTriggerTime(date);
    }
}
