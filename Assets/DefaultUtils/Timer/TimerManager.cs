﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class TimerManager : Singletone<TimerManager>
{
    protected List<Timer> timers = new List<Timer>();

    public static void Register(Timer t)
    {
        if(t != null)
            Instance.RegisterTimer(t);
    }

    public void RegisterTimer(Timer t)
    {
        timers.Add(t);
        if(timers.Count == 1)
        {
            StartTicker();
        }
    }

    public bool IsStarted(Timer t)
    {
        if (t == null)
            return false;
        if (timers.Contains(t))// && DateTime.Now > t.TriggerDate.Value)// DateTime.Compare(DateTime.Now, t.TriggerDate.Value) > 0)
        {
            return true;
        }        
        return false;
    }

    protected void StartTicker()
    {
        StartCoroutine(Ticker());
    }

    IEnumerator Ticker()
    {
        while(timers.Count > 0)
        {
            for (int i = 0; i < timers.Count; i++)
            {
                timers[i].Tick();
            }
            yield return new WaitForSecondsRealtime(1);
        }
    }
}
