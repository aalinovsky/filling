﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public static class UIEventsUtility
{
    public static void AddEvent(EventTrigger trigg, EventTriggerType type, System.Action<PointerEventData> action)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = type;
        entry.callback.AddListener((data) => { action((PointerEventData)data); });
        trigg.triggers.Add(entry);
    }
}
