﻿using UnityEngine;
using System.Collections;

public abstract class Splitter : ScriptableObject
{
    public int arrCount = 1;
    public float[] arr = new float[1];

    public Vector2 GetRange(int elementIndex)
    {
        float x = 0;
        float y = 0;
        if (elementIndex > arr.Length || arr.Length == 0)
        {
            return Vector2.zero;
        }

        if (elementIndex == arr.Length)
        {
            x = arr[arr.Length - 1];
            y = 1.0f;
        }
        else if (elementIndex == 0)
        {
            x = 0;
            y = arr[0];
        }
        else
        {
            x = arr[elementIndex - 1];
            y = arr[elementIndex];
        }

        return new Vector2(x, y);
    }

    public float GetLength(int elementIndex)
    {
        Vector2 range = GetRange(elementIndex);
        return range.y - range.x;
    }

    public int GetIndexOfChance(float f)
    {
        int resultId = 0;
        for (int i = 0; i < arr.Length + 1; i++)
        {
            Vector2 v = GetRange(i);
            if (v.x <= f && v.y >= f)
            {
                resultId = i;
                break;
            }
        }
        return resultId;
    }    

    public void UpdatePointValue(int index, float value)
    {
        if (!IndexOnRange(index))
            return;

        value = Mathf.Clamp(value, 0.0f, 1.0f);

        arr[index] = value;
        if (IndexOnRange(index - 1) && arr[index - 1] > value)
        {
            UpdatePointValue(index - 1, value);
        }
        if (IndexOnRange(index + 1) && arr[index + 1] < value)
        {
            UpdatePointValue(index + 1, value);
        }
    }

    public bool IndexOnRange(int index)
    {
        if (index < 0 || index >= arr.Length)
        {
            return false;
        }
        return true;
    }

    public abstract void UpdateSize(int newSize);
    
}