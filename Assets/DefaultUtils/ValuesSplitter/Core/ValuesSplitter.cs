﻿using UnityEngine;
using System.Collections;
using System;

public abstract class ValuesSplitter<T> : Splitter
{
    [SerializeField]
    public T[] labels = new T[1];
    
    public T GetValueOfChance(float f)
    {
        int i = GetIndexOfChance(f);
        return labels[i];
    }

    public T GetRandomValue()
    {
        return GetValueOfChance(UnityEngine.Random.Range(0.0f, 1.0f));
    }

    public string SplitterValues
    {
        get
        {
            string str = "";

            for (int i = 0; i < arr.Length; i++)
            {
                str += arr[i].ToString();
                str += i < arr.Length - 1 ? ":" : "";
            }

            return str;
        }

        set
        {
            string[] parsedStr = value.Split(':');
            if (parsedStr.Length > 0)
            {
                UpdateSize(parsedStr.Length);
                for (int i = 0; i < parsedStr.Length; i++)
                {
                    arr[i] = (float)Convert.ToDouble(parsedStr[i]);
                }
            }
        }
    }

    public override void UpdateSize(int newSize)
    {
        if (arr.Length == 0)
        {
            arr = new float[newSize];
            labels = new T[newSize+1];
            float step = 1.0f / (float)(newSize + 1);
            for (int i = 0; i < newSize; i++)
            {
                arr[i] = step *(i+1);
            }
        }
        else
        {
            float[] newArr = new float[newSize];
            T[] newlblArr = new T[newSize+1];
            int final = newSize > arr.Length ? arr.Length : newSize;
            newlblArr[0] = labels[0];
            for (int i = 0; i < final; i++)
            {
                newArr[i] = arr[i];
                newlblArr[i+1] = labels[i + 1];
            }
            if (newSize > arr.Length)
            {
                Vector2 size = arr.Length >= 1 ? GetRange(arr.Length) : Vector2.zero;

                float newX = size.x;
                float step = (1.0f - newX) / (newSize - arr.Length + 1);

                for (int i = arr.Length; i < newSize; i++)
                {
                    newArr[i] = newX + step * (i - arr.Length+1);
                }
            }
            arr = newArr;
            labels = newlblArr;
        }
    }
}