﻿#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using UnityEditor;

public class ValueSplitterWindow : EditorWindow
{
    public const float sliderWidth = 5.0f;
    Rect[] sliders;
    int selectedSlider = -1;

    Splitter values;
    public static Color[] colors = new Color[] {
        new Color(246 / 255.0f,81 / 255.0f,29 / 255.0f),
        new Color(255 / 255.0f,180 / 255.0f,0 / 255.0f),
        new Color(0 / 255.0f,166 / 255.0f,237 / 255.0f),
        new Color(127 / 255.0f,184.0f / 255.0f,0 / 255.0f),
        new Color(13 / 255.0f,44 / 255.0f,84 / 255.0f),
        Color.red,
        Color.blue,
        Color.black,
        Color.cyan
    };

    public static void Init(Splitter v)
    {        
        ValueSplitterWindow window = (ValueSplitterWindow)EditorWindow.GetWindow(typeof(ValueSplitterWindow));
        window.minSize = new Vector2(450, 100);
        window.values = v;
        window.wantsMouseMove = true;
        window.Show();
    }

    void OnGUI()
    {
        EditorGUI.BeginChangeCheck();

        GUILayout.Label("Base Settings", EditorStyles.boldLabel);

        float rectWidth = position.width - EditorGUIUtility.singleLineHeight;
        float rectHeight = EditorGUIUtility.singleLineHeight * 2;
        float xPos = EditorGUIUtility.singleLineHeight / 2.0f;
        float yPos = EditorGUIUtility.singleLineHeight * 2 + EditorGUIUtility.standardVerticalSpacing;

        Rect sliderRect = new Rect(xPos, yPos, rectWidth, rectHeight);
        EditorGUI.DrawRect(sliderRect, Color.green);

        float startPos = xPos;
        float prevSize = 0;

        sliders = new Rect[values.arr.Length];

        for (int i = 0; i < values.arr.Length + 1; i++)
        {
            Vector2 split = values.GetRange(i);
            float percents = split.y - split.x;
            prevSize = rectWidth * (percents);
            startPos = xPos + split.x * rectWidth;

            Rect r = new Rect(startPos, yPos, prevSize, rectHeight);
            Rect btnRect = new Rect(startPos - sliderWidth / 2.0f, yPos, sliderWidth, rectHeight);
            Color c = i >= colors.Length ? colors[i % colors.Length] : colors[i];
            EditorGUI.DrawRect(r, c);

            if (i > 0)
            {
                EditorGUI.DrawRect(btnRect, Color.gray);
                sliders[i - 1] = btnRect;
                EditorGUIUtility.AddCursorRect(btnRect, MouseCursor.SplitResizeLeftRight);
            }

            //string s = string.Format("{0:0.0} % \n{1}", percents * 100, values.labels[i]);
            //GUIStyle guiStyle = new GUIStyle();
            //guiStyle.normal.textColor = new Color(1,1,1);
            //EditorGUI.LabelField(new Rect(startPos + prevSize* 0.2f, yPos, prevSize, rectHeight), s, guiStyle);
        }

        if (Event.current.type == EventType.MouseDown)
        {
            Vector2 mouse = Event.current.mousePosition;
            for (int i = 0; i < sliders.Length; i++)
            {
                if (sliders[i].Contains(mouse))
                {
                    selectedSlider = i;
                    break;
                }
            }
        }
        else if (Event.current.type == EventType.MouseDrag)
        {
            Vector2 mouse = Event.current.mousePosition;
            if (selectedSlider >= 0)
            {
                float val = (mouse.x - xPos) / rectWidth;
                values.UpdatePointValue(selectedSlider, val);
            }
        }
        else if (Event.current.type == EventType.MouseUp)
        {
            EditorUtility.SetDirty(values);
            //EditorUtility.SetDirty(m_level);
            //EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());

            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            selectedSlider = -1;
        }

        for (int i =0;i< 3;i++)
        {
            GUILayout.Label("");
        }        
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("BlocksCount");
        values.arrCount = EditorGUILayout.IntField(values.arrCount);
        GUILayout.EndHorizontal();
        if (values.arr.Length != values.arrCount - 1)
        {
            if (values.arrCount - 1 >= 0)
            {
                values.UpdateSize(values.arrCount - 1);
            }
        }

        if (Event.current.isMouse) Event.current.Use();

        if (EditorGUI.EndChangeCheck())
        {
            EditorUtility.SetDirty(values);
            //EditorUtility.SetDirty(m_level);
            //EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());

            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
    }
}
#endif