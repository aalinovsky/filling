﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(ValuesColorSplitter))]
public class ValuesColorSplitterEditor : ValuesSplitterEditor<Color>
{
    public override void DrawEditButton(ValuesSplitter<Color> splitter)
    {
        if (GUILayout.Button("Edit"))
        {
            ValueSplitterWindow.Init(splitter);
        }
    }

    public override void DrawProperty(float posX, float posY, float w, float h)
    {    
        for (int i = 0; i < myTarget.labels.Length; i++)
        {
            myTarget.labels[i] = EditorGUI.ColorField(new Rect(posX, posY + (1 + i) * (EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing), w, h), myTarget.labels[i]);
        }
    }
}
