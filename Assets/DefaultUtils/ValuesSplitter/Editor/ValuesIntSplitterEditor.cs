﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ValueIntSplitter))]
public class ValuesIntSplitterEditor : ValuesSplitterEditor<int>
{
    public override void DrawEditButton(ValuesSplitter<int> splitter)
    {
        if (GUILayout.Button("Edit"))
        {
            ValueSplitterWindow.Init(splitter);
        }
    }

    public override void DrawProperty(float posX, float posY, float w, float h)
    {
        for (int i = 0; i < myTarget.labels.Length; i++)
        {
            myTarget.labels[i] = EditorGUI.IntField(new Rect(posX, posY + (1 + i) * (EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing), w, h), myTarget.labels[i]);
        }
    }
}
