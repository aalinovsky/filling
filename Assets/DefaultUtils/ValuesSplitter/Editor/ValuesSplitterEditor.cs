﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System;

//[CustomEditor(typeof(ValuesSplitter))]
public abstract class ValuesSplitterEditor<T> : Editor
{
    protected ValuesSplitter<T> myTarget;

    public override void OnInspectorGUI()
    {
        myTarget = (ValuesSplitter<T>)target;
       
        DrawEditButton(myTarget);

        EditorGUI.BeginChangeCheck();
        
        myTarget.arrCount = EditorGUILayout.IntField(myTarget.arrCount);
        

        Rect position = new Rect(0,
            (EditorGUIUtility.singleLineHeight  + EditorGUIUtility.standardVerticalSpacing) * 5 + EditorGUIUtility.standardVerticalSpacing,
            EditorGUIUtility.currentViewWidth,
            EditorGUIUtility.singleLineHeight);
        if (myTarget.arr.Length != myTarget.arrCount - 1)
        {
            if (myTarget.arrCount - 1 >= 0)
            {
                myTarget.UpdateSize(myTarget.arrCount - 1);
                //serializedObject.FindProperty("arr").arraySize = myTarget.arrCount;
            }
        }

        float rectWidth = position.width - EditorGUIUtility.singleLineHeight;
        float rectHeight = EditorGUIUtility.singleLineHeight;
        float xPos = EditorGUIUtility.singleLineHeight / 2.0f;
        float yPos = position.y; 


        Rect sliderRect = new Rect(xPos, yPos, rectWidth, rectHeight);

        EditorGUI.DrawRect(sliderRect, Color.green);


        float startPos = xPos;
        float prevSize = 0;


        for (int i = 0; i < myTarget.arr.Length + 1; i++)
        {
            Vector2 split = myTarget.GetRange(i);
            float percents = split.y - split.x;
            prevSize = rectWidth * (percents);
            startPos = xPos + split.x * rectWidth;

            Rect r = new Rect(startPos, yPos, prevSize, rectHeight);
            Color c = i >= ValueSplitterWindow.colors.Length ? ValueSplitterWindow.colors[i % ValueSplitterWindow.colors.Length] : ValueSplitterWindow.colors[i];
            EditorGUI.DrawRect(r, c);

            string s = string.Format("{0:0.0} %", percents * 100);
            GUIStyle guiStyle = new GUIStyle();
            guiStyle.normal.textColor = new Color(1, 1, 1);
            EditorGUI.LabelField(new Rect(startPos + prevSize * 0.2f, yPos, prevSize, rectHeight), s, guiStyle);
        }

        //for (int i = 0; i < myTarget.labels.Length; i++)
        //{
        //    myTarget.labels[i] = EditorGUI.TextField(new Rect(xPos, yPos + (1 + i) * (EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing), rectWidth, rectHeight), myTarget.labels[i]);
        //}
        DrawProperty(xPos, yPos, rectWidth, rectHeight);
        
        if (EditorGUI.EndChangeCheck())
        {
            EditorUtility.SetDirty(myTarget);            
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
    }

    public abstract void DrawProperty(float posX, float posY, float w, float h);

    public abstract void DrawEditButton(ValuesSplitter<T> s);
}