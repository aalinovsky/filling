﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ValuesStringSplitter))]
public class ValuesStringSplitterEditor : ValuesSplitterEditor<string>
{
    public override void DrawEditButton(ValuesSplitter<string> splitter)
    {
        if (GUILayout.Button("Edit"))
        {
            ValueSplitterWindow.Init(splitter);
        }
    }

    public override void DrawProperty(float posX, float posY, float w, float h)
    {
        for (int i = 0; i < myTarget.labels.Length; i++)
        {
            myTarget.labels[i] = EditorGUI.TextField(new Rect(posX, posY + (1 + i) * (EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing), w, h), myTarget.labels[i]);
        }
    }
}
