﻿using UnityEngine;
using System.Collections;

[System.Serializable]
[CreateAssetMenu(fileName = "splitter", menuName = "Splitter/ValuesIntSplitter", order = 51)]
public class ValueIntSplitter : ValuesSplitter<int>
{
}
