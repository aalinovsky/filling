﻿using UnityEngine;
using System.Collections;

[System.Serializable]
[CreateAssetMenu(fileName = "splitter", menuName = "Splitter/ValuesColorSplitter", order = 51)]
public class ValuesColorSplitter : ValuesSplitter<Color>
{
}
