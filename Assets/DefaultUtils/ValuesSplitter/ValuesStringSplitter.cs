﻿using UnityEngine;
using System.Collections;

[System.Serializable]
[CreateAssetMenu(fileName = "splitter", menuName = "Splitter/ValuesStringSplitter", order = 51)]
public class ValuesStringSplitter : ValuesSplitter<string>
{
}
