﻿using UnityEngine;

namespace DefaultUtility
{
    public static class VectorUtility
    {
        public static float GetRandom(this Vector2 vector)
        {
            return UnityEngine.Random.Range(vector.x, vector.y);
        }
    }
}