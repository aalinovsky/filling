﻿using UnityEngine;
using System.Collections;

public class AmbientSwitcher : MonoBehaviour
{
    protected Color prev;
    protected Color next;

    public void Prepare(Color prev, Color next)
    {
        this.prev = prev;
        this.next = next;
    }

    public void SetVisual(float val)
    {
        RenderSettings.ambientLight = Color.Lerp(prev, next, val);
    }
}
