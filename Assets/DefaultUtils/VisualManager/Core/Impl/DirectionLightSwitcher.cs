﻿using UnityEngine;
using System.Collections;

public class DirectionLightSwitcher : MonoBehaviour
{
    public Light dirLight;

    protected DirectionLightPreset prev;
    protected DirectionLightPreset next;

    public void Prepare(DirectionLightPreset prev, DirectionLightPreset next)
    {
        this.prev = prev;
        this.next = next;
    }

    public void SetVisual(float val)
    {
        dirLight.transform.rotation = Quaternion.Euler(Vector3.Lerp(prev.lightRotation, next.lightRotation, val));
        dirLight.intensity = Mathf.Lerp(prev.lightIntencity, next.lightIntencity, val);
        dirLight.color = Color.Lerp(prev.lightColor, next.lightColor, val);
    }
}

[System.Serializable]
public struct DirectionLightPreset
{
    public Vector3 lightRotation;
    public float lightIntencity;
    public Color lightColor;
}