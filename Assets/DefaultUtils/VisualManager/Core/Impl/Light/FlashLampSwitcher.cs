﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FlashLampSwitcher : Singletone<FlashLampSwitcher>
{
    protected TimeState prev;
    protected TimeState next;

    bool prevFlashState;
    bool currentFlashState;

    protected List<FlashLightController> lights = new List<FlashLightController>();

    public void AddLight(FlashLightController flash)
    {
        if (prevFlashState == currentFlashState)
        {
            //flash.SetIntensity(0);
            flash.SetState(currentFlashState);
        }
        lights.Add(flash);
    }

    public void RemoveLight(FlashLightController flash)
    {
        lights.Remove(flash);
    }

    public void Prepare(TimeState prev, TimeState next)
    {
        this.prev = prev;
        this.next = next;

        prevFlashState = IsEnableLight(prev);
        currentFlashState = IsEnableLight(next);
        if (!prevFlashState && currentFlashState)
        {
            SetActiveLight(true, 0);
        }
        else if (!prevFlashState && !currentFlashState)
        {
            SetActiveLight(false, 0);
        }
    }

    public void SetVisual(float val)
    {
        if (!prevFlashState && currentFlashState)
        {
            SetIntensityLight(val);
        }
        else if (prevFlashState && !currentFlashState)
        {
            SetIntensityLight(1.0f - val);
        }
    }

    protected void SetActiveLight(bool state, float value)
    {
        SetIntensityLight(value);
        for (int i = lights .Count-1; i >= 0; i--)
        {
            lights[i].SetState(state);
        }
    }

    protected void SetIntensityLight(float val)
    {
        for (int i = lights.Count - 1; i >= 0; i--)
        {
            lights[i].SetIntensity(val);
        }
    }

    protected bool IsEnableLight(TimeState time)
    {
        return time == TimeState.Night;
    }
}
