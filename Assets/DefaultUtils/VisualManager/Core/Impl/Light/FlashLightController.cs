﻿using UnityEngine;
using System.Collections;

public class FlashLightController : MonoBehaviour
{
    public Light[] flashlight;

    private void OnEnable()
    {
        if(FlashLampSwitcher.Instance != null)
            FlashLampSwitcher.Instance.AddLight(this);
    }

    private void OnDisable()
    {
        if (FlashLampSwitcher.Instance != null)
            FlashLampSwitcher.Instance.RemoveLight(this);
    }

    public void SetState(bool state)
    {
        for (int i = 0; i < flashlight.Length; i++)
        {
            flashlight[i].enabled = state;
        }
    }

    public void SetIntensity(float val)
    {
        for (int i = 0; i < flashlight.Length; i++)
        {
            flashlight[i].intensity = val*15;
        }        
    }
}
