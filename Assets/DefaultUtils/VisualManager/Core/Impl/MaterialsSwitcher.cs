﻿using UnityEngine;
using System.Collections;

public class MaterialsSwitcher : MonoBehaviour
{
    public Material material;

    protected MaterialPreset prev;
    protected MaterialPreset next;

    public void Prepare(MaterialPreset prev, MaterialPreset next)
    {
        this.prev = prev;
        this.next = next;
    }

    public void SetVisual(float val)
    {
        int colorsCount = prev.colors.Length > next.colors.Length ? next.colors.Length : prev.colors.Length;
        for (int i = 0; i < colorsCount; i++)
        {
            SetColor(next.colors[i].colorField, Color.Lerp(prev.colors[i].color, next.colors[i].color, val));
        }

        int floatCount = prev.floats.Length > next.floats.Length ? next.floats.Length : prev.floats.Length;
        for (int i = 0; i < floatCount; i++)
        {
            SetFloat(next.floats[i].floatField, Mathf.Lerp(prev.floats[i].value, next.floats[i].value, val));
        }
    }

    protected void SetColor(string field, Color newColor)
    {
        material.SetColor(field, newColor);
    }

    protected void SetFloat(string field, float val)
    {
        material.SetFloat(field, val);
    }

    public MaterialColor[] GetColorFields(string[] fields)
    {
        MaterialColor[] materialColors = new MaterialColor[fields.Length];
        for (int i = 0; i < fields.Length; i++)
        {
            materialColors[i] = new MaterialColor(fields[i], material.GetColor(fields[i]));
        }
        return materialColors;
    }

    public MaterialFloat[] GetFloatFields(string[] fields)
    {
        MaterialFloat[] materialFloats = new MaterialFloat[fields.Length];
        for (int i = 0; i < fields.Length; i++)
        {
            materialFloats[i] = new MaterialFloat(fields[i], material.GetFloat(fields[i]));
        }
        return materialFloats;
    }
}

[System.Serializable]
public struct MaterialPreset
{
    public MaterialColor[] colors;
    public MaterialFloat[] floats;
}

[System.Serializable]
public struct MaterialColor
{
    public string colorField;
    public Color color;

    public MaterialColor(string field, Color c)
    {
        colorField = field;
        color = c;
    }
}

[System.Serializable]
public struct MaterialFloat
{
    public string floatField;
    public float value;

    public MaterialFloat(string field, float val)
    {
        floatField = field;
        value = val;
    }
}