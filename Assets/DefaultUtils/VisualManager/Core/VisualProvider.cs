﻿using UnityEngine;
using System.Collections;

public abstract class VisualProvider : MonoBehaviour
{
    public Palette paletteForSave;

    protected Palette previousPalette;
    protected Palette currentPalette;

    public virtual void Prepare(Palette newPalette)
    {
        if (currentPalette == null)
        {
            previousPalette = currentPalette = newPalette;
        }
        else
        {
            previousPalette = currentPalette;
            currentPalette = newPalette;
        }
    }

    public abstract void SetVisual(float val);

    [EasyButtons.Button()]
    public abstract void SaveToPalette();
}
