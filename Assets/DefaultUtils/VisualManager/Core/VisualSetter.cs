﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DefaultUtility;

public class VisualSetter : Singletone<VisualSetter>
{
    public bool autoSetVisual = false;
    public SwitchMethod switchMethod = SwitchMethod.Linear;
    public bool instantSwith = false;
    [ConditionalHide("instantSwith", false, true)]
    public float switchDuration;
    [Space()]
    public VisualProvider provider;

    [SerializeField]
    protected Palette currentPalette;
    [Space()]
    public List<Palette> palettes = new List<Palette>();

    protected IntStorageData currentVisual;
    protected IEnumerator coroutine;

    private void Awake()
    {
        currentVisual = new IntStorageData("CurrentVisual");
    }

    private void Start()
    {
        if (autoSetVisual)
        {
            currentVisual.Value = -1;
            NextVisual();
        }
    }

    public static void Next()
    {
        Instance.NextVisual();
    }

    [Button()]
    public void NextVisual()
    {
        switch (switchMethod)
        {
            case SwitchMethod.Linear:
                currentVisual.Value += 1;

                if (currentVisual.Value >= palettes.Count)
                {
                    currentVisual.Value = currentVisual.Value % palettes.Count;
                }
                UpdateVisual(currentVisual.Value);
                break;
            case SwitchMethod.Random:
                currentVisual.Value = Random.Range(0, palettes.Count);
                UpdateVisual(currentVisual.Value);
                break;
        }  
        if(autoSetVisual && !instantSwith)
        {
            this.WaitAndStart(switchDuration + 0.1f, NextVisual);
        }
    }

    [Button()]
    public void UpdateCurrentPalette()
    {
        if(currentPalette != null && provider != null)
        {
            provider.paletteForSave = currentPalette;
            provider.SaveToPalette();
        }
    }

    public void UpdateVisual(Palette p)
    {
        UpdateVisual(palettes.IndexOf(p));
    }

    public void UpdateVisual(int i)
    {
        PrepareVisual(i);
        if (instantSwith)
        {
            SetVisual(1);
        }
        else
        {
            if(coroutine != null)
            {
                StopCoroutine(coroutine);
            }
            coroutine = SwithcVisual();
            StartCoroutine(coroutine);
        }
    }

    public void PrepareVisual(int index)
    {
        if (index >= 0 && index < palettes.Count)
        {
            currentPalette = palettes[index];
            if (provider != null)
                provider.Prepare(palettes[index]);
        }
    }

    protected void SetVisual(float process)
    {
        if (provider != null)
            provider.SetVisual(process);
    }

    IEnumerator SwithcVisual()
    {
        float dlt = 0;
        float t = 0;
        while (dlt < 1)
        {   
            dlt = Mathf.Min(t / switchDuration, 1);
            SetVisual(dlt);
            t += Time.deltaTime;
            yield return null;
        }
        coroutine = null;
    }
}

public enum SwitchMethod
{
    Linear,
    Random,
    None
}