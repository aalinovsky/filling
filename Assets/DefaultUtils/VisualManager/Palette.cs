﻿using UnityEngine;
using System.Collections;

[System.Serializable]
[CreateAssetMenu(fileName = "palette", menuName = "Visual/Palette")]
public class Palette : ScriptableObject
{
    public TimeState time;
    public Weather weather;
    [Header("Direction light")]
    //[ColorUsage(true, true)]
    [SerializeField]
    public Color ambient;
    [SerializeField]
    public DirectionLightPreset light;
    [SerializeField]
    public MaterialPreset skybox;
    [SerializeField]
    public MaterialPreset roadSteam;
    [SerializeField]
    public MaterialPreset carsFlashlights;
    [SerializeField]
    public MaterialPreset lampFlashlight;
}

public enum TimeState
{
    Dawning,
    Morning,
    Day,
    Sunset,
    Evening,
    Night
}

public enum Weather
{
    None,
    Rain
}
