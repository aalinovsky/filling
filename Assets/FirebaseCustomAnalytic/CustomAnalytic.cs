﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class CustomAnalytic
{
    public static string CreateRequest(CustomAnalyticSettings settings, Dictionary<string, string> paramaters, string eventName = "sendEvent")
    {
        return CreateRequest(settings.hostName, GetDefaultParameters(settings).Combine(paramaters) , eventName);
    }

    public static string CreateRequest(string host, Dictionary<string, string> paramaters, string eventName)
    {
        return string.Format("{0}/{1}?{2}", host, eventName, CreateGetRequest(paramaters));
    }

    public static string CreateGetRequest(Dictionary<string, string> paramaters)
    {
        string resultRequest = "";

        foreach (var item in paramaters)
        {
            resultRequest += string.Format("{0}={1}&", item.Key, item.Value);
        }
        return resultRequest;
    }

    public static Dictionary<string, string> GetDefaultParameters(CustomAnalyticSettings settings)
    {
        Dictionary<string, string> paramaters = new Dictionary<string, string>();

        paramaters.Add("appName", settings.appName);
        paramaters.Add("os", settings.osName);
        paramaters.Add("appVersion", settings.appVersion);

        return paramaters;
    }

    public static Dictionary<string, string> Combine(this Dictionary<string, string> paramaters1, Dictionary<string, string> paramaters2)
    {
        Dictionary<string, string> paramaters = paramaters1.Concat(paramaters2)
        .ToLookup(x => x.Key, x => x.Value)
        .ToDictionary(x => x.Key, g => g.First());
        return paramaters;
    }
}

public struct CustomAnalyticSettings
{
    public string hostName;
    public string osName;
    public string appVersion;
    public string appName;
}
