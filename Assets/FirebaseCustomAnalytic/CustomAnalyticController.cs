﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

public class CustomAnalyticController : Singletone<CustomAnalyticController>
{
    protected CustomAnalyticSettings Settings;
    protected static bool isInited = false;

    public CustomSettings analyticSettings;
    public string testKey = "TestLevelFromUnity";
    public CustomLevelEvent testEvent;

    [DefaultUtility.Button(DefaultUtility.ButtonMode.EnabledInPlayMode)]
    public void TestSendRequest()
    {
        SendLevelEvent(testKey, testEvent);
    }

    private void Awake()
    {
        Init();
    }

    protected void Init()
    {
        if (isInited)
            return;

        if (analyticSettings != null)
            Settings = analyticSettings.Settings;
        isInited = true;
    }

    public static void LevelEvent(string levelKey, CustomLevelEvent levelEvent)
    {
        Instance.SendLevelEvent(levelKey, levelEvent);
    }

    public void SendLevelEvent(string levelKey, CustomLevelEvent levelEvent)
    {
        if (!isInited)
            return;

        string getRequest = CustomAnalytic.CreateRequest(Settings, new Dictionary<string, string>() {
            { "level", levelKey },
            { "state", levelEvent.ToString().ToLower() } });
        DevLogs.Log(getRequest);
        StartCoroutine(SendGetRequest(getRequest));
    }

    IEnumerator SendGetRequest(string request)
    {
        UnityWebRequest uwr = UnityWebRequest.Get(request);
        yield return uwr.SendWebRequest();

        if (uwr.isNetworkError)
        {
            Debug.Log("Error While Sending: " + uwr.error);
        }
        else
        {
            Debug.Log("Received: " + uwr.downloadHandler.text);
        }
    }
}

public enum CustomLevelEvent
{
    Start,
    Complete,
    Fail
}
