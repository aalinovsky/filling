﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "customAnalyticSettings", menuName = "CustomAnalytic/Settings")]
public class CustomSettings : ScriptableObject
{
    public string hostName;
    public string appName;

    public CustomAnalyticSettings Settings
    {
        get
        {
            CustomAnalyticSettings settings = new CustomAnalyticSettings();

            settings.hostName = hostName;
            settings.appName = appName;
#if UNITY_EDITOR
            settings.osName = "Desktop";
#elif UNITY_ANDROID
            settings.osName = "Android";
#elif UNITY_IOS
            settings.osName = "IOS";
#endif
            settings.appVersion = Application.version;

            return settings;
        }
    }
}
