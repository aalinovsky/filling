﻿using UnityEngine;
using System.Collections;

public abstract class AbstractLocalizedTextController : MonoBehaviour
{
    public LocalizedFrasesDatabase database;
    public string key;
    protected LocalizedFrase frase;

    void Awake()
    {
        InitController();
        SetLanguage();
    }

    protected void InitController()
    {
        if (database != null)
        {
            frase = database.GetFrase(key);
            database.AddSetLanguageEvent(SetLanguage);
        }
    }

    protected abstract void SetText(string newText);

    public void SetLanguage()
    {
        if (frase != null)
        {
            SetText(frase.Value);
        }
    }
}