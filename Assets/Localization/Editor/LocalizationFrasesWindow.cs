﻿#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using UnityEditor;

public class LocalizationFrasesWindow 
{
    protected static float horizontalSpace = 8.5f;
    protected static float keyWidth = 140;
    protected static float deleteButtonWidth = 40;
    protected static float selectButtonWidth = 100;
    protected static float languageWidth
    {
        get
        {
            return (position.width - keyWidth - deleteButtonWidth - selectButtonWidth);
        }
    }

    protected static GUILayoutOption GetLanguageoptions(int count)
    {        
        return GUILayout.Width((languageWidth - horizontalSpace *(3+ count)) / count);
    }
    protected static LocalizedFrase fraseForRemove;
    protected static Rect position { get; set; }
    
    public static void DrawFrasesList(LocalizedFrasesDatabase db, ref Vector2 scroll, Rect size)
    {
        position = size;
        GUILayout.Space(30);

        //Rect rect = EditorGUILayout.BeginVertical();
        
        DeleteFrase(db);
        DrawHeader(db, db.ShowsLanguages);
        
        EditorGUILayout.BeginHorizontal();
        {
            scroll = EditorGUILayout.BeginScrollView(scroll);

            for (int i = 0; i < db.frases.Count; i++)
            {
                if(db.frases[i] != null)
                    DrawFrase(db.frases[i], db.ShowsLanguages);
            }

            EditorGUILayout.EndScrollView();
        }
        EditorGUILayout.EndHorizontal();

        //EditorGUILayout.EndVertical();

        //GUI.Box(rect, GUIContent.none);
    }

    protected static void DrawHeader(LocalizedFrasesDatabase db, SystemLanguage[] langs)
    {
        EditorGUILayout.BeginHorizontal();
        {            
            EditorGUILayout.LabelField("Keys", GUILayout.Width(keyWidth));

            for (int i = 0; i < langs.Length; i++)
            {
                EditorGUILayout.LabelField(langs[i].ToString(), GetLanguageoptions(langs.Length));
            }

        }
        EditorGUILayout.EndHorizontal();
    }

    protected static void DeleteFrase(LocalizedFrasesDatabase db)
    {
        if(fraseForRemove != null)
        {
            //if (EditorUtility.DisplayDialog("Delete frase?",
            //    "Are you sure you want to delete " + fraseForRemove.fraseKey
            //    + " from database?", "Yes", "No"))
            {
                db.frases.Remove(fraseForRemove);
                fraseForRemove = null;
                EditorUtility.SetDirty(db);
            }
        }
    }
        
    protected static void DrawFrase(LocalizedFrase frase, SystemLanguage[] langs)
    {
        EditorGUILayout.BeginHorizontal();
        {
            EditorGUI.BeginChangeCheck();

            frase.fraseKey = EditorGUILayout.TextArea(frase.fraseKey, GUILayout.Width(keyWidth));

            for (int i = 0; i < langs.Length; i++)
            {
                if (langs[i].Equals(SystemLanguage.English))
                {
                    frase.defaultStr = EditorGUILayout.TextArea(frase.defaultStr, GetLanguageoptions(langs.Length));
                }
                else
                {
                    if (!frase.HasLanguage(langs[i]))
                    {
                        frase.AddLanguage(langs[i]);
                    }

                    int index = frase.GetLanguageId(langs[i]);
                    //if(index < 0)
                    //{
                    //    EditorGUILayout.LabelField("", GUILayout.Width(languageWidth / langs.Length - horizontalSpace * langs.Length));
                    //}
                    //else
                    {
                        string s = EditorGUILayout.TextArea(frase.lines[index].val, GetLanguageoptions(langs.Length));
                        if(s != frase.lines[index].val)
                        {
                            frase.lines[index] = new StringValue() {val = s, font = frase.lines[index].font, language = frase.lines[index].language };
                        }
                    }
                }
            }

            if (GUILayout.Button("X", GUILayout.Width(deleteButtonWidth)))
            {
                fraseForRemove = frase;
            }
            if (GUILayout.Button("Select", GUILayout.Width(selectButtonWidth)))
            {
                UnityEditor.Selection.activeObject = frase;
            }
            if (EditorGUI.EndChangeCheck())
            {
                SaveChanges(frase);
            }
        }
        EditorGUILayout.EndHorizontal();
    }

    protected static void SaveChanges(LocalizedFrase frase)
    {
        EditorUtility.SetDirty(frase);
    }
}
#endif