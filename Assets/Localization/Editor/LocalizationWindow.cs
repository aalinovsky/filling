﻿using UnityEngine;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
using System.Collections.Generic;

public class LocalizationWindow : EditorWindow
{
    protected LocalizedFrasesDatabase database;
    protected TextAsset asset;
    protected string assetName = "localizations";
    protected ExportFormat format;

    protected float exportCurrentProgress = 0;
    protected float exportProgress = 0;
    protected Vector2 frasesListScroll = Vector2.zero;

    protected LocalizedFrase fraseForAdd;
    protected string newFraseKey;

    [MenuItem("Localization/Import|export")]
    public static LocalizationWindow ShowWindow()
    {
        return (LocalizationWindow)EditorWindow.GetWindow(typeof(LocalizationWindow));
    }

    public void SetDatabase(LocalizedFrasesDatabase newDB)
    {
        database = newDB;
    }

    public GUILayoutOption HalfSize
    {
        get
        {
            return GUILayout.Width(position.width / 2.0f - EditorGUIUtility.standardVerticalSpacing * 2);
        }
    }

    public GUILayoutOption LangsWidth
    {
        get
        {
            return GUILayout.Width(position.width / database.langsForExport.Count - EditorGUIUtility.standardVerticalSpacing * database.langsForExport.Count - 30);
        }
    }

    void OnGUI()
    {
        EditorGUILayout.BeginHorizontal();
        {
            EditorGUILayout.HelpBox("Editor for import / export localizations", MessageType.None);
        }
        EditorGUILayout.EndHorizontal();
        DrawDatabase();
        if (database == null)
            return;
       
        EditorGUI.BeginChangeCheck();
        {
            DrawAssetFileField();

            DrawAddFrase();

            LocalizationFrasesWindow.DrawFrasesList(database, ref frasesListScroll, position);
        }
        if (EditorGUI.EndChangeCheck())
        {
            SaveChanges(database);
        }
        ShowProgressExport();
    }

    protected void DrawDatabase()
    {
        EditorGUILayout.BeginHorizontal();
        {
            database = (LocalizedFrasesDatabase)EditorGUILayout.ObjectField(database, typeof(LocalizedFrasesDatabase), true);
        }
        EditorGUILayout.EndHorizontal();

        if (database == null)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.HelpBox("Insert the Database file", MessageType.Warning);
            EditorGUILayout.EndHorizontal();
        }
        
    }

    protected void DrawAssetFileField()
    {
        EditorGUILayout.BeginHorizontal();
        {
            format = (ExportFormat)EditorGUILayout.EnumPopup(format, HalfSize);
            asset = (TextAsset)EditorGUILayout.ObjectField(asset, typeof(TextAsset), true, HalfSize);
        }
        EditorGUILayout.EndHorizontal();

        if (asset == null)
        {
            EditorGUILayout.BeginHorizontal();
            {
                EditorGUILayout.HelpBox("Create or select export file", MessageType.Warning);
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();
            {
                if (GUILayout.Button("Create file for export", HalfSize))
                {
                    CreateFile(database, assetName, format);
                }
                assetName = EditorGUILayout.TextArea(assetName, HalfSize);
            }
            EditorGUILayout.EndHorizontal();
        }

        if (asset != null)
        {      
            DrawImportExportButtons();
            DrawExportLanguages(database);
        }
    }

    protected void DrawAddFrase()
    {        
        EditorGUILayout.BeginHorizontal();
        {
            EditorGUILayout.HelpBox("Select frase asset or write key", MessageType.None);
        }
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        {
            newFraseKey = EditorGUILayout.TextField(newFraseKey);
            fraseForAdd = (LocalizedFrase)EditorGUILayout.ObjectField(fraseForAdd, typeof(LocalizedFrase), true);
        }
        EditorGUILayout.EndHorizontal();

        if(fraseForAdd != null)
        {
            database.AddFrase(fraseForAdd);
            fraseForAdd = null;
        }
        
        if (GUILayout.Button("Add frase"))
        {
            database.AddFrase(newFraseKey);
        }
    }

    protected void DrawImportExportButtons()
    {
        EditorGUILayout.BeginHorizontal();
        {
            if (GUILayout.Button("Export", HalfSize))
            {
                ExportToCSV(database, asset);
                EditorUtility.DisplayDialog("Localizations",
                string.Format("Export is finish"), "Ok");
            }

            if (GUILayout.Button("import", HalfSize))
            {
                ImportFromCSV(database, asset);
                EditorUtility.DisplayDialog("Localizations",
                string.Format("import is finish"), "Ok");
            }
        }
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.Space();
        EditorGUILayout.Space();
    }
    
    protected bool languagesFoldout = true;
    protected void DrawExportLanguages(LocalizedFrasesDatabase db)
    {
        languagesFoldout = EditorGUILayout.Foldout(languagesFoldout, "Languages settings");

        if (languagesFoldout)
        {
            EditorGUILayout.BeginHorizontal();
            {
                if (GUILayout.Button("Add language"))
                {
                    db.AddLanguage(SystemLanguage.Russian);
                }
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();
            {
                for (int i = 0; i < db.langsForExport.Count; i++)
                {
                    db.langsForExport[i] = (SystemLanguage)EditorGUILayout.EnumPopup(db.langsForExport[i], LangsWidth);
                    if (GUILayout.Button("X", GUILayout.Width(30)))
                    {
                        db.RemoveLanguage(db.langsForExport[i]);
                    }
                }
            }
            EditorGUILayout.EndHorizontal();

            if (db.langsForExport.Count > 0)
            {
                EditorGUILayout.BeginHorizontal();
                {
                    EditorGUILayout.HelpBox("Select languages for edit them", MessageType.None);
                }
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                {
                    for (int i = 0; i < db.langsForExport.Count; i++)
                    {
                        if (GUILayout.Button(db.langsForExport[i].ToString(), LangsWidth))
                        {
                            db.showLangs[i] = !db.showLangs[i];
                        }
                        EditorGUILayout.Toggle(db.showLangs[i], GUILayout.Width(30));
                    }
                }
                EditorGUILayout.EndHorizontal();
            }

            EditorGUILayout.Space();
            EditorGUILayout.Space();
        }
    }

    protected void CreateFile(LocalizedFrasesDatabase db, string name, ExportFormat format)
    {
        string path = AssetsUtility.GetAssetFolder(db);
        string relativePath = AssetsUtility.GetRelativePath(db);
        switch (format)
        {
            case ExportFormat.CSV:
                //if (!createInProcess)
                {
                    asset = CreateCSV(name, path, relativePath);
                    //DevLogs.Log(asset.name);
                }
                break;
        }
    }

    protected TextAsset CreateCSV(string name, string path, string relativePath)
    {
        string fullPath = string.Format("{0}{1}.csv", path, name);
        string relPath = string.Format("{0}{1}.csv", relativePath, name);
        Debug.LogFormat("Create file at {0}", fullPath);
        Debug.LogFormat("Create file at {0}", relPath);

        FileStream file = File.Open(fullPath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
        file.Close();
        AssetDatabase.Refresh(ImportAssetOptions.Default);
        return (TextAsset)AssetDatabase.LoadAssetAtPath(relPath, typeof(TextAsset));
    }

    protected void ExportToCSV(LocalizedFrasesDatabase db, TextAsset exportFile)
    {
        //StringBuilder sb = new StringBuilder();

        string result = "";

        List<string[]> data = db.ExportData();
        exportProgress = exportCurrentProgress = data.Count;

        for (int i = 0; i < data.Count; i++)
        {
            //DevLogs.LogFormat("{0}",string.Join(",", data[i]));
            exportCurrentProgress -= 1;
        }
        //Regex rgx = new Regex("[^A-Za-z0-9 _]");
        for (int index = 0; index < data.Count; index++)
        {
            result += string.Join(",", data[index]) +(index == data.Count - 1? "": "\n");
        }
        //sb.AppendLine(rgx.Replace(string.Join(",", data[index]), ""));


        string filePath = AssetsUtility.GetFullPath(exportFile);

        StreamWriter outStream = System.IO.File.CreateText(filePath);
        outStream.Write(result);
        outStream.Close();

        EditorUtility.SetDirty(asset);
        AssetDatabase.Refresh(ImportAssetOptions.Default);
        exportCurrentProgress = 0;
    }

    protected void ImportFromCSV(LocalizedFrasesDatabase db, TextAsset exportFile)
    {
        List<string[]> data = new List<string[]>();

        string[] lines = exportFile.text.Split('\n');
        //Regex rgx = new Regex("[^a-zA-Z0-9 -]");
        for (int i = 0; i < lines.Length; i++)
        {
            if (!string.IsNullOrEmpty(lines[i]))
            {
                string[] arr = SplitCsvLine(lines[i]);//lines[i].Split(',');
                if (arr.Length > 0 && !string.IsNullOrEmpty(arr[0]))
                {
                    data.Add(arr);
                }
            }
        }
        
        db.ImportData(data);
    }

    static public string[] SplitCsvLine(string line)
    {
        List<string> separatedLines = line.Split(',').ToList();
        List<string> separatedLinesResult = new List<string>();

        bool findStartLine = false;
        string strForAdd = "";
        for (int i = 0; i < separatedLines.Count; i++)
        {
            if (findStartLine)
            {                
                if (separatedLines[i].Contains('"'))
                {
                    strForAdd += "," + separatedLines[i].Remove(separatedLines[i].IndexOf('"'));
                    separatedLinesResult.Add(strForAdd);
                    findStartLine = false;                    
                }
                else
                {
                    strForAdd += "," + separatedLines[i];
                }
            }
            else
            {
                if (separatedLines[i].Contains('"'))
                {
                    strForAdd = "";
                    strForAdd += separatedLines[i].Substring(1);
                    findStartLine = true;
                }
                else
                {
                    separatedLinesResult.Add(separatedLines[i]);
                }
            }
        }
        return separatedLinesResult.ToArray();
    }

    protected void SaveChanges(LocalizedFrasesDatabase db)
    {
        EditorUtility.SetDirty(db);
    }

    protected void ShowProgressExport()
    {
        if (exportCurrentProgress > 0 && exportProgress != 0)
        {
            EditorUtility.DisplayProgressBar("Localization export", "Export in progress " + exportCurrentProgress +" "+ exportProgress, 1.0f - exportCurrentProgress / exportProgress);
            //exportCurrentProgress -= Time.unscaledDeltaTime;
            if (exportCurrentProgress <= 0)
            {
                EditorUtility.ClearProgressBar();
            }
        }
    }

    void OnInspectorUpdate()
    {
        Repaint();
    }
}

public enum ExportFormat
{
    CSV
}
#endif
