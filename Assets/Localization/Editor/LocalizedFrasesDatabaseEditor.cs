﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEditorInternal;

[CanEditMultipleObjects]
[CustomEditor(typeof(LocalizedFrasesDatabase))]
public class LocalizedFrasesDatabaseEditor : Editor
{
    private ReorderableList list;

    private void OnEnable()
    {
        list = new ReorderableList(serializedObject,
                serializedObject.FindProperty("frases"),
                true, true, true, true);

        list.onCanRemoveCallback = (ReorderableList l) => {
            return l.count > 1;
        };

        list.onRemoveCallback = (ReorderableList l) => {           
                ReorderableList.defaultBehaviours.DoRemoveButton(l);
        };

        list.drawElementCallback =
            (Rect rect, int index, bool isActive, bool isFocused) => {
               var element = list.serializedProperty.GetArrayElementAtIndex(index);

                rect.y += 2;
                EditorGUI.PropertyField(
                    new Rect(rect.x, rect.y, 200, EditorGUIUtility.singleLineHeight),
                    element, GUIContent.none);

                if (element.objectReferenceValue == null)
                {
                    return;
                }
                EditorGUI.PropertyField(
                    new Rect(rect.x + 210, rect.y, 200, EditorGUIUtility.singleLineHeight),
                    new SerializedObject(element.objectReferenceValue).FindProperty("fraseKey"), GUIContent.none);
                EditorGUI.PropertyField(
                    new Rect(rect.x + 420, rect.y, 20, EditorGUIUtility.singleLineHeight),
                    new SerializedObject(element.objectReferenceValue).FindProperty("caps"), GUIContent.none);
            };
    }

    public override void OnInspectorGUI()
    {
        if (GUILayout.Button("Edit"))
        {
            LocalizationWindow.ShowWindow().SetDatabase((LocalizedFrasesDatabase)target);
        }
        serializedObject.Update();
        list.DoLayoutList();
        serializedObject.ApplyModifiedProperties();

        DrawDefaultInspector();
    }
}
