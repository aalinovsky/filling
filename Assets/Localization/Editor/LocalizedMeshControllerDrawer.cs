﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using TMPro;

[CustomEditor(typeof(LocalizedMeshController))]
public class LocalizedMeshControllerDrawer : Editor
{
    private int _choiceIndex = 0;
    string[] keys;

    public override void OnInspectorGUI()
    {
        LocalizedMeshController locText = (LocalizedMeshController)target;

        EditorGUI.BeginChangeCheck();
        locText.txt = (TextMeshPro)EditorGUILayout.ObjectField(locText.txt, typeof(TextMeshPro), true);
        locText.database = (LocalizedFrasesDatabase)EditorGUILayout.ObjectField(locText.database, typeof(LocalizedFrasesDatabase), true);

        if (locText.database != null && locText.database.frases.Count > 0)
        {
            if (keys == null)
            {
                keys = locText.database.GetKeys(true);
                for (int i = 0; i < keys.Length; i++)
                {
                    if (keys[i].Equals(locText.key))
                    {
                        _choiceIndex = i;
                        break;
                    }
                }
            }
            _choiceIndex = EditorGUILayout.Popup(_choiceIndex, keys);
            locText.key = keys[_choiceIndex];

            LocalizedFrase loc = locText.database.GetFrase(locText.key);
            if (loc != null)
            {
                EditorGUILayout.LabelField(loc.Value);
                if (locText.txt != null && GUILayout.Button("Confirm"))
                {
                    locText.txt.text = loc.Value;
                }
            }


        }

        if (EditorGUI.EndChangeCheck())
        {
            EditorUtility.SetDirty(locText);
        }
    }
}

