﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomPropertyDrawer(typeof(StringValue))]
public class StringValueDrawer : PropertyDrawer
{
    private static int fieldsCount = 3;
    // Draw the property inside the given rect
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        // Using BeginProperty / EndProperty on the parent property means that
        // prefab override logic works on the entire property.
        EditorGUI.BeginProperty(position, label, property);
        // Draw label
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
        // Don't make child fields be indented
        int indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;
        // Calculate rects

        Rect valueText = new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight);
        Rect langRect = new Rect(position.x, position.y + 2 * EditorGUIUtility.singleLineHeight, position.width, EditorGUIUtility.singleLineHeight);
        Rect fontRect = new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight, position.width, EditorGUIUtility.singleLineHeight);
        // Draw fields - passs GUIContent.none to each so they are drawn without labels

        EditorGUI.PropertyField(valueText, property.FindPropertyRelative("val"), GUIContent.none);
        EditorGUI.PropertyField(langRect, property.FindPropertyRelative("language"), GUIContent.none);
        EditorGUI.PropertyField(fontRect, property.FindPropertyRelative("font"), GUIContent.none);
        // Set indent back to what it was
        EditorGUI.indentLevel = indent;
        EditorGUI.EndProperty();
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        float extraHeight = (fieldsCount - 1) * EditorGUIUtility.singleLineHeight;
        return base.GetPropertyHeight(property, label) + extraHeight;
    }
}
#endif