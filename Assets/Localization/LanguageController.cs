﻿using UnityEngine;
using System.Collections;
using DefaultUtility;

public class LanguageController : Singletone<LanguageController>
{
    public LocalizedFrasesDatabase database;

#if UNITY_EDITOR
    public SystemLanguage testLang;

    [Button()]
    public void SetToTestlanguage()
    {
        Setlanguage(testLang);
    }
#endif

    public static SystemLanguage Language
    {
        get
        {
            if (SavedLanguage < 0)
            {
                return Application.systemLanguage;
            }
            else
            {
                return (SystemLanguage)SavedLanguage;
            }
        }
    }

    protected static int SavedLanguage
    {
        get
        {
            return PlayerPrefsUtility.GetEncryptedInt("SavedLanguage", -1);
        }

        set
        {
            PlayerPrefsUtility.SetEncryptedInt("SavedLanguage", value);
        }
    }

    public static void Setlanguage(SystemLanguage lang)
    {
        SavedLanguage = (int)lang;
        Instance.database.Setlanguage();
    }
}
