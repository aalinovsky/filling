﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "LocalizedFrase", menuName = "Localization/Frase", order = 1)]
[System.Serializable]
public class LocalizedFrase : ScriptableObject
{
    //[Header("Unique key for current frase")]
    public string fraseKey = "empty";
    public bool caps = false;
    [Header("English frase")]
    public string defaultStr;
    public Font defaultFont;
    public List<StringValue> lines = new List<StringValue>();

    public string Value
    {
        get
        {
            SystemLanguage l = LanguageController.Language;
            return GetValueByLang(l);
        }
    }

    public string GetValue(params object[] obj)
    {
        return string.Format(Value, obj);
    }

    public int GetLanguageId(SystemLanguage l)
    {
        int index = -1;

        for (int i = 0; i < lines.Count; i++)
        {
            if (lines[i].language.Equals(l))
            {
                index = i;
                break;
            }
        }

        return index;
    }

    public bool HasLanguage(SystemLanguage l)
    {
        if (l.Equals(SystemLanguage.English))
        {
            return true;
        }

        for (int i = 0; i < lines.Count; i++)
        {
            if (lines[i].language == l)
                return true;
        }

        return false;
    }

    public void AddLanguage(SystemLanguage l, string val = "")
    {
        if (!HasLanguage(l))
        {
            StringValue s = new StringValue();
            s.language = l;
            s.val = val;
            lines.Add(s);
        }
    }

    public string GetValueByLang(SystemLanguage l)
    {
        if (l == SystemLanguage.English)
        {
            return GetValue(defaultStr);
        }

        foreach (StringValue s in lines)
        {
            if (s.language == l)
            {
                return GetValue(s.val);
            }
        }

        return GetValue(defaultStr);
    }

    public void SetLangValue(SystemLanguage l, string newValue)
    {
        if (!HasLanguage(l))
        {
            AddLanguage(l, newValue);
            return;
        }

        if (l == SystemLanguage.English)
        {
            defaultStr = newValue;
            return;
        }

        StringValue newStr = new StringValue();
        newStr.language = l;
        newStr.val = newValue;

        for (int i = 0; i < lines.Count; i++)
        {
            if (lines[i].language == l)
            {
                newStr.font = lines[i].font;
                lines[i] = newStr;
                return;
            }
        }

        lines.Add(newStr);
    }

    protected string GetValue(string s)
    {
        return caps ? s.ToUpper() : s;
    }

    public Font Font
    {
        get
        {
            SystemLanguage l = LanguageController.Language;
            if (l == SystemLanguage.Russian)
            {
                return defaultFont;
            }

            foreach (StringValue s in lines)
            {
                if (s.language == l)
                {
                    return s.font;
                }
            }

            return defaultFont;
        }
    }
}