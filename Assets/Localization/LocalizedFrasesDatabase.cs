﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;
using System.Linq;
using System.IO;
#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu(fileName = "LocalizedFrasesDatabase", menuName = "Localization/Database", order = 1)]
[System.Serializable]
public class LocalizedFrasesDatabase : ScriptableObject
{
    public List<SystemLanguage> langsForExport = new List<SystemLanguage>();
    public List<bool> showLangs = new List<bool>();

    [SerializeField]
    public List<LocalizedFrase> frases = new List<LocalizedFrase>();

    protected string[] fraseKeys;
    protected event Action setLanguage;
    
    public void AddSetLanguageEvent(Action act)
    {
        setLanguage += act;
    }

    public void Setlanguage()
    {
        setLanguage();
    }

    public LocalizedFrase GetFrase(string key)
    {
        foreach (LocalizedFrase fr in frases)
        {
            if (fr.fraseKey.Equals(key))
            {
                return fr;
            }
        }
        return null;
    }
#if UNITY_EDITOR
    public string[] GetKeys(bool forceUpdate = false)
    {
        if(fraseKeys == null || fraseKeys.Length != frases.Count || forceUpdate)
        {
            fraseKeys = new string[frases.Count];
            for (int i = 0; i < frases.Count; i++)
            {
                if (frases[i] != null && !string.IsNullOrEmpty(frases[i].fraseKey))
                {
                    fraseKeys[i] = frases[i].fraseKey;
                }
                else
                {
                    fraseKeys[i] = "EMPTY_KEY_OR_ASSET";
                }
            }
        }
        return fraseKeys;
    }

    public List<string[]> ExportData()
    {
        int columns = langsForExport.Count + 1;

        List<string[]> data = new List<string[]>();

        string[] dataFormat = new string[columns];
        dataFormat[0] = "KEY";
        for (int i = 0; i < langsForExport.Count; i++)
        {
            dataFormat[i + 1] = langsForExport[i].ToString();
        }

        data.Add(dataFormat);

        for (int i = 0; i < frases.Count; i++)
        {
            dataFormat = new string[columns];
            if (frases[i] != null && !frases[i].fraseKey.Equals("empty") && !string.IsNullOrEmpty(frases[i].fraseKey))
            {
                dataFormat[0] = frases[i].fraseKey;
                for (int j = 0; j < langsForExport.Count; j++)
                {
                    dataFormat[j + 1] = frases[i].GetValueByLang(langsForExport[j]).Replace('\n',' ');
                }
                data.Add(dataFormat);
            }
        }
        return data;
    }

    public void ImportData(List<string[]> data)
    {
        string[] header = data[0];
        List<SystemLanguage> langs = new List<SystemLanguage>();
        for (int i = 1; i < header.Length; i++)
        {
            langs.Add((SystemLanguage)System.Enum.Parse(typeof(SystemLanguage), header[i]));
            Debug.Log(langs[i - 1]);
        }
        List<SystemLanguage> newLangs = langs.Except(langsForExport).ToList();
        foreach (SystemLanguage sl in newLangs)
        {
            AddLanguage(sl);
            DevLogs.Log("New langs "+ sl);
        }

        for (int i = 1; i < data.Count; i++)
        {
            if (data[i] == null || data[i].Length == 0)
                continue;

            string key = data[i][0];
            if (!string.IsNullOrEmpty(key))
            {
                LocalizedFrase fraseForEdit = GetFrase(key);
                if (fraseForEdit == null)
                {
                    fraseForEdit = AddFrase(key);
                    for (int k = 0; k < langsForExport.Count; k++)
                    {
                        fraseForEdit.AddLanguage(langsForExport[k]);
                    }
                }

                for (int j = 0; j < langs.Count; j++)
                {
                    try
                    {
                        fraseForEdit.SetLangValue(langs[j], data[i][j + 1]);
                    }
                    catch
                    {
                        fraseForEdit.SetLangValue(langs[j], "");
                    }
                }
                EditorUtility.SetDirty(fraseForEdit);
            }
        }
    }

    protected SystemLanguage[] showedLangs;
    public SystemLanguage[] ShowsLanguages
    {
        get
        {
            showedLangs = new SystemLanguage[0];
            showedLangs = new SystemLanguage[showLangs.FindAll((val) => { return val; }).Count()];
            int j = 0;
            for (int i = 0; i < showLangs.Count; i++)
            {
                if (showLangs[i])
                {
                    showedLangs[j] = langsForExport[i];
                    j++;
                }
            }
            return showedLangs;
        }
    }

    public void AddLanguage(SystemLanguage lang)
    {
        //if (langsForExport.Contains(lang))
        //    return;

        langsForExport.Add(lang);
        if (langsForExport.Count > showLangs.Count)
        {
            showLangs.Add(true);
        }

        foreach (LocalizedFrase lf in frases)
        {
            lf.AddLanguage(lang);
        }
    }

    public void RemoveLanguage(SystemLanguage lang)
    {
        int index = langsForExport.IndexOf(lang);
        if (langsForExport.Count == showLangs.Count)
        {            
            showLangs.RemoveAt(index);
        }
        langsForExport.RemoveAt(index);
    }

    public LocalizedFrase AddFrase(LocalizedFrase frase)
    {
        if (frases.Contains(frase))
            return frase;

        //for (int i = 0; i < langsForExport.Count; i++)
        //{
        //    frase.AddLanguage(langsForExport[i]);
        //}

        EditorUtility.SetDirty(frase);
        frases.Add(frase);
        //EditorUtility.DisplayDialog("Database updated",
        //        string.Format("New frase {0} added", frase.fraseKey), "Ok");

        return frase;
    }

    public LocalizedFrase AddFrase(string key)
    {
        if(string.IsNullOrEmpty(key))
        {
            return null;
        }
        foreach (string s in GetKeys())
        {
            if (s.Equals(key))
            {
                return GetFrase(key);
            }
        }

        LocalizedFrase asset = ScriptableObject.CreateInstance<LocalizedFrase>();
        asset.fraseKey = key;


        string path = AssetDatabase.GetAssetPath(this);
        if (path == "")
        {
            path = "Assets";
        }
        else if (Path.GetExtension(path) != "")
        {
            path = path.Replace(Path.GetFileName(AssetDatabase.GetAssetPath(this)), "");
        }

        string assetPathAndName = string.Format("{0}NewFrases_{1}/{2}.asset", path, name, key);
        Directory.CreateDirectory(string.Format("{0}NewFrases_{1}", path, name));
        AssetDatabase.CreateAsset(asset, assetPathAndName);

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();

        return AddFrase(asset); 
    }
#endif

}
