﻿using UnityEngine;
using System.Collections;
using TMPro;

[RequireComponent(typeof(TextMeshPro))]
public class LocalizedMeshController : AbstractLocalizedTextController
{
    public TextMeshPro txt;

    public TextMeshPro FraseText
    {
        get
        {
            if (txt == null)
            {
                txt = GetComponent<TextMeshPro>();
            }
            return txt;
        }
    }

    protected override void SetText(string newText)
    {
        if (frase != null && FraseText != null)
        {
            FraseText.text = newText;
        }
    }
}