﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class LocalizedTextController : AbstractLocalizedTextController
{
    public Text txt;

    public Text FraseText
    {
        get
        {
            if (txt == null)
            {
                txt = GetComponent<Text>();
            }
            return txt;
        }
    }

    protected override void SetText(string newText)
    {
        if (frase != null && FraseText != null)
        {
            FraseText.text = newText;
        }
    }
}
