﻿using UnityEngine;
using System.Collections;
using TMPro;

[RequireComponent(typeof(TextMeshProUGUI))]
public class LocalizedTextMeshController : AbstractLocalizedTextController
{
    public TextMeshProUGUI txt;

    public TextMeshProUGUI FraseText
    {
        get
        {
            if (txt == null)
            {
                txt = GetComponent<TextMeshProUGUI>();
            }
            return txt;
        }
    }

    protected override void SetText(string newText)
    {
        if (frase != null && FraseText != null)
        {
            FraseText.text = newText;
        }
    }
}

