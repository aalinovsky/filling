﻿using UnityEngine;

[System.Serializable]
public struct StringValue
{
    public string val;
    public SystemLanguage language;
    public Font font;
}
