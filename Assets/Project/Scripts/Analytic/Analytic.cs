﻿using UnityEngine;
using System.Collections;
using DefaultUtility;
using System.Collections.Generic;

public class Analytic : Singletone<Analytic>
{
    protected int daysSinceFirstLaunch;
    protected IntStorageData sessionsCount;
    private void Awake()
    {
        DailyVisit.SessionStarted();
        daysSinceFirstLaunch = this.GetNumberDaysSinceTheFirstLaunch();
        sessionsCount = new IntStorageData("sessionsCounter");
        sessionsCount.Value++;
    }

    private void Start()
    {

    }

    public void NoAdsClick(string screeName)
    {
        Dictionary<string, object> parameters = new Dictionary<string, object>();

        parameters.Add("screen", screeName);

        SendEvent("noAdsClick", parameters);
    }

    public void BuyNoAds()
    {
        SendEvent("noAdsPurchase", new Dictionary<string, object>());
    }

    public void SendEvent(string eventName)
    {
        SendEvent(eventName, new Dictionary<string, object>());
    }

    public void SendEvent(string eventName, Dictionary<string, object> parameters)
    {
#if AM
        AppMetrica.Instance.ReportEvent(eventName, GetDefaultParameters(parameters));
#endif
    }

    public void SendAdsEvent(string eventName, string screenName, string adsType)
    {
        Dictionary<string, object> param = new Dictionary<string, object>();
        param.Add("adsType", adsType);        
        param.Add("screen", screenName);
#if AM
        AppMetrica.Instance.ReportEvent(eventName, GetDefaultParameters(param));
#endif
    }

    protected Dictionary<string, object> GetDefaultParameters(Dictionary<string, object> parameters)
    {
        AddKeyAndValue(ref parameters, "configversion", GameSettings.Data.remote.ConfigVersion.Value);
        AddKeyAndValue(ref parameters, "daysSinceFirstLaunch", daysSinceFirstLaunch);
        AddKeyAndValue(ref parameters, "sessionsCount", sessionsCount.Value);
        AddKeyAndValue(ref parameters, "level", GameSettings.LevelNumber.Value);

        return parameters;
    }

    protected void AddKeyAndValue(ref Dictionary<string, object> parameters, string key, object value)
    {
        if (!parameters.ContainsKey(key))
        {
            parameters.Add(key, value);
        }
    }
}
