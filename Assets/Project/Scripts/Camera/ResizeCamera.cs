﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Camera))]
public class ResizeCamera : MonoBehaviour
{
    private new Camera camera;
    
    public float DesignAspectHeight;
    public float DesignAspectWidth;

    public void Awake()
    {
        Resize();
    }

    public void Resize()
    {
        float targetaspect = DesignAspectWidth / DesignAspectHeight; 

        float windowaspect = (float)Screen.width / (float)Screen.height;

        float scaleheight = windowaspect / targetaspect;

        Camera camera = GetComponent<Camera>();

        camera.orthographicSize = camera.orthographicSize / scaleheight;

    }
}
