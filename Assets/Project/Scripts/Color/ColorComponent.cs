﻿using UnityEngine;
using System.Collections;
using TMPro;
using UnityEngine.UI;

public class ColorComponent : MonoBehaviour
{
    public Component componentForColorize;
    public ColorType color;

    private void Start()
    {
        if (componentForColorize != null)
        {
            GameColorizer.Instance.RegisterComponent(componentForColorize, color);
            Destroy(this);
        }
    }

#if UNITY_EDITOR

    [DefaultUtility.Button()]
    public void FindComponent()
    {
        FindComponent(typeof(Image));
        FindComponent(typeof(SpriteRenderer));
        FindComponent(typeof(TextMeshProUGUI));
        FindComponent(typeof(TextMeshPro));
    }

    protected void FindComponent(System.Type t)
    {
        Component c = GetComponent(t);
        if (c != null)
            componentForColorize = c;
    }

#endif
}
