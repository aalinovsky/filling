﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "colorPreset", menuName = "ColorizeGame/Preset")]
public class ColorPreset : ScriptableObject
{
    public Sprite bgSprite;
    public Color white = Color.white;
    public Color whiteBorderline = Color.white;
    public Color black = Color.black;
    public Color enemy = Color.red;
    public Color enemyUnscale = Color.red;

    public Color GetColor(ColorType t)
    {
        switch (t)
        {
            case ColorType.Elements:
                return white;
            case ColorType.Background:
                return black;
            case ColorType.Enemy:
                return enemy;
            case ColorType.UnscaleParticles:
                return enemyUnscale;
            default:
                return white;
        }
    }

    public Color GetShapeColor()
    {
        return Color.Lerp(white, whiteBorderline, UnityEngine.Random.Range(0.0f, 1.0f));
    }
}
