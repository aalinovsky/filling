﻿using UnityEngine;
using System.Collections;

public enum ColorType
{
    Background,
    Elements,
    Enemy,
    UnscaleParticles
}
