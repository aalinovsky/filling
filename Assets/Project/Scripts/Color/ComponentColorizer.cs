﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class ComponentColorizer<T> : GameColorizeController where T : Component
{
    protected List<T> whiteList = new List<T>();
    protected List<T> blackList = new List<T>();
    protected List<T> another1List = new List<T>();
    protected List<T> another2List = new List<T>();

    public override void SetPreset(ColorPreset newPreset)
    {
        currentPreset = newPreset;

        SetColor(whiteList.ToArray(), newPreset.GetColor(ColorType.Elements));
        SetColor(blackList.ToArray(), newPreset.GetColor(ColorType.Background));
        SetColor(another1List.ToArray(), newPreset.GetColor(ColorType.Enemy));
        SetColor(another2List.ToArray(), newPreset.GetColor(ColorType.UnscaleParticles));
    }

    public override void RegisterComponent(Component component, ColorType clr)
    {
        if(typeof(T) == component.GetType())
        {
            var com = (T)component;
            switch (clr)
            {
                case ColorType.Background:
                    blackList.Add(com);
                    break;
                case ColorType.Elements:
                    whiteList.Add(com);
                    break;
                case ColorType.Enemy:
                    another1List.Add(com);
                    break;
                case ColorType.UnscaleParticles:
                    another2List.Add(com);
                    break;
                default:
                    break;
            }

            if(currentPreset != null)
            {
                SetColor(com, currentPreset.GetColor(clr));
            }

        }
    }

    protected abstract void SetColor(T[] images, Color c);
    protected abstract void SetColor(T image, Color c);
}
