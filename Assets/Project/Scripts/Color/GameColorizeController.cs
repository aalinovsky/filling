﻿using UnityEngine;
using System.Collections;

public abstract class GameColorizeController : MonoBehaviour
{
    protected ColorPreset currentPreset;

    public abstract void SetPreset(ColorPreset newPreset);
    public abstract void RegisterComponent(Component component, ColorType clr);
}
