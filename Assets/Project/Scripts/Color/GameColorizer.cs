﻿using UnityEngine;
using System.Collections;
using DefaultUtility;
using UnityEngine.UI;

public class GameColorizer : Singletone<GameColorizer>
{
    public SpriteRenderer background;

    public ColorPreset[] pressets;

    ColorPreset currentPreset = null;
    public ColorPreset CurrentPreset
    {
        get
        {
            if(currentPreset == null)
                return pressets[currentPressetIndex];
            return currentPreset;
        }
    }

    protected int currentPressetIndex = 0;
    [Space()]
    public GameColorizeController[] colorizers;

#if UNITY_EDITOR
    [Button()]
    public void NextColor()
    {
        currentPressetIndex++;
        UpdateColor();
    }

    [Button()]
    public void UpdateColor()
    {
        SetColor(currentPressetIndex);
    }
#endif

    public void SetColor(int id)
    {
        if(id >= 0 && id < pressets.Length)
        {
            currentPressetIndex = id;
            SetColor(pressets[currentPressetIndex]);
        }
        currentPreset = pressets[currentPressetIndex];
    }

    public void SetColor(ColorPreset preset)
    {
        currentPreset = preset;
        for (int i = 0; i < colorizers.Length; i++)
        {
            colorizers[i].SetPreset(preset);
        }

        background.sprite = preset.bgSprite;
        background.gameObject.SetActive(preset.bgSprite != null);
    }

    public void RegisterComponent(Component component, ColorType clr)
    {
        for (int i = 0; i < colorizers.Length; i++)
        {
            colorizers[i].RegisterComponent(component, clr);
        }
    }
}
