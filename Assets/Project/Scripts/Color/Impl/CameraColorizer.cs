﻿using UnityEngine;
using System.Collections;

public class CameraColorizer : GameColorizeController
{
    public Camera mainCam;

    public override void RegisterComponent(Component component, ColorType clr)
    {
    }

    public override void SetPreset(ColorPreset newPreset)
    {
        mainCam.backgroundColor = newPreset.GetColor(ColorType.Background);
    }
}
