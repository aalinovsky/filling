﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ImageColorizer : ComponentColorizer<Image>
{
    protected override void SetColor(Image[] images, Color c)
    {
        for (int i = 0; i < images.Length; i++)
        {
            SetColor(images[i], c);
        }
    }

    protected override void SetColor(Image image, Color c)
    {
        var newC = c;
        newC.a = image.color.a;
        image.color = newC;
    }
}