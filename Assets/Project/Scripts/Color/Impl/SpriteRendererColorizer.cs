﻿using UnityEngine;
using System.Collections;

public class SpriteRendererColorizer : ComponentColorizer<SpriteRenderer>
{
    protected override void SetColor(SpriteRenderer[] images, Color c)
    {
        for (int i = 0; i < images.Length; i++)
        {
            SetColor(images[i], c);
        }
    }

    protected override void SetColor(SpriteRenderer image, Color c)
    {
        Color newC = c;
        newC.a = image.color.a;
        image.color = newC;
    }
}