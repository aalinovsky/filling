﻿using UnityEngine;
using System.Collections;
using TMPro;

public class TextColorizer : ComponentColorizer<TextMeshProUGUI>
{
    protected override void SetColor(TextMeshProUGUI[] images, Color c)
    {
        for (int i = 0; i < images.Length; i++)
        {
            SetColor(images[i], c);
        }
    }

    protected override void SetColor(TextMeshProUGUI image, Color c)
    {
        Color newC = c;
        newC.a = image.color.a;
        image.color = newC;
    }
}