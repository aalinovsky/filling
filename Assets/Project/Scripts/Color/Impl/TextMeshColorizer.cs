﻿using UnityEngine;
using System.Collections;
using TMPro;

public class TextMeshColorizer : ComponentColorizer<TextMeshPro>
{
    protected override void SetColor(TextMeshPro[] images, Color c)
    {
        for (int i = 0; i < images.Length; i++)
        {
            SetColor(images[i], c);
        }
    }

    protected override void SetColor(TextMeshPro image, Color c)
    {
        Color newC = c;
        newC.a = image.color.a;
        image.color = newC;
    }
}
