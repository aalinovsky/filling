﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using CustomService;
#if FIREBASE
using Firebase;
#endif
using UnityEditor;
using UnityEngine;

public class FirebaseManager : MonoBehaviour
{
    #if FIREBASE
    DependencyStatus dependencyStatus = DependencyStatus.UnavailableOther;
    private FirebaseApp firebaseInstance;
    private bool isInitialized = false;

    private void Start()
    {        
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            dependencyStatus = task.Result;
            if (dependencyStatus == DependencyStatus.Available)
            {
                InitializeFirebase();
            }
            else
            {
                Debug.LogError(
                  "Could not resolve all Firebase dependencies: " + dependencyStatus);
            }
        });
    }

    private void InitializeFirebase()
    {
        Dictionary<string, object> defaults = new Dictionary<string, object>();

        defaults.Add("ConfigVersion", GameSettings.Data.remote.ConfigVersion.Value);
        defaults.Add("AdsSettings", GameSettings.AdsData.Settings);
        defaults.Add("InterstitialFrequency", GameSettings.Data.remote.InterstitialFrequency.Value);
        defaults.Add("EnemySpeed", GameSettings.Data.remote.EnemySpeed.Value);
        defaults.Add("ScaleFactor", GameSettings.Data.gameplay.scaleSquareFactor);

        Firebase.RemoteConfig.FirebaseRemoteConfig.SetDefaults(defaults);
        Debug.Log("RemoteConfig configured and ready!");
        isInitialized = true;

        FetchDataAsync();
    }

    /// <summary>
    /// Displays the data.
    /// </summary>
    public void DisplayData()
    {
        Debug.Log("Current Data:");
        Debug.Log("configVersion : " +
                 Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("configVersion").StringValue);
    }

    /// <summary>
    /// Fetches the data asynchronous.
    /// </summary>
    public Task FetchDataAsync()
    {
        Debug.Log("Fetching data...");

        // FetchAsync only fetches new data if the current data is older than the provided
        // timespan.  Otherwise it assumes the data is "recent enough", and does nothing.
        // By default the timespan is 12 hours, and for production apps, this is a good
        // number.  For this example though, it's set to a timespan of zero, so that
        // changes in the console will always show up immediately.
        Task fetchTask = Firebase.RemoteConfig.FirebaseRemoteConfig.FetchAsync(TimeSpan.Zero);
        return fetchTask.ContinueWith(FetchComplete);
    }

    private void FetchComplete(Task fetchTask)
    {
        if (fetchTask.IsCanceled)
        {
            Debug.Log("Fetch canceled.");
        }
        else if (fetchTask.IsFaulted)
        {
            Debug.Log("Fetch encountered an error.");
        }
        else if (fetchTask.IsCompleted)
        {
            Debug.Log("Fetch completed successfully!");
        }

        var info = Firebase.RemoteConfig.FirebaseRemoteConfig.Info;
        switch (info.LastFetchStatus)
        {
            case Firebase.RemoteConfig.LastFetchStatus.Success:
                Firebase.RemoteConfig.FirebaseRemoteConfig.ActivateFetched();
                Debug.Log(string.Format("Remote data loaded and ready (last fetch time {0}).",
                                       info.FetchTime));

                break;
            case Firebase.RemoteConfig.LastFetchStatus.Failure:
                switch (info.LastFetchFailureReason)
                {
                    case Firebase.RemoteConfig.FetchFailureReason.Error:
                        Debug.Log("Fetch failed for unknown reason");
                        break;
                    case Firebase.RemoteConfig.FetchFailureReason.Throttled:
                        Debug.Log("Fetch throttled until " + info.ThrottledEndTime);
                        break;
                }
                break;
            case Firebase.RemoteConfig.LastFetchStatus.Pending:
                Debug.Log("Latest Fetch call still pending.");
                break;
        }
        DisplayData();

        GameSettings.Data.remote.ConfigVersion.Value = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("ConfigVersion").StringValue;
        GameSettings.AdsData.Settings = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("AdsSettings").StringValue;
        GameSettings.Data.remote.InterstitialFrequency.Value = (int)Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("InterstitialFrequency").DoubleValue;
        GameSettings.Data.remote.EnemySpeed.Value = (float)Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("EnemySpeed").DoubleValue;        
        GameSettings.Data.gameplay.scaleSquareFactor = (float)Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("ScaleFactor").DoubleValue;        
    }
#endif
}
