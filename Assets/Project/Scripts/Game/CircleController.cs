﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class CircleController : MonoBehaviour
{
    public Sprite[] meshSprites;
    public Sprite meshEmptySprite;
    public SpriteRenderer mesh;
    public SpriteMask mask;
    public ParticleSystem endDragEffect;
    public ParticleSystem explosionEffect;

    public List<ShapeController> shapes;

    public SpriteRenderer sprite;
    public float decreeseStep = 0.2f;
    public int health = 15;
    public float vibrateDlt = 0.5f;
    public float minScale = 0.1f;
    public float maxScale = 0.1f;
    [SerializeField]
    protected Collider2D Collider
    {
        get
        {
            if(currentShape != null)
                return currentShape.collider;

            return null;
        }
    }
    protected bool isTouched;

    protected float currentHealth = 0;
    protected float scaleTime = 0;
    protected float vibroTime = 0;
    protected Vector3 startScale;
    protected Vector3 targetScale;
    protected float scaleDuration = 1.1f;
    protected ShapeController currentShape;

    protected float CurrentHealth
    {
        get
        {
            return currentHealth;
        }

        set
        {
            if (currentHealth != value)
            {
                currentHealth = value;
                UpdatDamageImage();
            }
        }
    }

    public bool IsTouched
    {
        get
        {
            return isTouched;
        }

        set
        {            
            startScale = new Vector3(minScale, minScale, minScale);
            targetScale = new Vector3(maxScale, maxScale, maxScale);
            IsKinematic = isTouched = value;

            if (!isTouched)
                endDragEffect.Play();

            if (!isTouched && LevelController.Instance.State != LevelState.Failed && IsBigBall && gameObject.activeInHierarchy)
            {
                BigBallCreated();
            }
        }
    }

    public bool IsBigBall
    {
        get
        {
            return Square / LevelController.Instance.gameField.Calculate() >= GameSettings.Data.gameplay.bigBallSquarePercents;
        }
    }

    public float Square
    {
        get
        {
            if(currentShape != null)
                return currentShape.GetSquare();

            return 0;
            //return Mathf.PI * Mathf.Pow(transform.localScale.x / 2.0f, 2);
        }
    }

    public void SetShape(Shape type)
    {
        if (sprite != null)
        {
            sprite.color = endDragEffect.startColor = explosionEffect.startColor = GameColorizer.Instance.CurrentPreset.GetShapeColor();
        }

        foreach (var item in shapes)
        {
            item.gameObject.SetActive(false);
        }

        currentShape = shapes.Find(o => o.shapeType.shape == type);
        if(currentShape != null)
        {
            ParticleSystem.ShapeModule settings = endDragEffect.shape;

            sprite.sprite = mask.sprite = settings.sprite = currentShape.shapeType.icon;
            currentShape.gameObject.SetActive(true);
        }
    }

    private void Awake()
    {
        SetShape(GameSettings.Data.gameplay.defaultShape);
        CurrentHealth = health;
    }

    private void Start()
    {
        mesh.size = Vector2.one;
        
        scaleDuration = GameSettings.Data.gameplay.fillingDuration;
        
        //if (collide == null)
        //    collide = GetComponent<CircleCollider2D>();
    }

    public void BigBallCreated()
    {
        GameSettings.UI.GetScreen<GameScreen>().DrawRewardForBigBall(transform.position);
    }

    public void Reset()
    {
        mask.gameObject.SetActive(false);
        CurrentHealth = health;
        scaleTime = 0;        
        transform.localScale = new Vector3(minScale, minScale, minScale);
        startScale = new Vector3(minScale, minScale, minScale);
        targetScale = new Vector3(maxScale, maxScale, maxScale);
        if(IsTouched)
            IsTouched = false;
        sprite.enabled = true;
        mesh.size = Vector2.one;
        Collider.enabled = true;
        mesh.enabled = true;
    }

    private void Update()
    {
        if (isTouched) {
            float dlt = Mathf.Min(scaleTime / scaleDuration, 1);
            transform.localScale = Vector3.Slerp(startScale, targetScale, dlt);
            endDragEffect.transform.localScale = transform.localScale * 0.1f;
            scaleTime += Time.deltaTime;
            vibroTime += Time.deltaTime;

            if(vibroTime >= vibrateDlt)
            {
                vibroTime = 0;
                Vibrate.Play(VibrateType.Peek);
                SoundManager.PlaySound(GameSettings.Data.gameplay.fillSound, Mathf.Lerp(GameSettings.Data.gameplay.fillSoundPitch.x, GameSettings.Data.gameplay.fillSoundPitch.y, dlt));
            }

            if(dlt == 1)
            {
                LevelController.Instance.TouchFinish(Vector3.zero);
            }
        }
    }

    public bool IsKinematic
    {
        get
        {
            return  Collider.attachedRigidbody.isKinematic;
        }

        set
        {
            Collider.attachedRigidbody.gravityScale = value ? 0 : 1;
            //collide.attachedRigidbody.isKinematic = value;
        }
    }

    public void Collision(Collision2D collision)
    {
        if (IsTouched)
        {
            if (collision.transform.CompareTag(Tags.CIRCLE))
            {
                CircleCollizion();
            }
            else if (collision.transform.CompareTag(Tags.ENEMY))
            {
                EnemyCollizion();
            }
            else if (collision.transform.CompareTag(Tags.WALLS))
            {
                WallCollizion();
            }
        }
    }

    protected void EnemyCollizion()
    {
        if (IsTouched)
        {
            LevelController.Instance.GameOver();
        }
    }

    protected void CircleCollizion()
    {
        LevelController.Instance.TouchFinish(Vector3.zero);
    }

    protected void WallCollizion()
    {
        //LevelController.Instance.TouchMove(transform.position);
    }

    protected void UpdatDamageImage()
    {
        Sprite meshSprite = meshEmptySprite;

        if(meshSprites.Length > 0)
        {
            int stepsLength = (int)((float)health / (float)meshSprites.Length);

            int meshIndex = (int)(meshSprites.Length - CurrentHealth / stepsLength) - 1;

            if(meshIndex >= 0 && meshIndex < meshSprites.Length)
            {
                meshSprite = meshSprites[meshIndex];
                mask.gameObject.SetActive(true);
            }
        }

        mesh.sprite = meshSprite;
    }

    protected void DestroyShape()
    {
        LevelController.Instance.DestroyShape(Square);

        gameObject.SetActive(false);
        Reset();
    }

    public void HitShape(int dmg = 1)
    {
        if (CurrentHealth > 0)
        {
            CurrentHealth -= dmg;
            if(CurrentHealth <= 0)
            {
                DestroyShape();
            }
        }
    } 

    public void Hit()
    {
        float step = (targetScale - startScale).x / (float)health;

        if(transform.localScale.x - step >= minScale)
        {
            float currentSquare = Square;
            transform.localScale -= new Vector3(step, step, 0);
            float newSquare = Square;

            LevelController.Instance.DestroyShape(currentSquare - newSquare);
        }
    }

    public void Explosion()
    {
        if(isTouched)
            LevelController.Instance.TouchFinish(Vector3.zero);


        if (explosionEffect != null)
            explosionEffect.Play();
        
        currentShape.gameObject.SetActive(false);
        sprite.enabled = false;
        Collider.enabled = false;
        IsKinematic = true;
        mesh.enabled = false;

        LevelController.Instance.DestroyShape(this, explosionEffect.main.duration+0.2f);
    }
}
