﻿using UnityEngine;
using System.Collections;

public class CirclePool : Pool<CircleController>
{
    public override void Reset()
    {
        for (int i = pool.Count-1; i >= 0; i--)
        {
            if (pool[i] != null && pool[i].gameObject.activeInHierarchy)
            {
                pool[i].Reset();
                Despawn(pool[i]);
            }
        }
    }
}
