﻿using UnityEngine;
using System.Collections;

public class EnemyBombBehaviour : IEnemyBehaviour
{
    protected EnemyController enemy;
    protected IEnumerator coolingDownRoutine;

    protected float coolingDownSpeed;
    protected float heatSpeed;

    protected float heat = 0;
    protected float Heat
    {
        get
        {
            return heat;
        }

        set
        {
            heat = value;
            heat = Mathf.Clamp(heat, 0.0f, 1.0f);

            enemy.SetHeat(heat);

            if(heat == 0 && coolingDownRoutine != null)
            {
                enemy.StopCoroutine(coolingDownRoutine);
                coolingDownRoutine = null;
            }
            else if(heat > 0 && coolingDownRoutine == null)
            {
                coolingDownRoutine = CoolingDown();
                enemy.StartCoroutine(coolingDownRoutine);
            }
            else if (heat == 1)
            {
                Explosion();
            }
        }
    }

    public void CollizionExit(CircleController circle)
    {
        HitShape();
    }

    public IEnemyBehaviour Init(EnemyController control)
    {
        coolingDownSpeed = GameSettings.Data.gameplay.coolingDownSpeed;
        heatSpeed = GameSettings.Data.gameplay.heatSpeed;
        enemy = control;
        return this;
    }

    public void Reset()
    {
        enemy.Collide.attachedRigidbody.velocity = Vector2.zero;
    }

    public void StartMovement()
    {
        if (enemy == null)
            return;

        enemy.Collide.attachedRigidbody.velocity = (new Vector2(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f))).normalized * GameSettings.Data.remote.EnemySpeed.Value;

        if (enemy.Collide.attachedRigidbody.velocity.x == 0 || enemy.Collide.attachedRigidbody.velocity.y == 0)
        {
            StartMovement();
        }
    }

    protected void HitShape()
    {
        Heat += heatSpeed;
    }

    protected void Explosion()
    {
        enemy.Explosion();
        Heat = 0;
    }

    protected IEnumerator CoolingDown()
    {
        while (Heat > 0)
        {
            yield return new WaitForSeconds(Time.fixedDeltaTime);
            Heat -= coolingDownSpeed;
        }
    }
}
