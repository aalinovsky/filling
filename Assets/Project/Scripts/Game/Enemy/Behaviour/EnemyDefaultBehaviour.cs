﻿using UnityEngine;
using System.Collections;

public class EnemyDefaultBehaviour : IEnemyBehaviour
{
    protected EnemyController enemy;

    public void CollizionExit(CircleController circle)
    {
        
    }

    public IEnemyBehaviour Init(EnemyController control)
    {
        enemy = control;
        return this;
    }

    public void Reset()
    {
        enemy.Collide.attachedRigidbody.velocity = Vector2.zero;
    }

    public void StartMovement()
    {
        if (enemy == null)
            return;

        enemy.Collide.attachedRigidbody.velocity = (new Vector2(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f))).normalized * GameSettings.Data.remote.EnemySpeed.Value;

        if (enemy.Collide.attachedRigidbody.velocity.x == 0 || enemy.Collide.attachedRigidbody.velocity.y == 0)
        {
            StartMovement();
        }
    }
}
