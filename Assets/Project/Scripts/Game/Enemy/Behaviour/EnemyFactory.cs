﻿using UnityEngine;
using System.Collections;

public static class EnemyFactory
{ 
    public static IEnemyBehaviour CreateEnemy(EnemyType type, EnemyController enemy)
    {
        IEnemyBehaviour newEnemy = null;

        switch (type)
        {
            case EnemyType.Default:
                newEnemy = new EnemyDefaultBehaviour();
                break;
            case EnemyType.Unscale:
                newEnemy = new EnemyUnscaleBehaviour();
                break;
            case EnemyType.Bomb:
                newEnemy = new EnemyBombBehaviour();
                break;
            default:
                newEnemy = new EnemyDefaultBehaviour();
                break;
        }

        newEnemy.Init(enemy);
        return newEnemy;
    }
}
