﻿public interface IEnemyBehaviour
{
    void Reset();
    IEnemyBehaviour Init(EnemyController control);
    void CollizionExit(CircleController circle);
    void StartMovement();
}