﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(CircleCollider2D))]
public class EnemyController : MonoBehaviour
{
    public ParticleSystem explosionParticle;
    public SpriteRenderer sprite;
    protected CircleCollider2D collide;

    public CircleCollider2D Collide
    {
        get
        {
            return collide;
        }
    }

    public TrailRenderer trail;
    public GameObject boostEffect;
    public GameObject heatEffect;
    protected EnemyType type = EnemyType.Default;
    protected IEnemyBehaviour behaviour;

    protected Skin currentSkin;
    public Skin CurrentSkin
    {
        get
        {
            if (currentSkin == null)
                currentSkin = SkinsController.Instance.CurrentSkin;
            return currentSkin;
        }
    }

    private void Awake()
    {
        if (collide == null)
            collide = GetComponent<CircleCollider2D>();    
    }

    private void Start()
    {
        InitSkin(SkinsController.Instance.CurrentSkin);
        SkinsController.Instance.updateSkinEvent += InitSkin;
    }

    protected void InitSkin(Skin s)
    {
        currentSkin = s;
        UpdateSkin();
    }

    protected Color SkinColor
    {
        get
        {
            if (!currentSkin.useCustomColor)
            {
                return GameColorizer.Instance.CurrentPreset.GetColor(ColorType.Enemy);
            }
            else
            {
                return currentSkin.customColor;
            }
        }
    }

    protected void UpdateSkin()
    {
        if (currentSkin == null)
            return;

        sprite.sprite = currentSkin.icon;

        if (sprite != null)
        {
            sprite.color = SkinColor;
        }

        if (trail != null)
        {
            Gradient clr = new Gradient();
            clr.colorKeys = new GradientColorKey[2];
            clr.alphaKeys = new GradientAlphaKey[2];

            clr.alphaKeys[0] = new GradientAlphaKey() { alpha = 1, time = 0 };
            clr.alphaKeys[1] = new GradientAlphaKey() { alpha = 1, time = 1 };

            if (!currentSkin.useCustomColor)
            {
                clr.colorKeys[0] = new GradientColorKey() { color = GameColorizer.Instance.CurrentPreset.GetColor(ColorType.Enemy), time = 0 };
                clr.colorKeys[1] = new GradientColorKey() { color = GameColorizer.Instance.CurrentPreset.GetColor(ColorType.Enemy), time = 1 };
            }
            else
            {
                clr.colorKeys[0] = new GradientColorKey() { color = currentSkin.customColor, time = 0 };
                clr.colorKeys[1] = new GradientColorKey() { color = currentSkin.customColor, time = 1 };
            }

            trail.colorGradient = clr;
            trail.startColor = GameColorizer.Instance.CurrentPreset.GetColor(ColorType.Enemy);
            trail.endColor = GameColorizer.Instance.CurrentPreset.GetColor(ColorType.Enemy);
            trail.Clear();
        }
    }

    public void Reset()
    {
        UpdateSkin();        
        if(behaviour != null)
        {
            behaviour.Reset();
        }
    }

    public void Init(EnemyType newType)
    {
        boostEffect.SetActive(false);
        heatEffect.SetActive(false);
        switch (newType)
        {
            case EnemyType.Unscale:
                boostEffect.SetActive(true);
                ParticleSystem.MainModule unslaceParticles = boostEffect.GetComponentInChildren<ParticleSystem>().main;
                unslaceParticles.startColor = CurrentSkin.unscaleParticles;
                break;
            case EnemyType.Bomb:
                heatEffect.SetActive(true);
                break;
            default:
                
                break;
        }
        behaviour = EnemyFactory.CreateEnemy(newType, this);
        this.type = newType;
        StartMovement();
    }

    public void StartMovement()
    {
        if (behaviour != null)
            behaviour.StartMovement();
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.transform.CompareTag(Tags.CIRCLE))
        {
            CircleController circle = collision.transform.GetComponent<CircleController>();

            if (behaviour != null)
                behaviour.CollizionExit(circle);

            collide.attachedRigidbody.velocity = (transform.position - collision.transform.position).normalized * GameSettings.Data.remote.EnemySpeed.Value; 
            if(collide.attachedRigidbody.velocity.magnitude == 0)
            {
                StartMovement();
            }
            return;
        }

        if (collide.attachedRigidbody.velocity.magnitude < GameSettings.Data.remote.EnemySpeed.Value - GameSettings.Data.gameplay.enemySpeedDelta ||
            collide.attachedRigidbody.velocity.magnitude > GameSettings.Data.remote.EnemySpeed.Value + GameSettings.Data.gameplay.enemySpeedDelta)
        {
            StartMovement();
        }

        if (collide.attachedRigidbody.velocity.x == 0 || collide.attachedRigidbody.velocity.y == 0)
        {
            StartMovement();
        }
    }

    public void Explosion()
    {
        List<CircleController> circles = LevelController.Instance.Circles;

        foreach (var circle in circles)
        {
            float distanceSqr = Vector3.Distance(transform.position, circle.transform.position) - circle.transform.localScale.x * 0.8f;
            if (distanceSqr <= GameSettings.Data.gameplay.enemyExplosionRange)
                circle.Explosion();
        }

        if (explosionParticle != null)
            explosionParticle.Play();
    }

    public void SetHeat(float heatValue)
    {
        sprite.color = Color.Lerp(SkinColor, GameSettings.Data.gameplay.explosionColor, heatValue);
    }
}

public enum EnemyBoostType
{
    Destroy,
    Scale,
    None
}

public enum EnemyType
{
    Default,
    Unscale,
    Bomb,
    Boid
}
