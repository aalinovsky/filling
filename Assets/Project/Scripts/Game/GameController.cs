﻿using UnityEngine;
using System.Collections;
using CustomService;
using System.Linq;

public class GameController : Singletone<GameController>
{
    public bool autoStartGame = false;
    public LocalizedFrase freeCoinsFrase;
    public AdsPlacement rewardedVideo;
    public AdsPlacement interstitial;
    [SerializeField]
    protected LevelController level;
    [SerializeField]
    protected GameColorizer colorizer;

    protected int restartCounter = 0;

    void Start()
    {
        colorizer.SetColor(GameSettings.LevelNumber.Value - 1);
        InitUIEvents();
        GameSettings.AdsData.banner.ShowBanner();
        if (autoStartGame)
            StartGame();
    }

    public void LevelCompleted()
    {
        Analytic.Instance.SendEvent("LevelComplete");
        CustomAnalyticController.Instance.SendLevelEvent(level.levelNumber.Value.ToString(), CustomLevelEvent.Complete);
#if GA
        GameAnalyticsSDK.GameAnalytics.NewProgressionEvent(GameAnalyticsSDK.GAProgressionStatus.Complete, "level", level.levelNumber.Value);
#endif
        GameSettings.UI.GetScreen<LevelCompleteScreen>().Show(rewardedVideo.IsReadyToShow);
    }

    public void LevelFailed()
    {
        GameSettings.UI.GetScreen<GameScreen>().Hide();
        Analytic.Instance.SendEvent("LevelFail");
        CustomAnalyticController.Instance.SendLevelEvent(level.levelNumber.Value.ToString(), CustomLevelEvent.Fail);
#if GA
        GameAnalyticsSDK.GameAnalytics.NewProgressionEvent(GameAnalyticsSDK.GAProgressionStatus.Fail, "level", level.levelNumber.Value);
#endif
        if (rewardedVideo.IsReadyToShow && GameSettings.LevelNumber.Value > 1)
        {
            GameSettings.UI.GetScreen<ContinueScreen>().Show();
        }
        else
        {
            if (!InNeedNewShape)
            {
                GameSettings.UI.GetScreen<LevelFailScreen>().Show(rewardedVideo.IsReadyToShow);
                GameSettings.UI.GetScreen<BottomButtonsScreen>().Show();
            }
            else
            {
                ShowNewShape();
            }
        }
    }

    public void StartGame()
    {
        GameSettings.UI.GetScreen<GameScreen>().Show();
        GameSettings.AdsData.banner.ShowBanner();
        level.RestartGame();
        Analytic.Instance.SendEvent("LevelStart");
        CustomAnalyticController.Instance.SendLevelEvent(level.levelNumber.Value.ToString(), CustomLevelEvent.Start);
#if GA
        GameAnalyticsSDK.GameAnalytics.NewProgressionEvent(GameAnalyticsSDK.GAProgressionStatus.Start, "level", level.levelNumber.Value);
#endif
    }

    protected void NextLevel()
    {
        level.NextLevel();
        GameSettings.UI.GetScreen<GameScreen>().Show();        

        Analytic.Instance.SendEvent("LevelStart");
        CustomAnalyticController.Instance.SendLevelEvent(level.levelNumber.Value.ToString(), CustomLevelEvent.Start);
#if GA
        GameAnalyticsSDK.GameAnalytics.NewProgressionEvent(GameAnalyticsSDK.GAProgressionStatus.Start, "level", level.levelNumber.Value);
#endif
    }

    protected void RestartGame()
    {
        restartCounter++;
        GameSettings.UI.GetScreen<GameScreen>().Show();
        level.RestartGame();
        //colorizer.SetColor(GameSettings.LevelNumber.Value - 1);
        Analytic.Instance.SendEvent("LevelReStart");
        CustomAnalyticController.Instance.SendLevelEvent(level.levelNumber.Value.ToString(), CustomLevelEvent.Start);
#if GA
        GameAnalyticsSDK.GameAnalytics.NewProgressionEvent(GameAnalyticsSDK.GAProgressionStatus.Start, "level", level.levelNumber.Value);
#endif
    }

    protected void ContinueGame()
    {
        //colorizer.SetColor(GameSettings.LevelNumber.Value);
        level.Continue();
        GameSettings.UI.GetScreen<GameScreen>().Show();
        Analytic.Instance.SendEvent("LevelContinue");
        CustomAnalyticController.Instance.SendLevelEvent(level.levelNumber.Value.ToString(), CustomLevelEvent.Start);
#if GA
        GameAnalyticsSDK.GameAnalytics.NewProgressionEvent(GameAnalyticsSDK.GAProgressionStatus.Start, "level", level.levelNumber.Value);
#endif
    }

    //////////////Functions for UI////////////////

    protected void InitUIEvents()
    {
        GameSettings.UI.GetScreen<BottomButtonsScreen>().rate.RegisterAction(ServiceController.Instance.Rate);
        GameSettings.UI.GetScreen<BottomButtonsScreen>().settings.RegisterAction(ShowSettings);
        GameSettings.UI.GetScreen<BottomButtonsScreen>().shop.RegisterAction(RequestOpenShop);
        GameSettings.UI.GetScreen<BottomButtonsScreen>().top.RegisterAction(ServiceController.Instance.ShowTop);
        GameSettings.UI.GetScreen<BottomButtonsScreen>().share.RegisterAction(ServiceController.Instance.Share);

        GameSettings.UI.GetScreen<ShopScreen>().closeBtn.RegisterAction(RequestHideShop);
        GameSettings.UI.GetScreen<ShopScreen>().freeCoins.RegisterAction(RequestCoinsShop);

        GameSettings.UI.GetScreen<NewShapeScreen>().closeBtn.RegisterAction(CollectShape);

        GameSettings.UI.GetScreen<MainScreen>().startAction += RequestStartGame;

        GameSettings.UI.GetScreen<LevelFailScreen>().againAction += RequestRestartLevel;
        GameSettings.UI.GetScreen<LevelFailScreen>().freeCoinsAction += RequestCoinsAfterFail;
        GameSettings.UI.GetScreen<LevelFailScreen>().homeAction += MainFromFail;

        GameSettings.UI.GetScreen<ContinueScreen>().continueAction += RequestContinueAfterDeath;
        GameSettings.UI.GetScreen<ContinueScreen>().cancelAction += CancelContinueRequest;

        GameSettings.UI.GetScreen<LevelCompleteScreen>().nextLevelAction += RequestNextLevel;
        GameSettings.UI.GetScreen<LevelCompleteScreen>().doubleRewardAction += RequestDoubleReward;

        GameSettings.UI.GetScreen<GameScreen>().pauseAction += RequestPause;

        level.fillingValue.RegisterSetter(GameSettings.UI.GetScreen<GameScreen>().SetFilling);
        level.fillingValue.RegisterSetter(GameSettings.UI.GetScreen<LevelFailScreen>().progress.UpdateValues);
        level.fillingValue.RegisterSetter(GameSettings.UI.GetScreen<ContinueScreen>().progress.UpdateValues);

        level.ballsValue.RegisterSetter(GameSettings.UI.GetScreen<GameScreen>().SetBallsCount);

        SetCoinsCount(GameSettings.Coins.Value);
        GameSettings.Coins.RegisterSetter(SetCoinsCount);
    }

    protected void ShowSettings()
    {
        GameSettings.UI.GetScreen<SettingsScreen>().Show();
    }

    protected void SetCoinsCount(int value)
    {
        GameSettings.UI.GetScreen<MainScreen>().UpdateCoins(value);
        GameSettings.UI.GetScreen<ShopScreen>().UpdateCoins(value);
        GameSettings.UI.GetScreen<LevelFailScreen>().UpdateCoins(value);
    }

    protected void MainFromFail()
    {
        level.Reset();
        GameSettings.UI.GetScreen<LevelFailScreen>().Hide();
        GameSettings.UI.GetScreen<MainScreen>().Show();
        GameSettings.UI.GetScreen<BottomButtonsScreen>().Show();        
    }

    protected virtual void RequestStartGame()
    {
        GameSettings.UI.GetScreen<MainScreen>().Hide();
        GameSettings.UI.GetScreen<BottomButtonsScreen>().Hide();
        StartGame();
    }

    protected virtual void RequestNextLevel()
    {
        NextLevel();
        GameSettings.UI.GetScreen<LevelCompleteScreen>().Hide();        
    }

    protected void RequestCoinsShop()
    {
        Analytic.Instance.SendAdsEvent("FreeCoinsAdsStart", "shop", "rewarded");
        rewardedVideo.ShowRewarded(RequestCoinsShopFinish, () => { Analytic.Instance.SendAdsEvent("FreeCoinsAdsFinishFail", "shop", "rewarded"); });
    }

    protected void RequestCoinsShopFinish(bool isShowed)
    {
        if (isShowed)
        {
            Analytic.Instance.SendAdsEvent("FreeCoinsAdsFinish", "shop", "rewarded");
            GameSettings.UI.GetScreen<ClaimRewardScreen>().ShowCoins(GameSettings.Data.gameplay.shopScreenFreeCoins);
            //GameSettings.Coins.Value += GameSettings.Data.gameplay.shopScreenFreeCoins;
        }
        else
        {
            Analytic.Instance.SendAdsEvent("FreeCoinsAdsFinishFail", "shop", "rewarded");
        }
    }

    protected void RequestCoinsAfterFail()
    {
        Analytic.Instance.SendAdsEvent("FreeCoinsAdsStart", "fail", "rewarded");
        rewardedVideo.ShowRewarded(RequestCoinsAfterFailFinish,
            () => { Analytic.Instance.SendAdsEvent("FreeCoinsAdsFinishFail", "fail", "rewarded"); });
    }

    protected void RequestCoinsAfterFailFinish(bool isShowed)
    {
        if (isShowed)
        {
            Analytic.Instance.SendAdsEvent("FreeCoinsAdsFinish", "fail", "rewarded");
            GameSettings.UI.GetScreen<ClaimRewardScreen>().ShowCoins(GameSettings.Data.gameplay.failScreenFreeCoins);
            //GameSettings.Coins.Value += GameSettings.Data.gameplay.failScreenFreeCoins;
        }
        else
        {
            Analytic.Instance.SendAdsEvent("FreeCoinsAdsFinishFail", "fail", "rewarded");
        }
    }

    protected virtual void RequestDoubleReward()
    {
        Analytic.Instance.SendAdsEvent("DoubleRewardAdsStart", "levelComplete", "rewarded");
        rewardedVideo.ShowRewarded(RequestCoinsAfterCompleteFinish,
            () => { Analytic.Instance.SendAdsEvent("DoubleRewardAdsFinishFail", "levelComplete", "rewarded"); });
    }

    protected void RequestCoinsAfterCompleteFinish(bool isShowed)
    {
        if (isShowed)
        {
            Analytic.Instance.SendAdsEvent("DoubleRewardAdsFinish", "levelComplete", "rewarded");
            GameSettings.UI.GetScreen<ClaimRewardScreen>().ShowCoins(GameSettings.Data.gameplay.completeScreenFreeCoins);
            //GameSettings.Coins.Value += GameSettings.Data.gameplay.completeScreenFreeCoins;
        }
        else
        {
            Analytic.Instance.SendAdsEvent("DoubleRewardAdsFinishFail", "levelComplete", "rewarded");
        }
    }

    protected void CancelContinueRequest()
    {
        GameSettings.UI.GetScreen<ContinueScreen>().Hide();
        if (!InNeedNewShape)
        {            
            GameSettings.UI.GetScreen<LevelFailScreen>().Show(AdsController.Instance.RewardedIsReady(rewardedVideo));
            GameSettings.UI.GetScreen<BottomButtonsScreen>().Show();
        }
        else
        {
            ShowNewShape();
        }
    }

    protected virtual void RequestRestartLevel()
    {
        if (interstitial.IsReadyToShow && restartCounter % GameSettings.Data.remote.InterstitialFrequency.Value == 0 && restartCounter >= GameSettings.Data.remote.InterstitialFrequency.Value)
        {
            interstitial.ShowInterstitial(
                () => { Analytic.Instance.SendAdsEvent("LevelFailAdsStart", "restart", "interstitial"); }, 
                () => { Analytic.Instance.SendAdsEvent("LevelFailAdsFinish", "restart", "interstitial"); });
        }
        RestartGame();
        GameSettings.UI.GetScreen<LevelFailScreen>().Hide();        
        GameSettings.UI.GetScreen<BottomButtonsScreen>().Hide();        
    }

    protected virtual void RequestContinueAfterDeath()
    {
        Analytic.Instance.SendAdsEvent("LevelContinueAdsStart", "restart", "rewarded");
        rewardedVideo.ShowRewarded(RewardedVideoFinish,
            ()=> { Analytic.Instance.SendAdsEvent("LevelContinueAdsFinishFail", "restart", "rewarded"); });             
    }

    protected void RewardedVideoFinish(bool isShowed)
    {
        Analytic.Instance.SendAdsEvent("LevelContinueAdsFinish", "restart", "rewarded");
        if (isShowed)
        {            
            ContinueGame();
            GameSettings.UI.GetScreen<GameScreen>().Show();
            GameSettings.UI.GetScreen<ContinueScreen>().ContinueSucess();
            GameSettings.UI.GetScreen<LevelFailScreen>().Hide();
        }
    }

    protected void RequestOpenShop()
    {
        level.Reset();
        GameSettings.UI.GetScreen<MainScreen>().Hide();
        GameSettings.UI.GetScreen<LevelFailScreen>().Hide();
        GameSettings.UI.GetScreen<BottomButtonsScreen>().Hide();
        GameSettings.UI.GetScreen<ShopScreen>().Show(rewardedVideo.IsReadyToShow);
    }

    protected void RequestHideShop()
    {
        GameSettings.UI.GetScreen<ShopScreen>().Hide();
        GameSettings.UI.GetScreen<BottomButtonsScreen>().Show();
        GameSettings.UI.GetScreen<MainScreen>().Show();
    }

    protected bool InNeedNewShape
    {
        get
        {
            return NextShape != null;
        }
    }

    protected ShapeType NextShape
    {
        get
        {
            ShapeType nextShape = null;
            if (GameSettings.Data.gameplay.shapeTypes.ToList().Find(s => !s.IsOpen))
            {
                ShapeType firstClosed = GameSettings.Data.gameplay.shapeTypes.ToList().First(s => !s.IsOpen);
                if (firstClosed != null)
                {
                    int firstClosedId = GameSettings.Data.gameplay.shapeTypes.ToList().IndexOf(firstClosed);
                    if (firstClosedId <= GameSettings.LevelNumber.Value - 1)
                    {
                        nextShape = firstClosed;
                    }
                }
            }
            return nextShape;
        }
    }

    protected void ShowNewShape()
    {
        GameSettings.UI.GetScreen<NewShapeScreen>().Show(NextShape);
    }

    protected void CollectShape()
    {
        GameSettings.UI.GetScreen<NewShapeScreen>().Hide();
        GameSettings.UI.GetScreen<LevelFailScreen>().Show(AdsController.Instance.RewardedIsReady(rewardedVideo));
        GameSettings.UI.GetScreen<BottomButtonsScreen>().Show();
    }

    protected virtual void RequestPause()
    {

    }
}
