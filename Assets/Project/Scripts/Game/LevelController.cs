﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TMPro;

public class LevelController : Singletone<LevelController>
{
    public float valueForLevelComplete = 66;
    [Space()]
    public float moveCircleSpeed = 1;

    public LevelNumberController levelText;

    [SerializeField]
    protected CirclePool circlePool;
    [SerializeField]
    protected EnemyPool enemyPool;

    [SerializeField]
    public SquareCalculator gameField;

    [Space()]
    public int beginEnemysCount = 2;
    public int maxBallsOnScene = 20;
    
    protected LevelState state = LevelState.None;
    public LevelState State
    {
        get
        {
            return state;
        }

        set
        {
            state = value;
            SetState(state);
        }
    }
    protected int enemyCount = 0;

    public FloatStorageData fillingValue;
    public IntStorageData ballsValue;
    public IntStorageData levelNumber
    {
        get
        {
            return GameSettings.LevelNumber;
        }
    }

    protected float fieldSquare;
    protected float currentSquare;
    protected bool isTouched = false;

    protected CircleController currentCircle;
    protected List<CircleController> circles = new List<CircleController>();
    public List<CircleController> Circles
    {
        get
        {
            return circles;
        }
    }

    protected Vector3 moveCircleTarget;
    
    public Level CurrentLevel
    {
        get
        {
            int lvlIndex = levelNumber.Value-1;
            int levelsCount = GameSettings.Data.gameplay.levels.Length;

            return GameSettings.Data.gameplay.levels[lvlIndex < levelsCount ? lvlIndex : GameSettings.Data.gameplay.levels.Length - 1];
        }
    }

    private void Awake()
    {
        fillingValue = new FloatStorageData("fillingValue", false);
        ballsValue = new IntStorageData("ballsValue", maxBallsOnScene, false);
        //levelNumber = new IntStorageData("levelNumber", 1, false);
        fillingValue.RegisterSetter(CheckFilling);
        levelNumber.RegisterSetter(levelText.UpdateLevel);
    }

    public void Reset()
    {
        ballsValue.Value = maxBallsOnScene;
        fillingValue.Value = 0;
        currentSquare = 0;
        circlePool.Reset();
        circles.Clear();
        State = LevelState.None;
        enemyPool.Reset();
    }

    private void Start()
    {
        fieldSquare = gameField.Calculate();
        levelNumber.Value = 1;
    }

    protected void SetState(LevelState newState)
    {
        switch (newState)
        {
            case LevelState.Started:
                levelText.PrepareLevel();
                break;
        }
    }

    public void PrepareLevel()
    {
        ballsValue.Value = maxBallsOnScene;
        State = LevelState.Started;
        GameColorizer.Instance.SetColor(CurrentLevel.palette);
        PrepareEnemys(CurrentLevel);
    }

    public void GameOver()
    {
        if (isTouched)
        {
            TouchFinish(Vector3.zero);
        }
        Vibrate.Play(VibrateType.Failed);
        state = LevelState.Failed;
        GameController.Instance.LevelFailed();
    }

    public void LevelComplete()
    {
        if (State != LevelState.Completed)
        {
            State = LevelState.Completed;
            TouchFinish(Vector3.zero);
            GameController.Instance.LevelCompleted();
        }
    }

    public void NextLevel()
    {
        levelNumber.Value++;
        ballsValue.Value = maxBallsOnScene;
        fillingValue.Value = 0;
        currentSquare = 0;
        circlePool.Reset();
        circles.Clear();
        State = LevelState.Started;
        GameColorizer.Instance.SetColor(CurrentLevel.palette);
        PrepareEnemys(CurrentLevel);
    }

    public void Continue()
    {
        ballsValue.Value = maxBallsOnScene;
        State = LevelState.Started;
    }

    public void RestartGame()
    {
        levelNumber.Value = 1;
        ballsValue.Value = maxBallsOnScene;
        fillingValue.Value = 0;
        currentSquare = 0;
        circlePool.Reset();
        circles.Clear();
        State = LevelState.Started;
        GameColorizer.Instance.SetColor(CurrentLevel.palette);
        PrepareEnemys(CurrentLevel);
    }

    protected void PrepareEnemys(Level l)
    {
        enemyPool.Reset();
        for (int i = 0; i < l.enemys.Length; i++)
        {
            CreateEnemy(l.enemys[i].type, l.enemys[i].count);
        }
    }

    protected void CreateEnemy(EnemyType type, int count = 1)
    {
        for (int i = 0; i < count; i++)
        {
            EnemyController enemy = enemyPool.Spawn(
                new Vector3(
                    Random.Range(gameField.topLeft.position.x, gameField.botRight.position.x),
                    Random.Range(gameField.topLeft.position.x, gameField.botRight.position.x),
                    0),
                Quaternion.identity);
            enemy.Reset();
            enemy.StartMovement();
            enemy.Init(type);
        }
    }

    private void Update()
    {
        if (fillingValue != null)
            fillingValue.Value = Mathf.Min(valueForLevelComplete, ((currentSquare + (currentCircle != null ? currentCircle.Square * GameSettings.Data.gameplay.scaleSquareFactor : 0)) / fieldSquare) * 100);
        MoveCircle();
    }

    protected void MoveCircle()
    {
        if (currentCircle != null && currentCircle.IsTouched)
        {
            Vector2 x = MinMaxX;
            Vector2 y = MinMaxY;
            moveCircleTarget.x = Mathf.Clamp(moveCircleTarget.x, x.x, x.y);
            moveCircleTarget.y = Mathf.Clamp(moveCircleTarget.y, y.x, y.y);
            currentCircle.transform.position = Vector3.Lerp(currentCircle.transform.position, moveCircleTarget, Time.deltaTime * moveCircleSpeed);
        }
    }

    public void TouchStart(Vector3 point)
    {
        if (State == LevelState.Started)
        {
            levelText.StartLevel();
            Vibrate.Play(VibrateType.Pop);
            ballsValue.Value--;
            point.z = 0;
            moveCircleTarget = point;
            currentCircle = circlePool.Spawn(point, Quaternion.identity);
            circles.Add(currentCircle);
            currentCircle.Reset();
            currentCircle.SetShape(GameSettings.Data.gameplay.RandomShape);
            currentCircle.transform.Rotate(new Vector3(0,0,UnityEngine.Random.Range(0, 360)));
            currentCircle.maxScale = gameField.Width;
            currentCircle.IsTouched = isTouched = true;
        }
    }

    public void TouchFinish(Vector3 point)
    {
        if (currentCircle != null)
        {
            Vibrate.Play(VibrateType.Pop);
            point.z = 0;
            currentSquare += currentCircle.Square * GameSettings.Data.gameplay.scaleSquareFactor;            
            currentCircle.IsTouched = isTouched = false;
            currentCircle = null;
            if(ballsValue.Value == 0 && fillingValue.Value < valueForLevelComplete)
            {
                GameOver();
            }
        }
    }

    public void TouchMove(Vector3 point)
    {
        if (State == LevelState.Started)
        {
            point.z = 0;            
            moveCircleTarget = point;
        }
    }

    protected Vector2 MinMaxY
    {
        get
        {
            if (currentCircle != null)
            {
                float r = CurrentRadius;
                return new Vector2(gameField.botRight.position.y + r, gameField.topLeft.position.y - r);
            }
            return Vector3.zero;
        }
    }

    protected Vector2 MinMaxX
    {
        get
        {
            if (currentCircle != null)
            {
                float r = CurrentRadius;
                return new Vector2(gameField.topLeft.position.x + r, gameField.botRight.position.x-r);
            }
            return Vector3.zero;
        }
    }

    protected float CurrentRadius
    {
        get
        {
            return (currentCircle.transform.localScale.x / 2.0f);// * 1.14f;
        }
    }

    protected void CheckFilling(float val)
    {
        if(val >= valueForLevelComplete)
        {
            LevelComplete();
        }
    }

    public void DestroyShape(float square)
    {
        currentSquare -= square * GameSettings.Data.gameplay.scaleSquareFactor;
    }

    public void DestroyShape(CircleController shape, float despawnDuration = 0)
    {
        DestroyShape(shape.Square);        
        this.WaitAndStart(despawnDuration, () => { circlePool.Despawn(shape); circles.Remove(shape); });
    }
}

public enum LevelState
{
    None,
    Started,
    Failed,
    Completed
}
