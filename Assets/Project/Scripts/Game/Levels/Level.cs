﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "level", menuName = "Game/Level")]
public class Level : ScriptableObject
{
    public ColorPreset palette;
    public EnemysOnLevel[] enemys;
}

[System.Serializable]
public struct EnemysOnLevel
{
    public EnemyType type;
    public int count;
}