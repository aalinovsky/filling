﻿using UnityEngine;
using System.Collections;
using TMPro;

public class LevelNumberController : MonoBehaviour
{
    public TextMeshPro levelNumLabel;
    public TextMeshPro holdToFillText;

    [Range(0,1)]
    public float alphaPrepareGame;
    [Range(0, 1)]
    public float alphaStartGame;

    public void UpdateLevel(int value)
    {
        levelNumLabel.text = value.ToString();
    }

    public void PrepareLevel()
    {
        SetAlpha(alphaPrepareGame);
    }

    public void StartLevel()
    {
        SetAlpha(alphaStartGame);
    }

    protected void SetAlpha(float newAlpha)
    {
        Color newClr = levelNumLabel.color;
        newClr.a = newAlpha;

        levelNumLabel.color = newClr;
        holdToFillText.color = newClr;
    }
}
