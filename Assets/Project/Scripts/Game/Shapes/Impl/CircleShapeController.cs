﻿using UnityEngine;
using System.Collections;

public class CircleShapeController : ShapeController
{
    public override float GetSquare()
    {
        return Mathf.PI * Mathf.Pow(transform.parent.localScale.x / 2.0f, 2);
    }
}
