﻿using UnityEngine;
using System.Collections;

public class HalfCircleShapeController : ShapeController
{
    public override float GetSquare()
    {
        return Mathf.PI * Mathf.Pow(transform.parent.localScale.x / 2.0f, 2) / 2.0f;
    }
}
