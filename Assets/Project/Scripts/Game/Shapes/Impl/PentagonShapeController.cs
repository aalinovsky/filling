﻿using UnityEngine;
using System.Collections;

public class PentagonShapeController : ShapeController
{
    public override float GetSquare()
    {
        return (5.0f / 2.0f) * Mathf.Sin(72.0f * Mathf.Deg2Rad) * Mathf.Pow(transform.parent.localScale.x / 2.0f, 2);
    }
}
