﻿using UnityEngine;
using System.Collections;

public class RectangleShapeController : ShapeController
{
    public override float GetSquare()
    {
        return Mathf.Pow(transform.parent.localScale.x, 2) / 2.0f;
    }
}
