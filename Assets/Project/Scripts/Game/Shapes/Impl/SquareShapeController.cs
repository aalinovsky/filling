﻿using UnityEngine;
using System.Collections;

public class SquareShapeController : ShapeController
{
    public override float GetSquare()
    {
        return Mathf.Pow(transform.parent.localScale.x, 2);
    }
}
