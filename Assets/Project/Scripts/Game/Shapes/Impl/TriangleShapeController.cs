﻿using UnityEngine;
using System.Collections;

public class TriangleShapeController : ShapeController
{
    public override float GetSquare()
    {
        float a = transform.parent.localScale.x;

        return (a * a) / 2.0f; 
        //return ((Mathf.Pow(transform.parent.localScale.x, 2) * Mathf.Sqrt(3.0f)) / 4.0f); 
    }
}
