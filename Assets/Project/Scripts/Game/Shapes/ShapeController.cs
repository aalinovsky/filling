﻿using UnityEngine;
using System.Collections;

public abstract class ShapeController : MonoBehaviour
{
    public CircleController parent;
    public ShapeType shapeType;
    public Collider2D collider;

    public abstract float GetSquare();

    private void OnCollisionEnter2D(Collision2D collision)
    {
        parent.Collision(collision);
    }
}
