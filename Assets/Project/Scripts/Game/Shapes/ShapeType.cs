﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "shape", menuName = "Shapes/New")]
public class ShapeType : DBObject
{
    public Shape shape;
    public LocalizedFrase label;

    public override void ClaimReward(OpenMethod method)
    {
    }

    public override string GetIdentificator()
    {
        return "Shapes";
    }
}

public enum Shape
{
    Circle,
    Rectangle,
    Triangle,
    Pentagon,
    Semicircle,
    Square
}
