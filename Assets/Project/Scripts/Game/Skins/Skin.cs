﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "skin", menuName = "Skin")]
public class Skin : DBObject
{
    public LocalizedFrase label;
    public int cost;
    public bool useCustomColor = false;
    public Color customColor;
    public Color unscaleParticles;

    public override void ClaimReward(OpenMethod method)
    {
    }

    public override string GetIdentificator()
    {
        return SecretUniqueKey;
    }
}
