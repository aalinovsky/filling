﻿using UnityEngine;
using System.Collections;

public class SkinsController : Singletone<SkinsController>
{
    public SkinsDatabase skinsDb;

    public delegate void SkinDelegate(Skin s);
    public event SkinDelegate updateSkinEvent;

    public Skin CurrentSkin
    {
        get
        {
            return skinsDb.CurrentObject;
        }

        set
        {
            skinsDb.CurrentObject = value;
            if(updateSkinEvent != null)
                updateSkinEvent(value);
        }
    }

#if UNITY_EDITOR
    public Skin testSkin;

    [DefaultUtility.Button()]
    protected void UpdateSkin()
    {
        CurrentSkin = testSkin;
    }
#endif

}
