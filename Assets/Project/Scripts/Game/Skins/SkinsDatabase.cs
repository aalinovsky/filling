﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "skinsDatabase", menuName = "SkinsDatabase")]
public class SkinsDatabase : Database<Skin>
{
    protected override string GetCurrentObjectTag()
    {
        return "SkinKey";
    }
}
