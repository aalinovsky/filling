﻿using UnityEngine;
using System.Collections;

public class SquareCalculator : MonoBehaviour
{
    public Transform topLeft;
    public Transform botRight;

    public float Calculate()
    {
        return Mathf.Abs(Width * Height);
    }

    public float Width
    {
        get
        {
            return botRight.position.x - topLeft.position.x;
        }
    }

    public float Height
    {
        get
        {
            return topLeft.position.y - botRight.position.y;
        }
    }
}
