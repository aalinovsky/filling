﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class TouchHandler : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IMoveHandler, IDragHandler
{
    [SerializeField]
    protected Camera cameraMain;
    [SerializeField]
    protected LevelController levelController;

    public void OnMove(AxisEventData eventData)
    {
        levelController.TouchMove(Point);
        DevLogs.Log("Move");
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        levelController.TouchStart(Point);
        DevLogs.Log("Touch down");
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        levelController.TouchFinish(Point);
        DevLogs.Log("touch up");
    }

    public void OnDrag(PointerEventData eventData)
    {
        levelController.TouchMove(Point);
    }

    protected Vector3 Point
    {
        get
        {
            return cameraMain.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0)); 
        }
    }
}
