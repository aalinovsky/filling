﻿using UnityEngine;
using System.Collections;

public class GameSettings : Singletone<GameSettings>
{
    public RemoteSettingsCallbacksInitializer callbacks;
    
    [SerializeField]
    public GameSettingsData settingsData;
    public static GameSettingsData Data
    {
        get
        {
            return Instance.settingsData;
        }
    }

    [SerializeField]
    protected ServiceData serviceData;
    public static ServiceData ServiceData
    {
        get
        {
            if (Instance != null)
                return Instance.serviceData;
            return null;
        }
    }

    [SerializeField]
    protected AdsData adsData;
    public static AdsData AdsData
    {
        get
        {
            if (Instance != null)
                return Instance.adsData;
            return null;
        }
    }

    [SerializeField]
    protected UIScreenController ui = null;
    public static UIScreenController UI
    {
        get
        {
            if (Instance.ui == null)
            {
                Instance.ui = FindObjectOfType<UIScreenController>();
                DevLogs.Log("Dev: GameSettings -> UI reference inited");
            }
            return Instance.ui;
        }
    }

    public static IntStorageData LevelNumber;
    public static IntStorageData TopLevelNumber;
    public static IntStorageData Coins;

    private void Awake()
    {
        LevelNumber = new IntStorageData("LevelNumber", 1, false);
        TopLevelNumber = new IntStorageData("TopLevelNumber", 1, true);

        LevelNumber.RegisterSetter(SetLevel);
        TopLevelNumber.RegisterSetter(SetTopLevel);

        Coins = new IntStorageData("Coins");
        DevLogs.Log("Dev: GameSettings -> Start initialization=============", Color.red);
        for (int i = 0; i < Data.InitableSettings.Length; i++)
        {
            Data.InitableSettings[i].Init();
        }
        callbacks.InitCallbacks();
        DevLogs.Log("Dev: GameSettings -> Stop initialization==============", Color.red);
    }

    protected void SetTopLevel(int value)
    {
        ServiceController.Instance.SetTop(value);
    }

    protected void SetLevel(int value)
    {
        if(TopLevelNumber.Value < value)
        {
            TopLevelNumber.Value = value;
        }
    }
}