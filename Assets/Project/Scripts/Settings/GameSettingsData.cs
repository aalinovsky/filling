﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;

[CreateAssetMenu(fileName = "Settings", menuName = "Game/Settings/General")]
[ExecuteInEditMode]
public class GameSettingsData : ScriptableObject
{
    private InitableSettings[] _initableSettings = null;
    public InitableSettings[] InitableSettings
    {
        get
        {
            _initableSettings = new InitableSettings[] { gameplay, debug, perfomance, ui, remote };
            return _initableSettings;
        }
    }

    public DebugSettings debug;
    public PerfomanceSettings perfomance;
    public GamePlaySettings gameplay;
    public UISettings ui;
    public RemoteSettings remote;
}
