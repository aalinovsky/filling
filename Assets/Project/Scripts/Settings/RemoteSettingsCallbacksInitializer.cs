﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;
using System;

public class RemoteSettingsCallbacksInitializer : MonoBehaviour
{
    public TextMeshProUGUI configVersion;

    public void InitCallbacks()
    {        
        GameSettings.Data.remote.ConfigVersion.RegisterSetValueCallback(UpdateConfigVersion);
        DevLogs.Log("Remote config set callback added " + GameSettings.Data.remote.ConfigVersion.Value, Color.green);
        UpdateConfigVersion();
    }

    protected void UpdateConfigVersion()
    {
        DevLogs.Log("Remote config seted " + GameSettings.Data.remote.ConfigVersion.Value, Color.green);
        if(configVersion != null)
            configVersion.text = GameSettings.Data.remote.ConfigVersion.Value;
        Canvas.ForceUpdateCanvases();        
    }

    protected void Updat(string s)
    {
        DevLogs.Log("Remote config seted by seter to " + GameSettings.Data.remote.ConfigVersion.Value, Color.green);
        StartCoroutine(WaitEndOfFrame(UpdateConfigVersion));
    }

    IEnumerator WaitEndOfFrame(Action act)
    {
        yield return new WaitForEndOfFrame();
        act();
    }
}
