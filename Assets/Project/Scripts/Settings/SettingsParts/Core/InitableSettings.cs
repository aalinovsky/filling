﻿using UnityEngine;

public abstract class InitableSettings : ScriptableObject
{
    public abstract void Init();
}
