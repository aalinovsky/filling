﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "Dbugettings", menuName = "Game/Settings/Debug")]
public class DebugSettings : InitableSettings
{
    public bool isDevLogs = false;

    public override void Init()
    {
        DevLogs.Log("Dev : init DebugSettings", Color.yellow);
    }
}
