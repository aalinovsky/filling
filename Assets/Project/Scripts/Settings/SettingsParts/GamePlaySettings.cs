﻿using UnityEngine;
using System.Collections;
using DefaultUtility;
using System.Linq;

[CreateAssetMenu(fileName = "GameplaySettings", menuName = "Game/Settings/GamePlaySettings")]
public class GamePlaySettings : InitableSettings
{    
    [Space()]
    public Shape defaultShape;
    public bool randomShape = false;
    public ShapeType[] shapeTypes;
    [Space()]
    //public int boostedEnemyAfterLevel = 5;
    //public EnemyBoostType boost = EnemyBoostType.Scale;
    public float fillingDuration = 6;
    [Range(1.0f, 2.0f)]
    public float scaleSquareFactor = 1.1f;

    public Level[] levels;

    //public Vector2 enemySpeed;
    public float enemySpeedDelta;
    [Header("Enemy Bomb")]
    public Color explosionColor;
    public float coolingDownSpeed = 0.001f;
    public float heatSpeed = 0.1f;
    public float enemyExplosionRange;

    [Header("Fill sound")]
    public string fillSound = "filling";
    public Vector2 fillSoundPitch;
    [Header("Rewards")]
    public int failScreenFreeCoins = 30;
    public int shopScreenFreeCoins = 30;
    public int completeScreenFreeCoins = 50;
    public int coinsForBigBall = 2;
    [Header("Shop")]
    public int randomSkinCost;
    [Range(0.04f,1)]
    public float bigBallSquarePercents = 0.25f;
    [Header("Missions")]
    public bool enableMissions = false;

    //public float EnemySpeed
    //{
    //    get
    //    {
    //        return Random.Range(enemySpeed.x, enemySpeed.y);
    //    }
    //}

    public Shape RandomShape
    {
        get
        {
           
            return randomShape ? shapeTypes.ToList().FindAll(s => s.IsOpen).RandomObject().shape : defaultShape;
        }
    }

    public override void Init()
    {
        DevLogs.Log("Dev : init GamePlaySettings", Color.yellow);
        if (enableMissions)
        {
            MissionsController.Instance.CheckMissionsState();
        }
    }
}
