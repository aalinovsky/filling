﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Camera))]
public class CameraClippingPerformance : MonoBehaviour
{
    [SerializeField]
    protected Camera cam;

    protected float deltaDistance = 0;
    public float DeltaClippingDistance
    {
        get
        {
            return deltaDistance;
        }
    }
    protected int step;

    void Start()
    {
        cam = GetComponent<Camera>();
    }

    public void SetDistance()
    {
        if(cam.farClipPlane > GameSettings.Data.perfomance.minMaxClippingDistance.x)
        {
            step++;
            cam.farClipPlane = Mathf.Clamp(GameSettings.Data.perfomance.minMaxClippingDistance.y - step * GameSettings.Data.perfomance.decreeseClippingDistance, GameSettings.Data.perfomance.minMaxClippingDistance.x, GameSettings.Data.perfomance.minMaxClippingDistance.y);
            deltaDistance = GameSettings.Data.perfomance.minMaxClippingDistance.y - cam.farClipPlane;            
        }
    }
}
