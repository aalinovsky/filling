﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Light))]
public class DirectionLightPerformance : MonoBehaviour
{
    public Light direction;

    private void Start()
    {
        if (direction == null)
            direction = GetComponent<Light>();
    }

    public void UpdateQuality()
    {
        switch (direction.shadowResolution)
        {
            case UnityEngine.Rendering.LightShadowResolution.VeryHigh:
                direction.shadowResolution = UnityEngine.Rendering.LightShadowResolution.High;
                break;
            case UnityEngine.Rendering.LightShadowResolution.High:
                direction.shadowResolution = UnityEngine.Rendering.LightShadowResolution.Medium;
                break;
            case UnityEngine.Rendering.LightShadowResolution.Medium:
                direction.shadowResolution = UnityEngine.Rendering.LightShadowResolution.Low;
                break;
            case UnityEngine.Rendering.LightShadowResolution.Low:
                direction.shadows = LightShadows.None;
                break;
        }
    }
}
