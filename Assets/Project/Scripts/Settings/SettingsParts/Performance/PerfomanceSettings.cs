﻿using UnityEngine;

[CreateAssetMenu(fileName = "PerfomanceSettings", menuName = "Game/Settings/Perfomance")]
public class PerfomanceSettings : InitableSettings
{
    public DepthTextureMode depthTexture;
    [Range(0, 4)]
    public int vSync = 0;
    public bool isEnableFog = true;
    [Space()]
    public int targetFPSIOS = 60;
    public int targetFPSAndroid = 60;
    public int targetFPSDesktop = 60;

    [Range(0, 1)]
    public float resolutionScale = 1;
    public bool enabledAutoResolutionScale = true;
    [Range(0, 1)]
    public float minResolutionScale = 1;
    [Range(0, 1)]
    public float decreeeResolutionScaleSteps = 0.2f;
    [Space()]
    public float delayBeforeStartCheckFPS = 4;
    [Header("Сколько измеряем фпс для определения среднего значения")]
    public float measurementsDuration = 10;
    [Header("Сколько предупреждений подряд должно произойти для уменьшения разрешения")]
    public int warningsToDecreeseCount = 1;
    public float delayBetweenMeasurements = 3;
    public int maxFPSDelta = 8;
    [Header("Camera ClippingPlane settings")]
    public Vector2 minMaxClippingDistance;
    public float decreeseClippingDistance;
    [Space()]
    public RerformanceUp[] performanceInstructions;
    public PerformancePriorityInstructions[] fpsInstructions = new PerformancePriorityInstructions[] {
        new PerformancePriorityInstructions() { priority = PerformancePriority.Critical },
        new PerformancePriorityInstructions() { priority = PerformancePriority.High },
        new PerformancePriorityInstructions() { priority = PerformancePriority.Midium },
        new PerformancePriorityInstructions() { priority = PerformancePriority.Default }
    };

    public int TargetFPS
    {
        get
        {
#if UNITY_EDITOR
            return targetFPSDesktop;
#elif UNITY_ANDROID
            return targetFPSAndroid;
#elif UNITY_IOS
            return targetFPSIOS;
#endif
        }
    }

    public int MinFPS
    {
        get
        {
            return TargetFPS - maxFPSDelta;
        }
    }

    public float CurrentResolutionScale
    {
        get
        {
            return currentResolutionScale;
        }
    }

    protected float currentResolutionScale;
    protected Resolution deviceResolution;

    public override void Init()
    {
        DevLogs.Log("Dev : init PerfomanceSettings", Color.yellow);
        if (Application.targetFrameRate != TargetFPS)
        {
            Application.targetFrameRate = TargetFPS;
        }
        deviceResolution = Screen.currentResolution;

        currentResolutionScale = resolutionScale;
        SetResolutionScale(resolutionScale);

        QualitySettings.vSyncCount = vSync;
        RenderSettings.fog = isEnableFog;
        Camera.main.depthTextureMode = depthTexture;
    }

    public void DecreeseResolution()
    {
        if (currentResolutionScale > minResolutionScale)
        {
            currentResolutionScale -= decreeeResolutionScaleSteps;
            SetResolutionScale(currentResolutionScale);
        }
    }

    public void SetResolutionScale(float scale)
    {
        scale = Mathf.Clamp(scale, minResolutionScale, 1);

        Resolution screenSize = deviceResolution;
        screenSize.height = (int)(screenSize.height * scale);
        screenSize.width = (int)(screenSize.width * scale);
        Screen.SetResolution(screenSize.width, screenSize.height, true);

        currentResolutionScale = scale;
    }

    public PerformancePriorityInstructions GetPriority(int fps)
    {
        for (int i = 0; i < fpsInstructions.Length; i++)
        {
            if(fps <= fpsInstructions[i].fps)
            {
                return fpsInstructions[i];
            }
        }

        return fpsInstructions[0];
    }
}

public enum RerformanceUp
{
    ScaleResolution,
    Lighting,
    ClippingPlane
}

public enum PerformancePriority
{
    Default,
    Midium,
    High,
    Critical
}

[System.Serializable]
public class PerformancePriorityInstructions
{
    public int fps;
    public int decreeseSteps;
    public PerformancePriority priority;
}