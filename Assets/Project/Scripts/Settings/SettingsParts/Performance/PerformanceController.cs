﻿using UnityEngine;
using System.Collections;

public class PerformanceController : MonoBehaviour
{
    public PerfomanceSettings settings;
    [Space()]
    public DirectionLightPerformance lighting;
    public CameraClippingPerformance cameraQuality;

    protected bool isEnabled = false;
    public bool EnabledPerformanceController
    {
        get
        {
            return isEnabled;
        }

        set
        {
            if (isEnabled != value)
            {
                isEnabled = value;
                if (isEnabled)
                {
                    StartController();
                }
                else
                {
                    StopController();
                }
            }
        }
    }

    protected IEnumerator fpsCheckerRoutine = null;
    protected int warningsCount = 0;
    protected int stepNumber = 0;

    public void Start()
    {
        EnabledPerformanceController = settings.enabledAutoResolutionScale;
    }

    protected void StartController()
    {
        StopController();
        fpsCheckerRoutine = PerformanceControlRoutine();
        StartCoroutine(fpsCheckerRoutine);
    }

    protected void StopController()
    {
        if(fpsCheckerRoutine != null)
        {
            StopCoroutine(fpsCheckerRoutine);
            fpsCheckerRoutine = null;
            warningsCount = 0;
        }
    }

    protected IEnumerator PerformanceControlRoutine()
    {
        yield return new WaitForSeconds(settings.delayBeforeStartCheckFPS);

        while (gameObject.activeInHierarchy)
        {
            int resultFps = 0;
            float time = 0;
            float steps = 0;

            while(settings.measurementsDuration > time)
            {
                time += Time.deltaTime;
                steps += 1.0f;
                yield return null;
            }

            resultFps = (int)(steps / time);

            if (resultFps <= settings.MinFPS)
            {
                if (warningsCount >= settings.warningsToDecreeseCount)
                {
                    if (stepNumber == 0)
                    {
                        StepForUpPerformance(settings.GetPriority(resultFps));
                    }
                    else
                    {
                        NextPerformanceStep();
                    }
                    warningsCount = 0;
                }
                else
                {
                    warningsCount++;
                }
            }
            else
            {
                warningsCount = 0;
            }

            DevLogs.Log(string.Format("PERFORMANCE: FPS:{0}, ResolutionScale:{1}, Warnings:{2}, Step {3}", resultFps, settings.CurrentResolutionScale, warningsCount, stepNumber), Color.green);
            yield return new WaitForSeconds(settings.delayBetweenMeasurements);
        }
    }

    protected void StepForUpPerformance(PerformancePriorityInstructions instructions)
    {
        for (int i = 0; i < instructions.decreeseSteps; i++)
        {
            NextPerformanceStep();
        }
    }

    protected void NextPerformanceStep()
    {
        if (settings.performanceInstructions.Length > stepNumber)
        {
            switch (settings.performanceInstructions[stepNumber])
            {
                case RerformanceUp.ScaleResolution:
                    ResolutionScale();
                    break;
                case RerformanceUp.Lighting:
                    ShadowsQuality();
                    break;
                case RerformanceUp.ClippingPlane:
                    ClippingPlaneDistance();
                    break;
            }
        }
        stepNumber++;
    }

    protected void ResolutionScale()
    {
        settings.DecreeseResolution();
    }

    protected void ShadowsQuality()
    {
        lighting.UpdateQuality();
    }

    protected void ClippingPlaneDistance()
    {
        cameraQuality.SetDistance();
    }
}
