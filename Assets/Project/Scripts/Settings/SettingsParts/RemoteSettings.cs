﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "RemoteSettings", menuName = "Game/Settings/Remote")]
public class RemoteSettings : InitableSettings
{
    [SerializeField]
    private string configVersion = "0.0";
    [SerializeField]
    private bool adsEnables = true;
    [SerializeField]
    private int interstitialFrequency = 4;
    [SerializeField]
    private float enemySpeed = 3.2f;

    public StringStorageData ConfigVersion;
    public BoolStorageData AdsEnabled;
    public IntStorageData InterstitialFrequency;
    public FloatStorageData EnemySpeed;

    public override void Init()
    {
        ConfigVersion = new StringStorageData("ConfigVersion", true, configVersion);
        AdsEnabled = new BoolStorageData("AdsEnabled", true, adsEnables);
        InterstitialFrequency = new IntStorageData("InterstitialFrequency", interstitialFrequency, true);
        EnemySpeed = new FloatStorageData("EnemySpeed", true, enemySpeed);
    }
}
