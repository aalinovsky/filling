﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "UISettings", menuName = "Game/Settings/UI")]
public class UISettings : InitableSettings
{
    public bool multitouch = false;
    public float hideRewardForBigBallDuration = 1.5f;

    public override void Init()
    {
        DevLogs.Log("Dev : init UISettings", Color.yellow);
        Input.multiTouchEnabled = multitouch;
        GameSettings.UI.Init();
    }
}

public enum GlobalColorSetMethod
{
    Random,
    Linear
}

public enum GlobalColorSetMode
{
    LevelComplete,
    Death
}
