﻿using UnityEngine;
using System.Collections;

public static class Tags
{
    public const string CIRCLE = "circle";
    public const string ENEMY = "enemy";
    public const string WALLS = "walls";
}
