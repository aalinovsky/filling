﻿using UnityEngine;

[RequireComponent(typeof(Animator))]
public abstract class AbstractScreen : MonoBehaviour, IScreen
{
    public Animator Animator;

    protected bool isInited = false;

    protected bool isShow = false;
    public bool IsShow
    {
        get
        {
            return isShow;
        }
    }

    private void Awake()
    {
        UIAwake();
    }

    protected virtual void UIAwake()
    {
        Animator = GetComponent<Animator>();
    }

    private void Start()
    {
        UIStart();
    }

    protected virtual void UIStart()
    {

    }

    public abstract void Show();

    public abstract void Hide();

    public virtual void InitButtons()
    {
        if (isInited)
            return;
        isInited = true;
    }

}
