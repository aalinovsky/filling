﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public abstract class AbstractUIScreen : MonoBehaviour
{
    protected List<CachedTransitionScreen> screens = new List<CachedTransitionScreen>();
    
    private void Awake()
    {
        
    }

    public virtual void Init()
    {
        FindAllPages();
    }

    protected void FindAllPages()
    {
        screens.Clear();
        foreach (Transform t in transform)
        {
            CachedTransitionScreen screen = t.GetComponent<CachedTransitionScreen>();
            if (screen != null)
            {
                screens.Add(screen);                
            }
        }
    }

    public CachedTransitionScreen GetTransitionScreen(Type t)
    {
        foreach (CachedTransitionScreen abstractS in screens)
        {
            if (abstractS.GetType() == t)
                return abstractS;
        }
        return null;
    }

    public T GetScreen<T>() where T : CachedTransitionScreen
    {
        return (T)GetTransitionScreen(typeof(T));
    }

    public void Show<T>() where T : CachedTransitionScreen
    {
        GetScreen<T>().Show();
    }

    public void Hide<T>() where T : CachedTransitionScreen
    {
        GetScreen<T>().Hide();
    }
}
