﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class AbstractUIScreenController : AbstractUIScreen
{
    protected CachedTransitionScreen currentScreen;
    public CachedTransitionScreen CurrentScreen
    {
        get
        {
            return currentScreen;
        }

        set
        {
            currentScreen = value;
            currentScreen.BecomeCurrentScreen();
        }
    }

    public static T Screen<T>() where T : CachedTransitionScreen
    {
        return GameSettings.UI.GetScreen<T>();
    }    
}
