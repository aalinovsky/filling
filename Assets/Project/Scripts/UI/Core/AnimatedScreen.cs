﻿using UnityEngine;

public class AnimatedScreen : AbstractScreen
{
    public AnimationClip hideClip;

    protected string showParameter = "";
    private float hideTime = 0;

    public override void Hide()
    {
        if (!isShow)
            return;

        
        if (showParameter != "")
        {
            if (hideTime != 0)
            {
                this.WaitAndStart(hideTime, () => {
                    gameObject.SetActive(false);
                    isShow = false;
                });
            }
            else
            {
                isShow = false;
                Animator.SetBool(showParameter, false);
            }           
        }
        else
        {
            isShow = false;
            gameObject.SetActive(false);
        }
    }

    public override void Show()
    {
        if (isShow)
            return;

        isShow = true;
        gameObject.SetActive(true);
        if (showParameter != "")
        {
            Animator.SetBool(showParameter, true);
        }
    }

    protected override void UIAwake()
    {
        base.UIAwake();
        InitAnimatorParameters();
    }

    protected virtual void InitAnimatorParameters()
    {
        if(Animator.parameterCount > 0)
            showParameter = Animator.GetParameter(0).name;

        if (hideClip != null)
            hideTime = hideClip.length;
    }
}
