﻿using System;
using UnityEngine.UI;

[System.Serializable]
public class ButtonAndAction
{
    public Button btn;
    protected Action action;

    public void RegisterAction(Action a)
    {
        if (btn != null)
        {
            btn.onClick.RemoveListener(ClickAction);
            btn.onClick.AddListener(ClickAction);
        }

        action = a;
    }

    public void UnregisterAction()
    {
        action = null;
    }

    protected void ClickAction()
    {
        if (action != null)
        {
            action();
        }
    }
}