﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class CachedTransitionScreen : AnimatedScreen
{
    [Header("Buttons")]
    public ButtonAndAction closeBtn;

    protected CachedTransitionScreen receiverScreen = null;

    protected override void UIAwake()
    {
        base.UIAwake();
        InitButtons();
    }

    public virtual void Show(CachedTransitionScreen receiver)
    {
        receiverScreen = receiver;
        ShowRule(receiverScreen);
    }

    
    protected abstract void ShowRule(CachedTransitionScreen prevScreen);
    protected virtual void BackRule()
    {
        if (receiverScreen != null)
        {
            if (!receiverScreen.IsShow)
            {
                receiverScreen.Show(this);
            }
            GameSettings.UI.CurrentScreen = receiverScreen;            
        }
        Hide();
    }

    public virtual void Back()
    {
        BackRule();
        SoundManager.PlaySound("ClickButton");
    }

    public virtual void BecomeCurrentScreen()
    {

    }
}
