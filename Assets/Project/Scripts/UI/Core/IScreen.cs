﻿using UnityEngine;
using System.Collections;

public interface IScreen
{
    void Show();
    void Hide();
}
