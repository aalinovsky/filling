﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ImagesIconSetter : MonoBehaviour
{
    public Image[] images;
    public Sprite icon;

    [DefaultUtility.Button]
    public void UpdateIcon()
    {
        foreach (var item in images)
        {
            item.sprite = icon;
        }
    }
}
