﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ProgressController : MonoBehaviour
{
    public bool instanceSetValue = false;
    public float speed;
    
    public Text percents;
    public Image progressbar;
    public Image arrow;

    //public bool useColors = false;
    //public Color start;
    //public Color end;

    [SerializeField]
    protected float value;
    protected float progressWidth;
    protected float progressStartXPos;

    protected float screenDlt;

    private void Start()
    {
        screenDlt = Screen.width / 1080.0f;

        progressWidth = progressbar.rectTransform.sizeDelta.x * screenDlt;
        progressStartXPos = progressbar.rectTransform.position.x - progressWidth / 2.0f;
    }

    public void Reset()
    {        
        MyReset();
    }

    public virtual void MyReset()
    {
        value = 0;
        if (progressbar != null)
            progressbar.fillAmount = value;
        SetPercents(value);
        MoveArrow(value);
    }

    public virtual void UpdatePercents(float newVal)
    {
        if (instanceSetValue)
        {
            MoveArrow(newVal);
        }
        else
        {
            value = newVal;
        }
    }

    protected void MoveArrow(float f)
    {
        if (arrow != null)
        {
            Vector3 newPos = arrow.rectTransform.position;
            newPos.x = progressStartXPos + progressWidth * f;
            arrow.rectTransform.position = newPos;
        }
    }

    protected void SetPercents(float f)
    {
        if (percents != null)
        {
            int i = (int)(f * 100);
            percents.text = string.Format("{0}%", i);
        }
    }

    void Update()
    {
        if (!enabled)
            return;

        if (value != progressbar.fillAmount || value == 0)
        {
            if(progressbar != null)
                progressbar.fillAmount = Mathf.MoveTowards(progressbar.fillAmount, value, Time.deltaTime * speed);
            SetPercents(progressbar.fillAmount);
            MoveArrow(progressbar.fillAmount);
        }
    }
}
