﻿using UnityEngine;
using System.Collections;

public class BottomButtonsScreen : CachedTransitionScreen
{
    public ButtonAndAction rate;
    public ButtonAndAction settings;
    public ButtonAndAction shop;
    public ButtonAndAction share;
    public ButtonAndAction top;

    protected override void ShowRule(CachedTransitionScreen prevScreen)
    {
    }
}
