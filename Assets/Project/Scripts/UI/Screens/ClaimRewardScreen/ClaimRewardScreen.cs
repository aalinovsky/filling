﻿using UnityEngine;
using System.Collections;
using TMPro;

public class ClaimRewardScreen : CachedTransitionScreen
{
    public GameObject coinsPanel;
    public TextMeshProUGUI coinsCountLabel;

    protected override void ShowRule(CachedTransitionScreen prevScreen)
    {
        throw new System.NotImplementedException();
    }

    public void ShowCoins(int coinsCount)
    {
        coinsPanel.SetActive(true);
        coinsCountLabel.text = string.Format("+{0}", coinsCount);
        closeBtn.RegisterAction(()=> { ClaimCoins(coinsCount); });
        Show();
    }

    protected void ClaimCoins(int count)
    {
        GameSettings.Coins.Value += count;
        Hide();
    }
}
