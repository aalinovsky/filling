﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using TMPro;

public class ContinueScreen : CachedTransitionScreen
{
    public LevelProgressPanel progress;
    public float continueDuration = 5;
    public float cancelActiveAfter = 2;
    //public float cancelActivateDuration = 2;
    //public TextMeshProUGUI cancelContinueLabel;
    [SerializeField, Space()]
    protected Button continueButton;
    public Image continueImg;
    public TextMeshProUGUI continueLabel;

    IEnumerator continueRoutine;
    IEnumerator cancelContinueRoutine;

    public event Action cancelAction;
    public event Action continueAction;

    #region Init_region

    public override void InitButtons()
    {
        if (isInited)
            return;
        closeBtn.RegisterAction(Cancel);
        if (continueButton != null)
            continueButton.onClick.AddListener(Continue);
        isInited = true;
    }

    protected override void ShowRule(CachedTransitionScreen prevScreen)
    {
    }
    #endregion

    protected void Cancel()
    {
        if (cancelAction != null)
        {
            Stoproutines();
            cancelAction();
        }
    }

    protected void Continue()
    {
        if (continueAction != null)
        {
            Stoproutines();
            continueAction();
        }
    }

    protected void Stoproutines()
    {
        if (continueRoutine != null)
        {
            StopCoroutine(continueRoutine);
        }
        if (cancelContinueRoutine != null)
        {
            StopCoroutine(cancelContinueRoutine);
        }
    }

    public void ContinueSucess()
    {
        Hide();
    }

    public override void Show()
    {
        Stoproutines();
        closeBtn.btn.gameObject.SetActive(false);
        base.Show();
        continueRoutine = this.StartLerp(continueDuration, ContinueLerp,Cancel);
        cancelContinueRoutine = this.WaitAndStart(cancelActiveAfter, ()=> { closeBtn.btn.gameObject.SetActive(true); }); 
    }

    protected void ContinueLerp(float val)
    {
        continueImg.fillAmount = 1.0f - val;
        continueLabel.text = ((int)(1 + (1.0f-val) * continueDuration)).ToString();
    }

    public override void Hide()
    {
        base.Hide();
    }
}
