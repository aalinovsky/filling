﻿using UnityEngine;
using System.Collections;
using TMPro;
using UnityEngine.UI;
using System;

public class GameScreen : CachedTransitionScreen
{
    [SerializeField]
    protected Button pauseButton;

    public event Action pauseAction;

    public RewardElementPool rewardsPool;

    [Space()]
    public ProgressController progress;
    public TextMeshProUGUI currentLevel;
    public TextMeshProUGUI nextLevel;

    [Space()]
    public TextMeshProUGUI fillingLabel;
    public TextMeshProUGUI ballsLabel;

    public override void InitButtons()
    {
        if (isInited)
            return;
        if (pauseButton != null)
            pauseButton.onClick.AddListener(Pause);
        isInited = true;
    }


    protected override void UIStart()
    {
        base.UIStart();
    }

    protected override void BackRule()
    {
    }

    protected override void ShowRule(CachedTransitionScreen prevScreen)
    {
        throw new System.NotImplementedException();
    }

    protected override void InitAnimatorParameters()
    {
        base.InitAnimatorParameters();
    }

    public override void Show()
    {
        rewardsPool.Reset();
        base.Show();        
    }

    public void InitLevel(int currentLevel)
    {
        if(this.currentLevel != null)
            this.currentLevel.text = currentLevel.ToString();
        if (this.nextLevel != null)
            this.nextLevel.text = (currentLevel+1).ToString();
        if (progress != null)
            progress.Reset();
    }
    public void SetProgress(float val)
    {
        if(progress != null)
            progress.UpdatePercents(val);
    }

    protected void Pause()
    {
        if (pauseAction != null)
            pauseAction();
    }

    public void SetFilling(float value)
    {
        if (fillingLabel != null)
            fillingLabel.text = string.Format("{0:0.0}%", value);
    }

    public void SetBallsCount(int i)
    {
        if (ballsLabel != null)
            ballsLabel.text = string.Format("{0}", i);
    }

    public void DrawRewardForBigBall(Vector3 ballPosition)
    {
        RewardElement re = rewardsPool.Spawn(Camera.main.WorldToScreenPoint(ballPosition), Quaternion.identity);

        re.Show(GameSettings.Data.gameplay.coinsForBigBall, GameSettings.Data.ui.hideRewardForBigBallDuration, rewardsPool);
        GameSettings.Coins.Value += GameSettings.Data.gameplay.coinsForBigBall;
        SoundManager.PlaySound("Coin");
    }
}