﻿using UnityEngine;
using System.Collections;
using TMPro;

public class RewardElement : MonoBehaviour
{
    public CanvasGroup canvasGr;
    public TextMeshProUGUI textReward;
    public AnimationCurve hideCurve;

    protected RewardElementPool myPool;

    public void Show(int reward, float hideDuration, RewardElementPool pool)
    {
        myPool = pool;
        textReward.text = string.Format("+{0}",reward);
        textReward.color = GameColorizer.Instance.CurrentPreset.enemy;

        this.StartLerp(hideDuration, LerpAction, StopLerpAction);
    }

    protected void LerpAction(float val)
    {
        canvasGr.alpha = hideCurve.Evaluate(val);
    }

    protected void StopLerpAction()
    {
        myPool.Despawn(this);
        gameObject.SetActive(false);
    }
}
