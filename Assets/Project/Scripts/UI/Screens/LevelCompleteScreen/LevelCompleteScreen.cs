﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LevelCompleteScreen : CachedTransitionScreen
{
    [SerializeField]
    protected Button nextLevelButton;
    [SerializeField]
    protected Button doubleRewardButton;
    public TextMeshProUGUI freeCoinsLabel;
    [Space()]
    public GameObject confettiEmitter;
    public event Action nextLevelAction;
    public event Action doubleRewardAction;

    #region Init_region

    public override void InitButtons()
    {
        if (isInited)
            return;
        nextLevelButton.onClick.AddListener(NextLevel);
        if(doubleRewardButton != null)
            doubleRewardButton.onClick.AddListener(DoubleReward);
        isInited = true;
    }

    protected override void ShowRule(CachedTransitionScreen prevScreen)
    {
    }
    #endregion

    protected void NextLevel()
    {
        if (nextLevelAction != null)
            nextLevelAction();
    }

    protected void DoubleReward()
    {
        if (doubleRewardAction != null)
        {
            doubleRewardButton.gameObject.SetActive(false);
            doubleRewardAction();
        }
    }

    public void Show(bool adsLoaded)
    {
        freeCoinsLabel.text = GameController.Instance.freeCoinsFrase.GetValue(GameSettings.Data.gameplay.completeScreenFreeCoins);
        doubleRewardButton.gameObject.SetActive(adsLoaded);
        Show();
    }

    public override void Show()
    {
        if(ServiceController.RateState && !GameSettings.ServiceData.IsShowRate && GameSettings.ServiceData.showRateDialogOnLevel == GameSettings.LevelNumber.Value)
        {
            GameSettings.UI.GetScreen<RateScreen>().Show();
        }
        confettiEmitter.SetActive(true);
        base.Show();
    }

    public override void Hide()
    {
        base.Hide();
        confettiEmitter.SetActive(false);
    }
}
