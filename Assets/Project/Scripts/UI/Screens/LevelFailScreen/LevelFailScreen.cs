﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using TMPro;

public class LevelFailScreen : CachedTransitionScreen
{
    public TextMeshProUGUI coinsLabel;
    public LevelProgressPanel progress;
    [SerializeField]
    protected Button homeButton;
    public event Action homeAction;

    [SerializeField]
    protected Button againButton;
    [SerializeField]
    protected Button freeCoinsButton;
    public TextMeshProUGUI freeCoinsLabel;

    public event Action againAction;
    public event Action freeCoinsAction;
    
    #region Init_region

    public override void InitButtons()
    {
        if (isInited)
            return;
        againButton.onClick.AddListener(Again);

        if (homeButton != null)
            homeButton.onClick.AddListener(Home);

        if (freeCoinsButton != null)
            freeCoinsButton.onClick.AddListener(FreeCoins);
        
        isInited = true;
    }

    protected override void ShowRule(CachedTransitionScreen prevScreen)
    {
    }
    #endregion

    public void UpdateCoins(int coins)
    {
        if (coinsLabel != null)
            coinsLabel.text = coins.ToString();
    }


    protected void Again()
    {
        if (againAction != null)
            againAction();
    }

    protected void FreeCoins()
    {
        if (freeCoinsAction != null)
        {
            freeCoinsButton.gameObject.SetActive(false);
            freeCoinsAction();
        }
    }

    public void Show(bool isLoadedReward)
    {
        Debug.Log("rewarded is loaded : " + isLoadedReward.ToString());
        freeCoinsLabel.text = GameController.Instance.freeCoinsFrase.GetValue(GameSettings.Data.gameplay.failScreenFreeCoins);
        freeCoinsButton.gameObject.SetActive(isLoadedReward);
        Show();
    }

    protected void Home()
    {
        if (homeAction != null)
            homeAction();
    }
}
