﻿using UnityEngine;
using System.Collections;
using TMPro;

public class LevelProgressPanel : MonoBehaviour
{
    public TextMeshProUGUI level;
    public LocalizedFrase levelFrase;
    public TextMeshProUGUI percents;

    public void UpdateValues(float value)
    {
        if (percents != null)
            percents.text = string.Format("{0:0.0}%", value);

        if(level != null)
        {
            level.text = string.Format("{1} {0}", GameSettings.LevelNumber.Value, levelFrase.Value);
        }
    }
}
