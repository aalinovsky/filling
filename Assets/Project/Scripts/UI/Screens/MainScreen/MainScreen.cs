﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using TMPro;

public class MainScreen : CachedTransitionScreen
{
    public TextMeshProUGUI bestLabel;
    public LocalizedFrase bestLevelFrase;
    public TextMeshProUGUI[] coinsLabel;
    [SerializeField]
    protected Button startGameButton;
    public event Action startAction;
    
    #region Init_region

    public override void InitButtons()
    {
        if (isInited)
            return;
        startGameButton.onClick.AddListener(StartGame);
        
        isInited = true;
    }
    
    protected override void ShowRule(CachedTransitionScreen prevScreen)
    {
    }
    #endregion

    public void UpdateCoins(int coins)
    {
        for (int i = 0; i < coinsLabel.Length; i++)
        {
            if (coinsLabel[i] != null)
                coinsLabel[i].text = coins.ToString();
        }
    }

    protected void StartGame()
    {
        if (startAction != null)
            startAction();
    }

    public override void Show()
    {
        UpdateBestLevel(GameSettings.TopLevelNumber.Value);
        base.Show();
    }

    protected void UpdateBestLevel(int lvl)
    {
        if(bestLabel != null)
        {
            bestLabel.text = string.Format("{1}: {0}", lvl, bestLevelFrase.Value);
        }
    }
}
