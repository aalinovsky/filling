﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;

public class NewShapeScreen : CachedTransitionScreen
{
    public Image newShapeIcon;
    public TextMeshProUGUI newShapeLabel;
    public GameObject confettiEmitter;

    protected override void ShowRule(CachedTransitionScreen prevScreen)
    {
        throw new System.NotImplementedException();
    }

    public override void InitButtons()
    {
        base.InitButtons();
    }
    
    public void Show(ShapeType newShape)
    {
        newShape.IsOpen = true; 
        newShapeIcon.sprite = newShape.icon;
        if(newShape.label != null)
            newShapeLabel.text = newShape.label.Value;

        Show();
    }

    public override void Show()
    {
        confettiEmitter.SetActive(true);
        base.Show();
    }

    public override void Hide()
    {
        confettiEmitter.SetActive(false);
        base.Hide();
    }
}
