﻿using UnityEngine;
using System.Collections;

public class RateScreen : CachedTransitionScreen
{
    public ButtonAndAction rate;
    public ButtonAndAction later;
    public ButtonAndAction newer;

    protected override void ShowRule(CachedTransitionScreen prevScreen)
    {
        throw new System.NotImplementedException();
    }

    public override void InitButtons()
    {
        base.InitButtons();
        rate.RegisterAction(Rate);
        later.RegisterAction(Later);
        newer.RegisterAction(Newer);
        isInited = true;
    }

    protected void Rate()
    {
        ServiceController.RateState = true;
        Analytic.Instance.SendEvent("Rate");
        ServiceController.Instance.Rate();
        Hide();
    }

    protected void Later()
    {
        Analytic.Instance.SendEvent("RateLater");
        Hide();
    }

    protected void Newer()
    {
        ServiceController.RateState = true;
        Analytic.Instance.SendEvent("RateNewer");
        Hide();
    }

    public override void Show()
    {
        GameSettings.ServiceData.IsShowRate = true;
        base.Show();
    }
}
