﻿using UnityEngine;
using System.Collections;

public class SettingsScreen : CachedTransitionScreen
{
    public ButtonAndAction restorePurchases;

    protected override void ShowRule(CachedTransitionScreen prevScreen)
    {

    }

    public override void InitButtons()
    {
        base.InitButtons();
        closeBtn.RegisterAction(Hide);
    }
}
