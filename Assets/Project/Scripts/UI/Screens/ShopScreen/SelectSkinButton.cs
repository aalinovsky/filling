﻿using UnityEngine;
using System.Collections;
using TMPro;

public class SelectSkinButton : MonoBehaviour
{
    public ButtonAndAction click;
    public TextMeshProUGUI skinLabel;
    public TextMeshProUGUI skinCost;

    public GameObject selectedState;
    public GameObject openState;
    public GameObject closedState;

    public Skin CurrentSkin
    {
        get
        {
            return selectedSkin;
        }

        set
        {
            selectedSkin = value;
            UpdateSkin();
        }
    }

    protected Skin selectedSkin;

    protected void UpdateSkin()
    {        

        if (selectedSkin.IsOpen)
        {
            if (!SkinsController.Instance.skinsDb.IsSelectedObject(selectedSkin))
            {
                SetState(SkinState.Open);
            }
            else
            {
                SetState(SkinState.Selected);
            }
        }
        else
        {
            SetState(SkinState.Closed);
        }
    }

    protected void BuySkin()
    {
        if (selectedSkin.cost <= GameSettings.Coins.Value)
        {
            GameSettings.Coins.Value -= selectedSkin.cost;
            selectedSkin.IsOpen = true;
            SelectSkin();
        }
    }

    protected void SelectSkin()
    {
        SkinsController.Instance.CurrentSkin = selectedSkin;
        SetState(SkinState.Selected);
    }

    protected void PlaySkin()
    {
        GameSettings.UI.GetScreen<ShopScreen>().Hide();
        GameController.Instance.StartGame();
    }

    protected void SetState(SkinState state)
    {
        click.UnregisterAction();
        selectedState.SetActive(false);
        openState.SetActive(false);
        closedState.SetActive(false);

        if (selectedSkin.label != null)
        {
            skinLabel.text = selectedSkin.label.Value.ToString();
        }
        else
        {
            skinLabel.text = "";
        }
        skinCost.text = selectedSkin.cost.ToString();

        switch (state)
        {
            case SkinState.Closed:
                    click.RegisterAction(BuySkin);
                    closedState.SetActive(true);                    
                break;
            case SkinState.Open:
                    click.RegisterAction(SelectSkin);
                    openState.SetActive(true);                    
                break;
            case SkinState.Selected:
                click.RegisterAction(PlaySkin);
                selectedState.SetActive(true);
                break;
        }
    }
}

public enum SkinState
{
    Closed,
    Open,
    Selected
}
