﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using DefaultUtility;
using System.Linq;

public class ShopScreen : CachedTransitionScreen
{
    public TextMeshProUGUI coinsLabel;
    public SelectSkinButton skinButton;
    public ButtonAndAction getRandomSkin;
    public TextMeshProUGUI randomSkinCost;
    public ButtonAndAction freeCoins;
    public TextMeshProUGUI freeCoinsLabel;
    public ScrollController scroll;

    protected override void ShowRule(CachedTransitionScreen prevScreen)
    {
        throw new System.NotImplementedException();
    }

    public override void InitButtons()
    {
        base.InitButtons();
        getRandomSkin.RegisterAction(RandomSkin);
        InitScroll();
        randomSkinCost.text = GameSettings.Data.gameplay.randomSkinCost.ToString();
    }

    public void UpdateCoins(int coins)
    {
        if (coinsLabel != null)
            coinsLabel.text = coins.ToString();
    }

    protected void InitScroll()
    {
        List<SkinData> skinsData = new List<SkinData>();

        foreach (var item in SkinsController.Instance.skinsDb.objectsList)
        {
            SkinData skinD = (SkinData)ScriptableObject.CreateInstance(typeof(SkinData));
            skinD.skin = item;
            skinsData.Add(skinD);
        }

        scroll.InitScrollContainer(skinsData);
        scroll.SetIndexAndPosition(SkinsController.Instance.skinsDb.CurrentObjectIndex);
    }

    public void SelectSkin(Skin s)
    {
        skinButton.CurrentSkin = s;
    }

    protected void RandomSkin()
    {
        Skin[] closedSkins = SkinsController.Instance.skinsDb.objectsList.FindAll(o => !o.IsOpen).ToArray();
        if(closedSkins.Length > 0 && GameSettings.Coins.Value >= GameSettings.Data.gameplay.randomSkinCost)
        {
            Skin randomClosedSkin = closedSkins[UnityEngine.Random.Range(0, closedSkins.Length)];
            int indexOfSkin = SkinsController.Instance.skinsDb.objectsList.IndexOf(randomClosedSkin);
            GameSettings.Coins.Value -= GameSettings.Data.gameplay.randomSkinCost;
            randomClosedSkin.IsOpen = true;
            SkinsController.Instance.CurrentSkin = randomClosedSkin;
            scroll.SetIndexAndPosition(indexOfSkin);
        }
    }

    public void Show(bool isLoadedReward)
    {
        Debug.Log("rewarded is loaded : " + isLoadedReward.ToString());
        freeCoinsLabel.text = GameController.Instance.freeCoinsFrase.GetValue(GameSettings.Data.gameplay.shopScreenFreeCoins);
        freeCoins.btn.gameObject.SetActive(isLoadedReward);
        Show();
    }
}
