﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DefaultUtility;

public class SkinShopElement : IElement
{
    public Color selected;
    public Color disabled;

    public Vector3 selectedScale;
    public Vector3 disabledScale;
    [Space()]
    public Image skinIcon;
    protected Skin skin;

    protected ShopScreen shop;

    public override void Init(object data)
    {
        shop = GameSettings.UI.GetScreen<ShopScreen>();
        SkinData s = (SkinData)data;
        skinIcon.sprite = s.skin.icon;
        skin = s.skin;
    }

    public override void Select()
    {
        shop.SelectSkin(skin);
        skinIcon.color = selected;
        skinIcon.transform.localScale = selectedScale;
    }

    public override void UnSelect()
    {
        skinIcon.transform.localScale = disabledScale;
        skinIcon.color = disabled;
    }

    //public override Transform GetTransform()
    //{
    //    return transform;
    //}

    public override void Click()
    {
    }
}

public class SkinData : ScrollElementData
{
    public Skin skin;
}