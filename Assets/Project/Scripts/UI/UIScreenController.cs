﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using CustomService;

public class UIScreenController : AbstractUIScreenController
{
    public bool enabledBack = true;

    public CachedTransitionScreen startScreen;
    public CachedTransitionScreen[] screensForStart;

    public override void Init()
    {
        base.Init();
        CurrentScreen = startScreen;

        for (int i = 0; i < screensForStart.Length; i++)
        {
            screensForStart[i].Show();
        }  
    }

    private void Update()
    {
#if !UNITY_IOS
        if (!enabledBack)
            return;

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (CurrentScreen != null)
            {
                CurrentScreen.Back();
            }
        }
#endif
    }
}
